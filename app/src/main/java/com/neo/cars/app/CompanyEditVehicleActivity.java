package com.neo.cars.app;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.neo.cars.app.Interface.CallBackButtonClick;
import com.neo.cars.app.Interface.MoreVehicleInfo_Interface;
import com.neo.cars.app.Interface.VehicleImageDeleteInterface;
import com.neo.cars.app.Interface.VehicleMake_Interface;
import com.neo.cars.app.Interface.VehicleModel_Interface;
import com.neo.cars.app.SetGet.PickUpLocationModel;
import com.neo.cars.app.SetGet.VehicleGalleryModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.Emailvalidation;
import com.neo.cars.app.Utils.ImageUtils;
import com.neo.cars.app.Utils.MessageText;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.NoDefaultSpinner;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Adapter.AddStopOverAdapterCompany;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.Webservice.ImageDeleteWebService;
import com.neo.cars.app.Webservice.MoreVehicleInfo_Webservice;
import com.neo.cars.app.Webservice.VehicleEdit_Webservice;
import com.neo.cars.app.Webservice.VehicleMake_Webservice;
import com.neo.cars.app.Webservice.VehicleModel_Webservice;
import com.neo.cars.app.Webservice.VehicleSoftDelete_Webservice;
import com.neo.cars.app.dialog.BottomSheetDialogPositiveNegative;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.dialog.YearMonthPickerDialog;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;
import com.neo.cars.app.font.CustomTextviewTitilliumBold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;


public class CompanyEditVehicleActivity extends RootActivity implements CallBackButtonClick,
        VehicleMake_Interface, VehicleModel_Interface, MoreVehicleInfo_Interface, VehicleImageDeleteInterface {

    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private RelativeLayout rlBackLayout;
//    private int transitionflag = StaticClass.transitionflagNext;

    private Context context;
    private Activity activity;
    private RelativeLayout rlImage1, rlImage2, rlImage3, rlUploadLicensePlateImage, rlOvertimeAvailable, rlNightChargesRate;
    private FrameLayout flSpnrVehicleType, flSpnrVehicleBrand, flSpnrVehicleMake;
    private ImageView ivVehicle1, ivVehicle2, ivVehicle3, ivTaxToken, ivPuc, ivLicensePlate, ibDefaultIamgeOne, ibDefaultIamgeTwo, ibDefaultIamgeThree;
    private NoDefaultSpinner spnrVehicleBrand, spnrVehicleMake, spnrParkingCharges;
    private File file, fileGalleryOne, fileGalleryTwo, fileGalleryThree, fileTax, filePuc, fileLicense,
            fileRegionalTransportPermit, fileRegistrationCertificateFront, fileRegistrationCertificateBack, fileFitnessCertificate, fileInsuranceCertificate;
    private ConnectionDetector cd;
    private ArrayList<String> listOfVehicleType, listOfVehicleMake, listOfVehicleModel;
    private ArrayAdapter<String> arrayAdapterVehicleType, arrayAdapterVehicleMake, arrayAdapterVehicleModel, arAdapterParkingCharges;
    private CustomButtonTitilliumSemibold btnSave, btnDelete;
    private CustomTextviewTitilliumBold tvLicensePlateTextview, tvRegionalTransportPermit, tvRegistrationCertificateFront, tvRegistrationCertificateBack,
            tvFitnessCertificate, tvInsuranceCertificate;
    private CustomEditTextTitilliumWebRegular tvHourlyRate, tvHourlyOvertimeRate, tvTotalKm, tvMaxPassengers, tvMaxLuggage, tvSpclInfo,
            tvLicenseno, etNightChargesRate;
    private CustomTextviewTitilliumWebRegular tvYrOfManufacture;

    private AppCompatImageView ivCheck;
    private boolean isSelected = false;
    private boolean isOtherLocSelected = false;

    private String strGalleryPath = "", strGalleryOnePath = "", strGalleryTwoPath = "", strGalleryThreePath = "", strTaxPath = "", strPcuPath = "", strLicenseIamgePath = "",
            strSelectedGalOne = "", strSelectedGalTwo = "", strSelectedGalThree = "", strSelectedTax = "", strSelectedPcu = "", strSelectedLicense = "",
            strBrandName = "", strVehicleMake = "", strHourlyRate = "", strOvertimeHourlyRate = "", strYrOfManufacture = "", strTotalKm = "", strMaxPassenger = "", strMaxLuggage = "", strSpecialInfo = "", strLicenseNo = "",
            strOvertimeAvailableCheck = "N", strSetGallerySelected = "", imageFilePath = "", strVehicleTypeId = "", strVehicleTypeName = "", strImage = "", strVehicleBrandId = "", strVehicleModelId = "",
            strVehicleMakeId = "", strVehicleBrandCode = "", strVehicleModelTitle = "", strSecondAddress = "", strAddress = "", mSelectedGalleryImagePath = "", mGalleryImagePath = "", userChoosenTask = "",
            strvehicleId = "", strGalleryOne = "", strGalleryTwo = "", strGalleryThree = "", strgalleryimageid1 = "", strgalleryimageid2 = "", strgalleryimageid3 = "", strDefaultImageId = "",
            strSittingCapacity = "", strLuggageCapacity = "", strStateId = "", strCityId = "", strStateName = "", strCityName = "",
            strACMin = "", strACMax = "", strNonACMin = "", strNonACMax = "", strOvertimeACMin = "", strOvertimeACMax = "", strOvertimeNonACMin = "", strOvertimeNonACMax = "";

    private String strSelectedRegionalTransport = "", strParkingCharge = "", toll_parking_charge = "", strNightChargesRate = "", strRegionalTransportImgPath = "",
            strRegistrationCertificateImgPathFront = "", strRegistrationCertificateImgPathBack = "", strFitnessCertificateImgPath = "", strInsuranceCertificateImgPath = "", strSelectedRegistrationCertificate = "",
            strSelectedFitnessCertificate = "", strSelectedInsuranceCertificate = "",
            strACNightMin = "", strACNightMax = "", strNONACNightMin = "", strNOnACNightMax = "";

    private static final int REQUEST_IMAGE_CAPTURE = 0411;
    private static final int PICK_IMAGE_REQUEST = 903;
    private Calendar mcalendar;
    private int day, month, year;

    private LinearLayout lladdmore_pickuplocation, llAddMoreLayout;
    private CustomTextviewTitilliumWebRegular tvpickuploc_addmore;

    private ArrayList<VehicleGalleryModel> listOfVehicleGallery;
    private ArrayList<PickUpLocationModel> listOfPickUpLocation;
    private ArrayList<String> listOfVehicleImage, listOfSelectedDefaultImage, listOfSelectedPickUpLocation, listOfParkingCharges;

    private FrameLayout flSpnrMaxPassenger, flSpnrMaxLuggage;
    private NoDefaultSpinner spnrMaxPassenger, spnrMaxLuggage;
    private int intMaxPassenger = 0, intMaxLuggage = 0;
    private ArrayList<Integer> listOfMaxPassenger, listOfMaxLuggage;
    private ArrayAdapter<Integer> arrayAdapterMaxPassenger, arrayAdapterMaxLuggage;
    private RelativeLayout llHourlyOvertimeLayout;

    private String strMaxPassngr = "", strMaxLugg = "";
    private VehicleTypeModel vehicleTypeModel;
    private ImageView iv_licenceplatealert, ivRegionalTransportPermit, ivRegistrationCertificateFront, ivRegistrationCertificateBack, ivFitnessCertificate, ivInsuranceCertificate,
            ivInfoNightChargesRate, ivInfoParkingCharges, ivinfoRegionalTransportPermit, ivinfoRegistrationCertificateFront, ivinfoRegistrationCertificateBack,
            ivinfoFitnessCertificate, ivinfoInsuranceCertificate;
    private CustomTextviewTitilliumWebRegular spnrStateList, spnrCityList;

    private int transitionflag = StaticClass.transitionflagNext;

    private static final int StateListRequestCode = 501;
    private static final int CityListRequestCode = 502;

    private boolean isDelete = false, isACSelected = false, isNightChargeSelected = false;

    private ImageView ivACCheck, iv_hourlyratealert;
    private String strACCheck = "", strOtherLoc = "N", strNightChargesCheck = "N";
    private ImageView iv_pickupinfo, iv_delete_icon2, iv_delete_icon3;

    private CustomTitilliumTextViewSemiBold tvStateListheader, tvCityListheader, tvVehicleTypeheader, tvVehiclemakerheader, tvBrandnameheader,
            tvYrOfManufactureheader, tvTotalKmheader, tvMaxPassengersheader, tvMaxLuggageheader, tvHourlyRateheader,
            tvCheck, tvLicenseheader, tvACAvailable;

    private CustomTextviewTitilliumBold tvTaxTokenTextview, tvPucTextview;

    private SpannableStringBuilder builder;
    private int start, end;
    private ImageView iv_hourlyovertimeratealert;
    private CustomTitilliumTextViewSemiBold tvShowOtherLoc;
    private ImageView iv_otherloc;
    private AppCompatImageView ivOtherLocCheck, ivCheckNightChargesAvailable;
    private CustomTextviewTitilliumWebRegular spnrVehicleType;
    private SharedPrefUserDetails sharedPref;

    private ProgressDialog dialog;
    private Uri imageUri;

    private String image_id2 = "", image_id3 = "", user_vehicle_id = "";

    private BottomViewCompany bottomViewCompany = new BottomViewCompany();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_vehicle_edit);

        strvehicleId = getIntent().getStringExtra("vehicleId");
        new AnalyticsClass(CompanyEditVehicleActivity.this);

        Initialize();

        if (NetWorkStatus.isNetworkAvailable(activity)) {
            new MoreVehicleInfo_Webservice().moreVehicleInfo(activity, CompanyEditVehicleActivity.this, strvehicleId);

        } else {
            Intent i = new Intent(activity, NetworkNotAvailable.class);
            startActivity(i);
        }
        Listener();
    }

    private void Initialize() {
        sharedPref = new SharedPrefUserDetails(this);
        activity = CompanyEditVehicleActivity.this;
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvDetails));

        rlBackLayout = findViewById(R.id.rlBackLayout);

        context = activity;
        cd = new ConnectionDetector(activity);
        mcalendar = Calendar.getInstance();
        day = mcalendar.get(Calendar.DAY_OF_MONTH);
        year = mcalendar.get(Calendar.YEAR);
        month = mcalendar.get(Calendar.MONTH);

        tvStateListheader = findViewById(R.id.tvStateListheader);

        tvCityListheader = findViewById(R.id.tvCityListheader);
        tvVehicleTypeheader = findViewById(R.id.tvVehicleTypeheader);
        tvVehiclemakerheader = findViewById(R.id.tvVehiclemakerheader);
        tvBrandnameheader = findViewById(R.id.tvBrandnameheader);
        tvACAvailable = findViewById(R.id.tvACAvailable);
        tvYrOfManufactureheader = findViewById(R.id.tvYrOfManufactureheader);
        tvTotalKmheader = findViewById(R.id.tvTotalKmheader);
        tvMaxPassengersheader = findViewById(R.id.tvMaxPassengersheader);
        tvMaxLuggageheader = findViewById(R.id.tvMaxLuggageheader);
        tvHourlyRateheader = findViewById(R.id.tvHourlyRateheader);
        tvCheck = findViewById(R.id.tvCheck);
        tvLicenseheader = findViewById(R.id.tvLicenseheader);
        tvTaxTokenTextview = findViewById(R.id.tvTaxTokenTextview);
        tvPucTextview = findViewById(R.id.tvPucTextview);

        ivVehicle1 = findViewById(R.id.ivVehicle1);
        ivVehicle2 = findViewById(R.id.ivVehicle2);
        ivVehicle3 = findViewById(R.id.ivVehicle3);
        flSpnrVehicleType = findViewById(R.id.flSpnrVehicleType);
        spnrVehicleType = findViewById(R.id.spnrVehicleType);

        btnSave = findViewById(R.id.btnSave);
        btnDelete = findViewById(R.id.btnDelete);

        tvTaxTokenTextview = findViewById(R.id.tvTaxTokenTextview);

        tvPucTextview = findViewById(R.id.tvPucTextview);

        tvHourlyRate = findViewById(R.id.tvHourlyRate);
        tvHourlyOvertimeRate = findViewById(R.id.tvHourlyOvertimeRate);
        tvYrOfManufacture = findViewById(R.id.tvYrOfManufacture);
        tvTotalKm = findViewById(R.id.tvTotalKm);
        tvMaxPassengers = findViewById(R.id.tvMaxPassengers);
        tvMaxLuggage = findViewById(R.id.tvMaxLuggage);
        tvSpclInfo = findViewById(R.id.tvSpclInfo);
        tvLicenseno = findViewById(R.id.tvLicenseno);
        tvLicenseno.setEnabled(false);
        tvLicenseno.setClickable(false);
        etNightChargesRate = findViewById(R.id.etNightChargesRate);

        tvRegionalTransportPermit = findViewById(R.id.tvRegionalTransportPermit);
        tvRegistrationCertificateFront = findViewById(R.id.tvRegistrationCertificateFront);
        tvRegistrationCertificateBack = findViewById(R.id.tvRegistrationCertificateBack);
        tvFitnessCertificate = findViewById(R.id.tvFitnessCertificate);
        tvInsuranceCertificate = findViewById(R.id.tvInsuranceCertificate);

        flSpnrVehicleBrand = findViewById(R.id.flSpnrVehicleBrand);
        flSpnrVehicleMake = findViewById(R.id.flSpnrVehicleMake);
        spnrVehicleBrand = findViewById(R.id.spnrVehicleBrand);
        spnrVehicleMake = findViewById(R.id.spnrVehicleMake);
        ivTaxToken = findViewById(R.id.ivTaxToken);
        ivPuc = findViewById(R.id.ivPuc);
        rlUploadLicensePlateImage = findViewById(R.id.rlUploadLicensePlateImage);
        ivLicensePlate = findViewById(R.id.ivLicensePlate);
        tvLicensePlateTextview = findViewById(R.id.tvLicensePlateTextview);

        rlOvertimeAvailable = findViewById(R.id.rlOvertimeAvailable);
        ivCheck = findViewById(R.id.ivCheck);
        ibDefaultIamgeOne = findViewById(R.id.ibDefaultIamgeOne);
        ibDefaultIamgeTwo = findViewById(R.id.ibDefaultIamgeTwo);
        ibDefaultIamgeThree = findViewById(R.id.ibDefaultIamgeThree);

        iv_delete_icon2 = findViewById(R.id.iv_delete_icon2);
        iv_delete_icon3 = findViewById(R.id.iv_delete_icon3);

        tvpickuploc_addmore = findViewById(R.id.tvpickuploc_addmore);
        lladdmore_pickuplocation = findViewById(R.id.lladdmore_pickuplocation);
        llAddMoreLayout = findViewById(R.id.llAddMoreLayout);
        rlNightChargesRate = findViewById(R.id.rlNightChargesRate);

        flSpnrMaxPassenger = findViewById(R.id.flSpnrMaxPassenger);
        spnrMaxPassenger = findViewById(R.id.spnrMaxPassenger);

        flSpnrMaxLuggage = findViewById(R.id.flSpnrMaxLuggage);
        spnrMaxLuggage = findViewById(R.id.spnrMaxLuggage);

        llHourlyOvertimeLayout = findViewById(R.id.llHourlyOvertimeLayout);

        iv_licenceplatealert = findViewById(R.id.iv_licenceplatealert);

        spnrStateList = findViewById(R.id.spnrStateList);
        spnrCityList = findViewById(R.id.spnrCityList);
        spnrParkingCharges = findViewById(R.id.spnrParkingCharges);

        ivACCheck = findViewById(R.id.ivACCheck);
        ivCheckNightChargesAvailable = findViewById(R.id.ivCheckNightChargesAvailable);
        iv_hourlyratealert = findViewById(R.id.iv_hourlyratealert);
        iv_pickupinfo = findViewById(R.id.iv_pickupinfo);
        iv_hourlyovertimeratealert = findViewById(R.id.iv_hourlyovertimeratealert);

        tvShowOtherLoc = findViewById(R.id.tvShowOtherLoc);
        iv_otherloc = findViewById(R.id.iv_otherloc);
        ivOtherLocCheck = findViewById(R.id.ivOtherLocCheck);

        ivRegionalTransportPermit =  findViewById(R.id.ivRegionalTransportPermit);
        ivRegistrationCertificateFront =  findViewById(R.id.ivRegistrationCertificateCFront);
        ivRegistrationCertificateBack =  findViewById(R.id.ivRegistrationCertificateBack);
        ivFitnessCertificate =  findViewById(R.id.ivFitnessCertificate);
        ivInsuranceCertificate =  findViewById(R.id.ivInsuranceCertificate);

        ivInfoNightChargesRate=findViewById(R.id.ivInfoNightChargesRate);
        ivInfoParkingCharges=findViewById(R.id.ivInfoParkingCharges);
        ivinfoRegionalTransportPermit=findViewById(R.id.ivinfoRegionalTransportPermit);
        ivinfoRegistrationCertificateFront=findViewById(R.id.ivinfoRegistrationCertificateFront);
        ivinfoRegistrationCertificateBack=findViewById(R.id.ivinfoRegistrationCertificateBack);
        ivinfoFitnessCertificate=findViewById(R.id.ivinfoFitnessCertificate);
        ivinfoInsuranceCertificate=findViewById(R.id.ivinfoInsuranceCertificate);

        listOfParkingCharges = new ArrayList<>();

        listOfParkingCharges.add(getResources().getString(R.string.included));
        listOfParkingCharges.add(getResources().getString(R.string.atActuals));

        arAdapterParkingCharges = new ArrayAdapter<String>(context, R.layout.countryitem, listOfParkingCharges);
        arAdapterParkingCharges.setDropDownViewResource(R.layout.simpledropdownitem);
        spnrParkingCharges.setPrompt(getResources().getString(R.string.tvSelectParkingCharges));
        spnrParkingCharges.setAdapter(arAdapterParkingCharges);

        //added by tb
        StaticClass.resetAl_Location_company();
//        bottomview.BottomView(CompanyEditVehicleActivity.this, StaticClass.Menu_profile);

    }

    private void Listener() {


        iv_delete_icon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialog(image_id2);

//                Toast.makeText(context, "Image_Id : "+image_id2+ "Vehicle_Id:"+user_vehicle_id, Toast.LENGTH_SHORT).show();

            }
        });

        iv_delete_icon3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialog(image_id3);

//                Toast.makeText(context, "Image_Id : "+image_id3+ "Vehicle_Id:"+user_vehicle_id, Toast.LENGTH_SHORT).show();

            }
        });



        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(CompanyEditVehicleActivity.this,
                        CompanyEditVehicleActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                        CompanyEditVehicleActivity.this.getResources().getString(R.string.yes),
                        CompanyEditVehicleActivity.this.getResources().getString(R.string.no));
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                        transitionflag = StaticClass.transitionflagBack;
                        finish();
                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();
            }
        });

        iv_pickupinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CustomToast(activity, MessageText.PickUpLocationInfoText);
            }
        });


        btnDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String strDeactivateMsg = activity.getResources().getString(R.string.OwnerDelete);

                final CustomAlertDialogOKCancel alertDialogYESNO = new CustomAlertDialogOKCancel(context,
                        strDeactivateMsg,
                        context.getResources().getString(R.string.yes),
                        context.getResources().getString(R.string.no));

                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialogYESNO.dismiss();
                        if (cd.isConnectingToInternet()) {
                            isDelete = true;
                            new VehicleSoftDelete_Webservice().VehicleSoftDelete(activity, strvehicleId);

                        } else {
                            new CustomToast(activity, activity.getResources().getString(R.string.Network_not_availabl));
                        }

                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();
            }
        });

        iv_licenceplatealert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CustomToast(activity, MessageText.Pleaeuseaformatasfollows);
            }
        });

        ibDefaultIamgeOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("d", "Path in 1:: " + strGalleryOnePath);
                Log.d("d", "***strGalleryOne****" + strGalleryOne);
                if ((!"".equals(strGalleryOnePath) && !"null".equals(strGalleryOnePath)) || (!"".equals(strGalleryOne) && !"null".equals(strGalleryOne))) {
                    ibDefaultIamgeOne.setImageResource(R.drawable.vehicle_info_blank_image_select_fill);
                    ibDefaultIamgeTwo.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                    ibDefaultIamgeThree.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                    strSetGallerySelected = "1";
//                    strDefaultImageId = listOfVehicleGallery.get(0).getId();

                    // modified by tb
                    if(listOfVehicleGallery !=null && listOfVehicleGallery.size()>0)
                        strDefaultImageId = listOfVehicleGallery.get(0).getId();
                    else
                        strDefaultImageId="0";

                    Log.d("strDefaultImageId", strDefaultImageId);
                    if (strgalleryimageid1.equals("0") || strgalleryimageid2.equals("0") || strgalleryimageid3.equals("0")){
                        strDefaultImageId = "0";
                    }

                } else {
                    new CustomToast(activity, getResources().getString(R.string.tvUploadImageFirst));
                }
            }
        });

        ibDefaultIamgeTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("d", "Path in 2:: " + strGalleryTwoPath);
                if ((!"".equals(strGalleryTwoPath) && !"null".equals(strGalleryTwoPath)) || (!"".equals(strGalleryTwo) && !"null".equals(strGalleryTwo))) {
                    ibDefaultIamgeOne.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                    ibDefaultIamgeTwo.setImageResource(R.drawable.vehicle_info_blank_image_select_fill);
                    ibDefaultIamgeThree.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                    strSetGallerySelected = "2";
//                    strDefaultImageId = listOfVehicleGallery.get(1).getId();

                    // modified by tb
                    if(listOfVehicleGallery !=null && listOfVehicleGallery.size()>1)
                        strDefaultImageId = listOfVehicleGallery.get(1).getId();
                    else
                        strDefaultImageId="0";

                    Log.d("strDefaultImageId", strDefaultImageId);
                    if (strgalleryimageid1.equals("0") || strgalleryimageid2.equals("0") || strgalleryimageid3.equals("0")){
                        strDefaultImageId = "0";
                    }

                } else {
                    new CustomToast(activity, getResources().getString(R.string.tvUploadImageFirst));

                }
            }
        });


        ibDefaultIamgeThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("d", "Path in 3:: " + strGalleryThreePath);
                if ((!"".equals(strGalleryThreePath) && !"null".equals(strGalleryThreePath)) || (!"".equals(strGalleryThree) && !"null".equals(strGalleryThree))) {
                    ibDefaultIamgeOne.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                    ibDefaultIamgeTwo.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                    ibDefaultIamgeThree.setImageResource(R.drawable.vehicle_info_blank_image_select_fill);

                    strSetGallerySelected = "3";
//                    strDefaultImageId = listOfVehicleGallery.get(2).getId();
                    Log.d("strDefaultImageId", strDefaultImageId);

                    // modified by tb
                    if(listOfVehicleGallery !=null && listOfVehicleGallery.size()>2)
                        strDefaultImageId = listOfVehicleGallery.get(2).getId();
                    else
                        strDefaultImageId="0";

                    if (strgalleryimageid1.equals("0") || strgalleryimageid2.equals("0") || strgalleryimageid3.equals("0")){
                        strDefaultImageId = "0";
                    }
                } else {
                    new CustomToast(activity, getResources().getString(R.string.tvUploadImageFirst));
                }
            }
        });

        ivCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSelected == true) {
                    ivCheck.setImageResource(R.drawable.uncheck);
                    isSelected = false;
                    strOvertimeAvailableCheck = "N";

                    llHourlyOvertimeLayout.setVisibility(View.GONE);

                    /*tvHourlyOvertimeRate.setText("");
                    tvHourlyOvertimeRate.setEnabled(false);*/

                } else if (isSelected == false) {
                    ivCheck.setImageResource(R.drawable.check);
                    isSelected = true;
                    strOvertimeAvailableCheck = "Y";

                    llHourlyOvertimeLayout.setVisibility(View.VISIBLE);
                    //  tvHourlyOvertimeRate.setEnabled(true);
                }
            }
        });

        ivCheckNightChargesAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNightChargeSelected){
                    ivCheckNightChargesAvailable.setImageResource(R.drawable.uncheck);
                    isNightChargeSelected = false;
                    strNightChargesCheck = "N";
                    rlNightChargesRate.setVisibility(View.GONE);
                }else {
                    ivCheckNightChargesAvailable.setImageResource(R.drawable.check);
                    isNightChargeSelected = true;
                    strNightChargesCheck = "Y";
                    rlNightChargesRate.setVisibility(View.VISIBLE);
                }
            }
        });

        ivOtherLocCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isOtherLocSelected == true) {
                    ivOtherLocCheck.setImageResource(R.drawable.uncheck);
                    isOtherLocSelected = false;
                    strOtherLoc = "N";

                } else if (isOtherLocSelected == false) {
                    ivOtherLocCheck.setImageResource(R.drawable.check);
                    isOtherLocSelected = true;
                    strOtherLoc = "Y";
                }
            }
        });

        iv_hourlyratealert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //set custom dialog for rate chart
                final Dialog customdialog = new Dialog(activity);
                customdialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                customdialog.setContentView(R.layout.dialog_car_rate);

                // set the custom dialog components - text, image and button
                CustomTextviewTitilliumBold tvVehicletype = customdialog.findViewById(R.id.tvVehicletype);
                tvVehicletype.setText(strVehicleTypeName);

                CustomTextviewTitilliumBold tvACMinRate = customdialog.findViewById(R.id.tvACMinRate);
                CustomTextviewTitilliumBold tvACMaxRate = customdialog.findViewById(R.id.tvACMaxRate);
                CustomTextviewTitilliumBold tvNonACMinRate = customdialog.findViewById(R.id.tvNonACMinRate);
                CustomTextviewTitilliumBold tvNonACMaxRate = customdialog.findViewById(R.id.tvNonACMaxRate);
                CustomButtonTitilliumSemibold btnOk = customdialog.findViewById(R.id.btnOk);

                if (!"".equals(strACMin) && !"null".equals(strACMin)) {
                    tvACMinRate.setText(strACMin);
                } else {
                    tvACMinRate.setText("N/A");
                }

                if (!"".equals(strACMax) && !"null".equals(strACMax)) {
                    tvACMaxRate.setText(strACMax);
                } else {
                    tvACMaxRate.setText("N/A");
                }

                if (!"".equals(strNonACMin) && !"null".equals(strNonACMin)) {
                    tvNonACMinRate.setText(strNonACMin);
                } else {
                    tvNonACMinRate.setText("N/A");
                }

                if (!"".equals(strNonACMax) && !"null".equals(strNonACMax)) {
                    tvNonACMaxRate.setText(strNonACMax);
                } else {
                    tvNonACMaxRate.setText("N/A");
                }


                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customdialog.dismiss();
                    }
                });
                customdialog.show();
            }
        });

        ivInfoNightChargesRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean dialogFlag=true;
                if(strACCheck.equalsIgnoreCase("N")){
                    if(strNonACMin.equals("") || strNonACMax.equals("")){
                        dialogFlag=false;
                    }
                }else{
                    if(strACMax.equals("") || strACMin.equals("")){
                        dialogFlag=false;
                    }
                }

                if (dialogFlag){
                    final Dialog customdialog = new Dialog(CompanyEditVehicleActivity.this);
                    customdialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                    customdialog.setContentView(R.layout.dialog_car_night_rate);

                    CustomTextviewTitilliumBold tvNightACMinRate = customdialog.findViewById(R.id.tvNightACMinRate);
                    CustomTextviewTitilliumBold tvNightACMaxRate = customdialog.findViewById(R.id.tvNightACMaxRate);
                    CustomTextviewTitilliumBold tvNightNonACMinRate = customdialog.findViewById(R.id.tvNightNonACMinRate);
                    CustomTextviewTitilliumBold tvNightNonACMaxRate = customdialog.findViewById(R.id.tvNightNonACMaxRate);
                    CustomButtonTitilliumSemibold btnOk = customdialog.findViewById(R.id.btnOk);

                    if (!TextUtils.isEmpty(strACNightMin) && !TextUtils.isEmpty(strACNightMax)){
                        tvNightACMinRate.setText(strACNightMin);
                        tvNightACMaxRate.setText(strACNightMax);

                    }else{
                        tvNightACMinRate.setText("N/A");
                        tvNightACMaxRate.setText("N/A");
                    }

                    if (!TextUtils.isEmpty(strNONACNightMin) && !TextUtils.isEmpty(strNOnACNightMax)){

                        tvNightNonACMinRate.setText(strNONACNightMin);
                        tvNightNonACMaxRate.setText(strNOnACNightMax);

                    }else {
                        tvNightNonACMinRate.setText("N/A");
                        tvNightNonACMaxRate.setText("N/A");
                    }


                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customdialog.dismiss();
                        }
                    });
                    customdialog.show();


                }else {
                    new CustomToast(CompanyEditVehicleActivity.this, "Please select vehicle model");
                }
            }
        });

        ivInfoParkingCharges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CustomToast(activity, MessageText.tvParkingChargesHeader);

            }
        });

        ivinfoRegionalTransportPermit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CustomToast(activity, MessageText.tvinfoRegionalPermitHeader);
            }
        });

        ivinfoRegistrationCertificateFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CustomToast(activity, MessageText.tvinfoRegistrationCertificate);
            }
        });

        ivinfoRegistrationCertificateBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CustomToast(activity, MessageText.tvinfoRegistrationCertificateBack);
            }
        });

        ivinfoFitnessCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CustomToast(activity, MessageText.tvinfoFitnessCertificate);
            }
        });

        ivinfoInsuranceCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CustomToast(activity, MessageText.tvinfoInsuranceCertificate);
            }
        });

        tvLicensePlateTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "License";
                selectImage(getResources().getString(R.string.msgUpldLicenseplateImage), strImage);
            }
        });

        tvTaxTokenTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "Tax";
                selectImage(getResources().getString(R.string.msgUpldTaxTokenImage), strImage);
            }
        });

        tvPucTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "Puc";
                selectImage(getResources().getString(R.string.msgUpldPUCPaper), strImage);
            }
        });

        tvRegionalTransportPermit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "RegionalTransportPermit";
                selectImage(getResources().getString(R.string.msgRegionalTransportPermit), strImage);
            }
        });

        tvRegistrationCertificateFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "RegistrationCertificateFront";
                selectImage(getResources().getString(R.string.msgRegistrationCertificate), strImage);
            }
        });

        tvRegistrationCertificateBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "RegistrationCertificateBack";
                selectImage(getResources().getString(R.string.msgRegistrationCertificate), strImage);
            }
        });

        tvFitnessCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "FitnessCertificate";
                selectImage(getResources().getString(R.string.msgFitnessCertificate), strImage);
            }
        });

        tvInsuranceCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "InsuranceCertificate";
                selectImage(getResources().getString(R.string.msgInsuranceCertificate), strImage);
            }
        });

        tvYrOfManufacture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                YrOfManufactureDialog();
            }
        });

        ivVehicle1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "Gallery1";
                selectImage(getResources().getString(R.string.msgUpldVehicleOneImage), strImage);
            }
        });

        ivVehicle2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "Gallery2";
                selectImage(getResources().getString(R.string.msgUpldVehicleOneImage), strImage);
            }
        });

        ivVehicle3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "Gallery3";
                selectImage(getResources().getString(R.string.msgUpldVehicleOneImage), strImage);
            }
        });

        spnrStateList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitionflag = StaticClass.transitionflagNext;
                Intent stateintent = new Intent(activity, StateList.class);
                startActivityForResult(stateintent, StateListRequestCode);

            }
        });

        spnrCityList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strStateId != null && !strStateId.equals("")) {
                    transitionflag = StaticClass.transitionflagNext;
                    Intent cityintent = new Intent(activity, CityList.class);
                    cityintent.putExtra("strStateId", strStateId);
                    startActivityForResult(cityintent, CityListRequestCode);
                }else {
                    new CustomToast(activity, "Please select State first!");
                }
            }
        });


        spnrParkingCharges.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                strParkingCharge = listOfParkingCharges.get(position);
                Log.d("strParkingCharge*", strParkingCharge);

                if (strParkingCharge.equalsIgnoreCase("Included")){

                    toll_parking_charge = "I";
                    Log.d("toll_parking_charge :", toll_parking_charge);
                }else if (strParkingCharge.equalsIgnoreCase("At Actuals")){
                    toll_parking_charge = "A";
                    Log.d("toll_parking_charge :", toll_parking_charge);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ivACCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isACSelected == true) {
                    ivACCheck.setImageResource(R.drawable.uncheck);
                    isACSelected = false;
                    strACCheck = "N";

                } else if (isACSelected == false) {
                    ivACCheck.setImageResource(R.drawable.check);
                    isACSelected = true;
                    strACCheck = "Y";
                }
            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvHourlyRate.setError(null);
                tvYrOfManufacture.setError(null);
                tvTotalKm.setError(null);
                tvLicenseno.setError(null);

                boolean cancel = false;
                View focusView = null;

                strHourlyRate = tvHourlyRate.getText().toString().trim();
                strOvertimeHourlyRate = tvHourlyOvertimeRate.getText().toString().trim();
                strNightChargesRate = etNightChargesRate.getText().toString().trim();
                strYrOfManufacture = tvYrOfManufacture.getText().toString().trim();
                strTotalKm = tvTotalKm.getText().toString().trim();


                Log.d("d", "Spinner:::" + spnrMaxPassenger.getSelectedItem());
                if (spnrMaxPassenger != null && spnrMaxPassenger.getSelectedItem() != null) {
                    strMaxPassenger = spnrMaxPassenger.getSelectedItem().toString();
                    Log.d("d", "strMaxPassenger:::" + strMaxPassenger);
                }

                if (spnrMaxLuggage != null && spnrMaxLuggage.getSelectedItem() != null) {
                    strMaxLuggage = spnrMaxLuggage.getSelectedItem().toString();
                    Log.d("d", "**strMaxLuggage***" + strMaxLuggage);
                }


                strLicenseNo = tvLicenseno.getText().toString().trim();

                strSecondAddress = tvpickuploc_addmore.getText().toString().trim();

                //Add vehicle field validation check
                if (TextUtils.isEmpty(strHourlyRate)) {
                    tvHourlyRate.setError(getString(R.string.error_field_required));
                    tvHourlyRate.requestFocus();
                    //focusView = tvHourlyRate;
                    // cancel = true;

                }else if (isNightChargeSelected && TextUtils.isEmpty(strNightChargesRate)){
                    new CustomToast(CompanyEditVehicleActivity.this, getResources().getString(R.string.msgNightCharge));
                    etNightChargesRate.requestFocus();

                } else if (TextUtils.isEmpty(strYrOfManufacture)) {
                    tvYrOfManufacture.setError(getString(R.string.error_field_required));
                    tvYrOfManufacture.requestFocus();
                    //focusView = tvYrOfManufacture;
                    //cancel = true;

                } else if (TextUtils.isEmpty(strTotalKm)) {
                    tvTotalKm.setError(getString(R.string.error_field_required));
                    tvTotalKm.requestFocus();
                    // focusView = tvTotalKm;
                    // cancel = true;

                }

                else if (TextUtils.isEmpty(strLicenseNo)) {
                    tvLicenseno.setError(getString(R.string.error_field_required));
                    tvLicenseno.requestFocus();
                    //focusView = tvLicenseno;
                    //cancel = true;

                } else if (!new Emailvalidation().lincescePlate(strLicenseNo.toUpperCase())) {
                    tvLicenseno.setError(getString(R.string.Entervalid_Vehicleregistrationplates));
                    tvLicenseno.requestFocus();
                    //focusView = tvLicenseno;
                    //cancel = true;

                }else if(strVehicleModelId.equals("")){

                    new CustomToast(CompanyEditVehicleActivity.this, "Select Model");
                }  else if (strGalleryPath.equalsIgnoreCase("")) {
                    new CustomToast(activity, getResources().getString(R.string.msgUploadGalleryPic));

               }else {
           /*["Dum Dum Airport "," howrah station "," kolkata station "]*/
                    ArrayList<String> myLocation = new ArrayList<String>();

                    if (StaticClass.getAl_Location_company() != null) {
//                        myLocation = StaticClass.getAl_Location();
                        myLocation = StaticClass.getAl_Location_company();
                    }

                    JSONArray jsonArrayAddress = new JSONArray();
//                    jsonArrayAddress.put(strSecondAddress);
                    for (int i = 0; i < myLocation.size(); i++) {
                        if (!myLocation.get(i).equals("")) {
                            jsonArrayAddress.put(myLocation.get(i));
                        }
                    }

                    strAddress = jsonArrayAddress.toString();
                    Log.d("d", "String address::" + strAddress);

                    if (cd.isConnectingToInternet()) {
                        new VehicleEdit_Webservice().VehicleEdit(activity, strvehicleId, strVehicleTypeId, strVehicleBrandId, strVehicleModelId,
                                strACCheck, strYrOfManufacture, strTotalKm, strStateId, strCityId, strMaxPassenger, strMaxLuggage, strHourlyRate, strOvertimeAvailableCheck, strOvertimeHourlyRate,
                                strSpecialInfo, strLicenseNo, strAddress, strOtherLoc, fileTax, filePuc, fileGalleryOne, fileGalleryTwo, fileGalleryThree,
                                fileRegionalTransportPermit, fileRegistrationCertificateFront, fileRegistrationCertificateBack, fileFitnessCertificate, fileInsuranceCertificate,
                                strgalleryimageid1, strgalleryimageid2,
                                strgalleryimageid3 , strSetGallerySelected, strDefaultImageId,
                                toll_parking_charge, strNightChargesCheck, strNightChargesRate);

                    } else {
                        Intent i = new Intent(activity, NetworkNotAvailable.class);
                        startActivity(i);
                    }
                }
            }
        });

        llAddMoreLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvpickuploc_addmore.getText().toString().trim().equals("")) {

                } else {
                    AddStopOverMethod();
                }
            }
        });

        iv_hourlyovertimeratealert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog customdialog = new Dialog(activity);
                customdialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                customdialog.setContentView(R.layout.dialog_car_rate);

                // set the custom dialog components - text, image and button
                CustomTextviewTitilliumBold tvVehicletype = customdialog.findViewById(R.id.tvVehicletype);
                tvVehicletype.setText(strVehicleTypeName);

                CustomTextviewTitilliumBold tvACMinRate = customdialog.findViewById(R.id.tvACMinRate);
                CustomTextviewTitilliumBold tvACMaxRate = customdialog.findViewById(R.id.tvACMaxRate);
                CustomTextviewTitilliumBold tvNonACMinRate = customdialog.findViewById(R.id.tvNonACMinRate);
                CustomTextviewTitilliumBold tvNonACMaxRate = customdialog.findViewById(R.id.tvNonACMaxRate);
                CustomButtonTitilliumSemibold btnOk = customdialog.findViewById(R.id.btnOk);

                if (!"".equals(strOvertimeACMin) && !"null".equals(strOvertimeACMin)) {
                    tvACMinRate.setText(strOvertimeACMin);
                } else {
                    tvACMinRate.setText("N/A");
                }

                if (!"".equals(strOvertimeACMax) && !"null".equals(strOvertimeACMax)) {
                    tvACMaxRate.setText(strOvertimeACMax);
                } else {
                    tvACMaxRate.setText("N/A");
                }

                if (!"".equals(strOvertimeNonACMin) && !"null".equals(strOvertimeNonACMin)) {
                    tvNonACMinRate.setText(strOvertimeNonACMin);
                } else {
                    tvNonACMinRate.setText("N/A");
                }

                if (!"".equals(strOvertimeNonACMax) && !"null".equals(strOvertimeNonACMax)) {
                    tvNonACMaxRate.setText(strOvertimeNonACMax);
                } else {
                    tvNonACMaxRate.setText("N/A");
                }

                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customdialog.dismiss();
                    }
                });
                customdialog.show();

            }
        });

        iv_otherloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CustomToast(activity, MessageText.OtherLocationWhileBookingText);
            }
        });
    }

    private void showDialog(final String image_id){
        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(CompanyEditVehicleActivity.this,
                CompanyEditVehicleActivity.this.getResources().getString(R.string.are_you_sure_to_delete),
                CompanyEditVehicleActivity.this.getResources().getString(R.string.yes),
                CompanyEditVehicleActivity.this.getResources().getString(R.string.no));
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();

                if(NetWorkStatus.isNetworkAvailable(CompanyEditVehicleActivity.this)){

                    new ImageDeleteWebService().imageDelete(CompanyEditVehicleActivity.this, user_vehicle_id, image_id);

                }else{

                }

            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();
    }

    private void AddStopOverMethod() {
        new AddStopOverAdapterCompany(activity, lladdmore_pickuplocation);
    }


    public void selectImage(final String strMessage, String filetype) {

        BottomSheetDialogPositiveNegative bsd = new BottomSheetDialogPositiveNegative(context,
                activity,
                CompanyEditVehicleActivity.this,
                strMessage,
                CompanyEditVehicleActivity.this.getResources().getString(R.string.Camera),
                CompanyEditVehicleActivity.this.getResources().getString(R.string.Gallery));
    }

    public void YrOfManufactureDialog() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), 01, 01);

        YearMonthPickerDialog yearMonthPickerDialog = new YearMonthPickerDialog(activity,
                calendar,
                new YearMonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onYearMonthSet(int year) {
                        System.out.println("**YearMonthPickerDialog**" + year);
                        tvYrOfManufacture.setText(String.valueOf(year));
                    }
                });

        yearMonthPickerDialog.show();
    }



    @Override
    public void onButtonClick(String strButtonText) {
        System.out.println("****strButtonText***" + strButtonText);

//        boolean result = CommonUtility.checkPermission(context);

        if (strButtonText.equals(activity.getResources().getString(R.string.Camera))) {
            new PrintClass("Alert Camera");
            userChoosenTask = "Camera";
//            if (result) {

                Dexter.withActivity(this).withPermissions( Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener()
                        {
                            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if(report.areAllPermissionsGranted()){

                                    takePhoto();
                                }

                                // check for permanent denial of any permission
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    // permission is denied permenantly, navigate user to app settings
                                    showAlert();
                                }
                            }
                            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                            {/* ... */

                                token.continuePermissionRequest();
                            }
                        }).check();

//            }


        } else if (strButtonText.equals(activity.getResources().getString(R.string.Gallery))) {
            new PrintClass("Alert Gallery");
            userChoosenTask = "Gallery";
//            if (result) {

                Dexter.withActivity(this).withPermissions( Manifest.permission.READ_EXTERNAL_STORAGE )
                        .withListener(new MultiplePermissionsListener()
                        {
                            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if(report.areAllPermissionsGranted()){

                                    mCallPhotoGallary();
                                }

                                // check for permanent denial of any permission
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    // permission is denied permenantly, navigate user to app settings
                                    showAlert();
                                }
                            }
                            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                            {/* ... */

                                token.continuePermissionRequest();
                            }
                        }).check();
//            }
        }
    }

    //select image from gallery
    private void mCallPhotoGallary() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select a photo"), PICK_IMAGE_REQUEST);

//        try {
//            PhotoPicker.builder()
//                    .setPhotoCount(1)
//                    .setShowCamera(true)
//                    .setShowGif(false)
//                    .setPreviewEnabled(false)
//                    .start(this, PICK_IMAGE_REQUEST);
//
//        } catch (Exception e) {
//            //To catch unknown Null pointer exception inside library
//            e.printStackTrace();
//        }
    }

    //take photo through camera
    private void takePhoto() {

        // set a image file path
        imageFilePath = ImageUtils.getFile().getAbsolutePath();
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        imageUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", ImageUtils.getFile());// return content:///..
        //imageUri=Uri.fromFile(getFile()); // returns file:///...
        Log.d("@ file uri :", imageUri.toString());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); //API >24
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case StateListRequestCode:

                    if (!data.getExtras().getString("StateName").equals("") && !data.getExtras().getString("StateId").equals("")) {
                        strStateId = data.getExtras().getString("StateId");
                        Log.d("State id: ", strStateId);
                        strStateName = data.getExtras().getString("StateName");
                        Log.d("State Name: ", strStateName);

                        spnrStateList.setText(strStateName);
                        spnrCityList.setText("");
                        strCityId = "";
                        strCityName = "";
                    }

                    break;

                case CityListRequestCode:

                    if (!data.getExtras().getString("CityName").equals("") && !data.getExtras().getString("CityId").equals("")) {
                        strCityId = data.getExtras().getString("CityId");
                        Log.d("CityId id: ", strCityId);
                        strCityName = data.getExtras().getString("CityName");
                        Log.d("CityName: ", strCityName);

                        spnrCityList.setText(strCityName);
                    }

                    break;

                case REQUEST_IMAGE_CAPTURE:
                    dialog = new ProgressDialog(context);
                    try {

                        dialog.setMessage("Image processing...");
                        dialog.show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try
                                {
                                    Bitmap fullImage = MediaStore.Images.Media.getBitmap(getContentResolver(),imageUri);

                                    InputStream input = context.getContentResolver().openInputStream(imageUri);
                                    ExifInterface ei;
                                    if (Build.VERSION.SDK_INT > 23)
                                        ei = new ExifInterface(input);
                                    else
                                        ei = new ExifInterface(imageUri.getPath());

                                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                                    switch (orientation) {
                                        case ExifInterface.ORIENTATION_ROTATE_90:
                                            fullImage = rotateImage(fullImage, 90);
                                            break;
                                        case ExifInterface.ORIENTATION_ROTATE_180:
                                            fullImage = rotateImage(fullImage, 180);
                                            break;
                                        case ExifInterface.ORIENTATION_ROTATE_270:
                                            fullImage = rotateImage(fullImage, 270);
                                            break;
                                        default:
                                            break;
                                    }


                                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                    fullImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                                    byte[] byteArray = bytes.toByteArray();
                                    Bitmap scaledBitmap=ImageUtils.decodeSampledBitmapFromResource(byteArray,400,400);
                                    file = ImageUtils.saveImage(scaledBitmap, context);

                                    if (file != null) {
                                        Log.d("d", "Camera image file:" + file);
                                        if (strImage == "Gallery1") {
                                            strGalleryOnePath = imageFilePath;
                                            fileGalleryOne = file;
                                            strgalleryimageid1 = "0";
                                            Log.d("d", "Camera Gallery One file:" + fileGalleryOne);
                                            strGalleryPath = strGalleryOnePath;
                                            Picasso.get().load(fileGalleryOne).into(ivVehicle1);

                                        } else if (strImage == "Gallery2") {
                                            strGalleryTwoPath = imageFilePath;
                                            fileGalleryTwo = file;
                                            strgalleryimageid2 = "0";
                                            Log.d("d", "Camera Gallery Two file:" + fileGalleryTwo);
                                            strGalleryPath = strGalleryTwoPath;
                                            Picasso.get().load(fileGalleryTwo).into(ivVehicle2);

                                        } else if (strImage == "Gallery3") {
                                            strGalleryThreePath = imageFilePath;
                                            fileGalleryThree = file;
                                            strgalleryimageid3 = "0";
                                            Log.d("d", "Camera Gallery Three file:" + fileGalleryThree);
                                            strGalleryPath = strGalleryThreePath;
                                            Picasso.get().load(fileGalleryThree).into(ivVehicle3);

                                        } else if (strImage == "Tax") {
                                            fileTax = file;
                                            Log.d("d", "Camera Tax file:" + fileTax);
                                            Picasso.get().load(fileTax).into(ivTaxToken);

                                        } else if (strImage == "Puc") {
                                            filePuc = file;
                                            Log.d("d", "Camera Puc file:" + filePuc);
                                            Picasso.get().load(filePuc).into(ivPuc);

                                        } else if (strImage == "License") {
                                            fileLicense = file;
                                            Log.d("d", "Camera License file: " + fileLicense);
                                            Picasso.get().load(fileLicense).into(ivLicensePlate);
                                        }else if (strImage.equalsIgnoreCase("RegionalTransportPermit")){
                                            fileRegionalTransportPermit = file;
                                            Log.d("d", "fileRegionalTransportPermit"+fileRegionalTransportPermit);
                                            Picasso.get().load(fileRegionalTransportPermit).into(ivRegionalTransportPermit);

                                        }

                                        else if (strImage.equalsIgnoreCase("RegistrationCertificateFront")){
                                            fileRegistrationCertificateFront = file;
                                            Log.d("d", "fileRegistrationCertificate"+fileRegistrationCertificateFront);
                                            Picasso.get().load(fileRegistrationCertificateFront).into(ivRegistrationCertificateFront);

                                        }
                                        else if (strImage.equalsIgnoreCase("RegistrationCertificateBack")){
                                            fileRegistrationCertificateBack = file;
                                            Log.d("d", "fileRegistrationCertificate"+fileRegistrationCertificateBack);
                                            Picasso.get().load(fileRegistrationCertificateBack).into(ivRegistrationCertificateBack);

                                        }


                                        else if (strImage.equalsIgnoreCase("FitnessCertificate")){
                                            fileFitnessCertificate = file;
                                            Log.d("d", "fileFitnessCertificate"+fileFitnessCertificate);
                                            Picasso.get().load(fileFitnessCertificate).into(ivFitnessCertificate);

                                        }else if (strImage.equalsIgnoreCase("InsuranceCertificate")){
                                            fileInsuranceCertificate = file;
                                            Log.d("d", "fileInsuranceCertificate"+fileInsuranceCertificate);
                                            Picasso.get().load(fileInsuranceCertificate).into(ivInsuranceCertificate);
                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(context, "Failed saving!", Toast.LENGTH_SHORT).show();
                                }
                                finally{
                                    dialog.dismiss();
                                }
                            }
                        }, 4000);  // 4 sec to allow processing of image captured


                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, "Failed camera!", Toast.LENGTH_SHORT).show();
                    }

                    break;

                case PICK_IMAGE_REQUEST:

                    try {

//                        ArrayList<String> photos = data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);

                        BufferedInputStream bufferedInputStream;
                        Bitmap bmp;

                        if(data != null) {
                            InputStream inputStream = context.getContentResolver().openInputStream(Objects.requireNonNull(data.getData()));
                            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...

                            bufferedInputStream = new BufferedInputStream(inputStream);
                            bmp = BitmapFactory.decodeStream(bufferedInputStream);

                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                            byte[] byteArray = bytes.toByteArray();
                            Bitmap scaledBitmap = ImageUtils.decodeSampledBitmapFromResource(byteArray, 400, 400);
                            file = ImageUtils.saveImageGallery(scaledBitmap, context);
                        }



                        if (strImage == "Gallery1") {
//                            strGalleryOnePath = "file://" + photos.get(0);
//                            Log.d("d", "Gallery 1 path : " + strGalleryOnePath);
//                            strSelectedGalOne = photos.get(0);
//                            Log.d("d", "Gallery image 1 to be uploaded: " + strSelectedGalOne);
                            fileGalleryOne = file;
                            strgalleryimageid1 = "0";
                            Log.d("d", "Gallery 1 Image File::" + fileGalleryOne);
                            strGalleryPath = file.getAbsolutePath();
                            Picasso.get().load(file).resize(200, 200).into(ivVehicle1);

                        } else if (strImage == "Gallery2") {
//                            strGalleryTwoPath = "file://" + photos.get(0);
//                            Log.d("d", "Gallery 2 path : " + strGalleryTwoPath);
//                            strSelectedGalTwo = photos.get(0);
//                            Log.d("d", "Gallery image 2 to be uploaded: " + strSelectedGalTwo);
                            fileGalleryTwo = file;
                            strgalleryimageid2 = "0";
                            Log.d("d", "Gallery 2 Image File::" + fileGalleryTwo);
                            strGalleryPath = file.getAbsolutePath();
                            Picasso.get().load(fileGalleryTwo).resize(200, 200).into(ivVehicle2);

                        } else if (strImage == "Gallery3") {
//                            strGalleryThreePath = "file://" + photos.get(0);
//                            Log.d("d", "Gallery 3 path : " + strGalleryThreePath);
//                            strSelectedGalThree = photos.get(0);
//                            Log.d("d", "Gallery image 3 to be uploaded: " + strSelectedGalThree);
                            fileGalleryThree = file;
                            strgalleryimageid3 = "0";
                            Log.d("d", "Gallery 3 Image File::" + fileGalleryThree);
                            strGalleryPath = file.getAbsolutePath();
                            Picasso.get().load(fileGalleryThree).resize(200, 200).into(ivVehicle3);

                        } else if (strImage == "Tax") {
//                            strTaxPath = "file://" + photos.get(0);
//                            Log.d("d", "Tax path : " + strTaxPath);
//                            strSelectedTax = photos.get(0);
                            Log.d("d", "Tax to be uploaded: " + strSelectedTax);
                            fileTax = file;
                            Log.d("d", "Tax Image File::" + fileTax);

                            Picasso.get().load(file).resize(200, 200).into(ivTaxToken);

                        } else if (strImage == "Puc") {
//                            strPcuPath = "file://" + photos.get(0);
//                            Log.d("d", "Puc path : " + strPcuPath);
//                            strSelectedPcu = photos.get(0);
//                            Log.d("d", "Puc to be uploaded: " + strSelectedPcu);
                            filePuc = file;
                            Log.d("d", "Puc Image File::" + filePuc);

                            Picasso.get().load(file).resize(200, 200).into(ivPuc);

                        } else if (strImage == "License") {
//                            strLicenseIamgePath = "file://" + photos.get(0);
//                            Log.d("d", "License image path : " + strLicenseIamgePath);
//                            strSelectedLicense = photos.get(0);
//                            Log.d("d", "License to be uploaded: " + strSelectedLicense);
                            fileLicense = file;
                            Log.d("d", "License Image File::" + fileLicense);

                            Picasso.get().load(file).resize(200, 200).into(ivLicensePlate);
                        }else if (strImage.equalsIgnoreCase("RegionalTransportPermit")){
//                            strRegionalTransportImgPath = "file://" + photos.get(0);
//                            Log.d("d", "strRegionalTransportImgPath : " + strRegionalTransportImgPath);
//                            strSelectedRegionalTransport = photos.get(0);
//                            Log.d("d", "strSelectedRegionalTransport" + strSelectedRegionalTransport);
                            fileRegionalTransportPermit = file;
                            Log.d("d", "fileRegionalTransportPermit" + fileRegionalTransportPermit);

                            Picasso.get().load(file).resize(200, 200).into(ivRegionalTransportPermit);

                        }


                        else if (strImage.equalsIgnoreCase("RegistrationCertificateFront")){
//                            strRegistrationCertificateImgPathFront = "file://" + photos.get(0);
//                            Log.d("d", "strRegistrationCertificateImgPath : " + strRegistrationCertificateImgPathFront);
//                            strSelectedRegistrationCertificate = photos.get(0);
//                            Log.d("d", "strSelectedRegistrationCertificate" + strSelectedRegistrationCertificate);
                            fileRegistrationCertificateFront = file;
                            Log.d("d", "fileRegistrationCertificate" + fileRegistrationCertificateFront);

                            Picasso.get().load(file).resize(200, 200).into(ivRegistrationCertificateFront);

                        }

                        else if (strImage.equalsIgnoreCase("RegistrationCertificateBack")){
//                            strRegistrationCertificateImgPathBack = "file://" + photos.get(0);
//                            Log.d("d", "strRegistrationCertificateImgPath : " + strRegistrationCertificateImgPathBack);
//                            strSelectedRegistrationCertificate = photos.get(0);
//                            Log.d("d", "strSelectedRegistrationCertificate" + strSelectedRegistrationCertificate);
                            fileRegistrationCertificateBack = file;
                            Log.d("d", "fileRegistrationCertificate" + fileRegistrationCertificateBack);

                            Picasso.get().load(file).resize(200, 200).into(ivRegistrationCertificateBack);

                        }


                        else if (strImage.equalsIgnoreCase("FitnessCertificate")){

//                            strFitnessCertificateImgPath = "file://" + photos.get(0);
//                            Log.d("d", "strFitnessCertificateImgPath : " + strFitnessCertificateImgPath);
//                            strSelectedFitnessCertificate = photos.get(0);
//                            Log.d("d", "strSelectedFitnessCertificate" + strSelectedFitnessCertificate);
                            fileFitnessCertificate = file;
                            Log.d("d", "fileFitnessCertificate" + fileFitnessCertificate);

                            Picasso.get().load(file).resize(200, 200).into(ivFitnessCertificate);


                        }else if (strImage.equalsIgnoreCase("InsuranceCertificate")){

//                            strInsuranceCertificateImgPath = "file://" + photos.get(0);
//                            Log.d("d", "strInsuranceCertificateImgPath : " + strInsuranceCertificateImgPath);
//                            strSelectedInsuranceCertificate = photos.get(0);
//                            Log.d("d", "strSelectedInsuranceCertificate" + strSelectedInsuranceCertificate);
                            fileInsuranceCertificate = file;
                            Log.d("d", "fileInsuranceCertificate" + fileInsuranceCertificate);

                            Picasso.get().load(file).resize(200, 200).into(ivInsuranceCertificate);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    @Override
    public void MoreVehicleInfo(VehicleTypeModel mvehicleTypeModel) {

        vehicleTypeModel = mvehicleTypeModel;

        if (!"".equals(vehicleTypeModel.getState_id()) && !"null".equals(vehicleTypeModel.getState_id())) {
            strStateId = vehicleTypeModel.getState_id();
        }

        if (!"".equals(vehicleTypeModel.getCity_id()) && !"null".equals(vehicleTypeModel.getCity_id())) {
            strCityId = vehicleTypeModel.getCity_id();
        }

        if (!"".equals(vehicleTypeModel.getState_name()) && !"null".equals(vehicleTypeModel.getState_name())) {
            spnrStateList.setText(vehicleTypeModel.getState_name());
        }

        if (!"".equals(vehicleTypeModel.getCity_name()) && !"null".equals(vehicleTypeModel.getCity_name())) {
            spnrCityList.setText(vehicleTypeModel.getCity_name());
        }


        if (!"".equals(vehicleTypeModel.getHourly_rate()) && !"null".equals(vehicleTypeModel.getHourly_rate())) {
            tvHourlyRate.setText(vehicleTypeModel.getHourly_rate());
        }

        if (!"".equals(vehicleTypeModel.getVehicle_type_name()) && !"null".equals(vehicleTypeModel.getVehicle_type_name())) {
            spnrVehicleType.setText(vehicleTypeModel.getVehicle_type_name());
        }


        if (!"".equals(vehicleTypeModel.getAc_available()) && !"null".equals(vehicleTypeModel.getAc_available())) {
            if ("Y".equals(vehicleTypeModel.getAc_available())) {
                ivACCheck.setImageResource(R.drawable.check);
                strACCheck = "Y";

            } else {
                ivACCheck.setImageResource(R.drawable.uncheck);
                strACCheck = "N";
            }
        }


        if (vehicleTypeModel.getNight_charge_available().equalsIgnoreCase("Y")) {


            ivCheckNightChargesAvailable.setImageResource(R.drawable.check);
            isNightChargeSelected = true;
            strNightChargesCheck = "Y";
            rlNightChargesRate.setVisibility(View.VISIBLE);


            if (!TextUtils.isEmpty(vehicleTypeModel.getNight_rate())) {
                etNightChargesRate.setText(vehicleTypeModel.getNight_rate());
            }
        }else {
        }


        if (!TextUtils.isEmpty(vehicleTypeModel.getToll_parking_charge())){

            if (vehicleTypeModel.getToll_parking_charge().equalsIgnoreCase("I")){
                spnrParkingCharges.setSelection(0);
            }else if (vehicleTypeModel.getToll_parking_charge().equalsIgnoreCase("A")){
                spnrParkingCharges.setSelection(1);
            }
        }

        if (!"".equals(vehicleTypeModel.getOther_pickup_location()) && !"null".equals(vehicleTypeModel.getOther_pickup_location())) {
            if ("Y".equals(vehicleTypeModel.getOther_pickup_location())) {
                ivOtherLocCheck.setImageResource(R.drawable.check);
                strOtherLoc = "Y";
            } else {
                ivOtherLocCheck.setImageResource(R.drawable.uncheck);
                strOtherLoc = "N";
            }
        }

        if (!"".equals(vehicleTypeModel.getOvertime_available()) && !"null".equals(vehicleTypeModel.getOvertime_available())) {
            if ("Y".equals(vehicleTypeModel.getOvertime_available())) {
                ivCheck.setImageResource(R.drawable.check);
                strOvertimeAvailableCheck = "Y";
                //code to be added
                if (!"".equals(vehicleTypeModel.getHourly_overtime_rate()) && !"null".equals(vehicleTypeModel.getHourly_overtime_rate())) {
                    // tvHourlyOvertimeRate.setEnabled(true);
                    llHourlyOvertimeLayout.setVisibility(View.VISIBLE);
                    tvHourlyOvertimeRate.setText(vehicleTypeModel.getHourly_overtime_rate());
                }
            } else if ("N".equals(vehicleTypeModel.getOvertime_available())) {
                ivCheck.setImageResource(R.drawable.uncheck);
                strOvertimeAvailableCheck = "N";
                //code to be added
                llHourlyOvertimeLayout.setVisibility(View.GONE);
                //  tvHourlyOvertimeRate.setEnabled(false);
            }
        }

        if (!"".equals(vehicleTypeModel.getYear()) && !"null".equals(vehicleTypeModel.getYear())) {
            tvYrOfManufacture.setText(vehicleTypeModel.getYear());
        }

        if (!"".equals(vehicleTypeModel.getKm_travelled()) && !"null".equals(vehicleTypeModel.getKm_travelled())) {
            tvTotalKm.setText(vehicleTypeModel.getKm_travelled());
        }


        if (!"".equals(vehicleTypeModel.getDescription()) && !"null".equals(vehicleTypeModel.getDescription())) {
            tvSpclInfo.setText(vehicleTypeModel.getDescription());
        }

        if (!"".equals(vehicleTypeModel.getLicense_plate_no()) && !"null".equals(vehicleTypeModel.getLicense_plate_no())) {
            tvLicenseno.setText(vehicleTypeModel.getLicense_plate_no());
        }

        if (!"".equals(vehicleTypeModel.getLicense_plate_image()) && !"null".equals(vehicleTypeModel.getLicense_plate_image())) {
            Picasso.get().load(vehicleTypeModel.getLicense_plate_image()).into(ivLicensePlate);
        }

        if (!"".equals(vehicleTypeModel.getTax_token_image()) && !"null".equals(vehicleTypeModel.getTax_token_image())) {
            Picasso.get().load(vehicleTypeModel.getTax_token_image()).into(ivTaxToken);
        }

        if (!"".equals(vehicleTypeModel.getPcu_paper_image()) && !"null".equals(vehicleTypeModel.getPcu_paper_image())) {
            Picasso.get().load(vehicleTypeModel.getPcu_paper_image()).into(ivPuc);
        }


        if (!TextUtils.isEmpty(vehicleTypeModel.getValid_permit())){
            Picasso.get().load(vehicleTypeModel.getValid_permit()).into(ivRegionalTransportPermit);
        }


        if (!TextUtils.isEmpty(vehicleTypeModel.getRegistration_certificate())){
            Picasso.get().load(vehicleTypeModel.getRegistration_certificate()).into(ivRegistrationCertificateFront);
        }

        if (!TextUtils.isEmpty(vehicleTypeModel.getRegistration_certificate_back())){
            Picasso.get().load(vehicleTypeModel.getRegistration_certificate_back()).into(ivRegistrationCertificateBack);
        }

        if (!TextUtils.isEmpty(vehicleTypeModel.getFitness_certificate())){
            Picasso.get().load(vehicleTypeModel.getFitness_certificate()).into(ivFitnessCertificate);
        }


        if (!TextUtils.isEmpty(vehicleTypeModel.getInsurance_certificate())){
            Picasso.get().load(vehicleTypeModel.getInsurance_certificate()).into(ivInsuranceCertificate);
        }


        if (!"".equals(vehicleTypeModel.getAc_hourly_min_rate()) && !"null".equals(vehicleTypeModel.getAc_hourly_min_rate())) {
            strACMin = vehicleTypeModel.getAc_hourly_min_rate();
        }

        if (!"".equals(vehicleTypeModel.getAc_hourly_max_rate()) && !"null".equals(vehicleTypeModel.getAc_hourly_max_rate())) {
            strACMax = vehicleTypeModel.getAc_hourly_max_rate();
        }

        if (!"".equals(vehicleTypeModel.getAc_hourly_overtime_min_rate()) && !"null".equals(vehicleTypeModel.getAc_hourly_overtime_min_rate())) {
            strOvertimeACMin = vehicleTypeModel.getAc_hourly_overtime_min_rate();
        }

        if (!"".equals(vehicleTypeModel.getAc_hourly_overtime_max_rate()) && !"null".equals(vehicleTypeModel.getAc_hourly_overtime_max_rate())) {
            strOvertimeACMax = vehicleTypeModel.getAc_hourly_overtime_max_rate();
        }

        if (!"".equals(vehicleTypeModel.getNonac_hourly_min_rate()) && !"null".equals(vehicleTypeModel.getNonac_hourly_min_rate())) {
            strNonACMin = vehicleTypeModel.getNonac_hourly_min_rate();
        }

        if (!"".equals(vehicleTypeModel.getNonac_hourly_max_rate()) && !"null".equals(vehicleTypeModel.getNonac_hourly_max_rate())) {
            strNonACMax = vehicleTypeModel.getNonac_hourly_max_rate();
        }

        if (!"".equals(vehicleTypeModel.getNonac_hourly_overtime_min_rate()) && !"null".equals(vehicleTypeModel.getNonac_hourly_overtime_min_rate())) {
            strOvertimeNonACMin = vehicleTypeModel.getNonac_hourly_overtime_min_rate();
        }

        if (!"".equals(vehicleTypeModel.getNonac_hourly_overtime_max_rate()) && !"null".equals(vehicleTypeModel.getNonac_hourly_overtime_max_rate())) {
            strOvertimeNonACMax = vehicleTypeModel.getNonac_hourly_overtime_max_rate();
        }

        if (!"".equals(vehicleTypeModel.getVehicle_type_id()) && !"null".equals(vehicleTypeModel.getVehicle_type_id())) {
            strVehicleTypeId = vehicleTypeModel.getVehicle_type_id();
            Log.d("*Edit Vehicle type id*", strVehicleTypeId);
        }

        listOfVehicleGallery = vehicleTypeModel.getArr_VehicleGalleryModel();
        listOfVehicleImage = new ArrayList<String>();
        listOfSelectedDefaultImage = new ArrayList<String>();
        for (int i = 0; i < listOfVehicleGallery.size(); i++) {
            listOfVehicleImage.add(listOfVehicleGallery.get(i).getImage_file());
            listOfSelectedDefaultImage.add(listOfVehicleGallery.get(i).getIs_default());
            Log.d("d", "Vehicle arraylist image:: " + listOfVehicleImage.get(i));
            Log.d("d", "Vehicle gallery default selection: " + listOfSelectedDefaultImage.get(i));

            if ("Y".equalsIgnoreCase(listOfVehicleGallery.get(i).getIs_default())){
                Log.d("d", "Default image id : " + listOfVehicleGallery.get(i).getId());
            }
        }

        if (listOfVehicleImage.size() > 0) {
            for (int j = 0; j < listOfVehicleImage.size(); j++) {
                if (j == 0) {
                    if (!"".equals(listOfVehicleGallery.get(j).getImage_file()) && !"null".equals(listOfVehicleGallery.get(j).getImage_file())) {
                        strGalleryOne = listOfVehicleGallery.get(j).getImage_file();
//                        strDefaultImageId = listOfVehicleGallery.get(j).getId();
                        Log.d("d", "~~~~strGalleryOne~~~~~~" + strGalleryOne);
                        strGalleryPath = listOfVehicleGallery.get(j).getImage_file();
                        Picasso.get().load(listOfVehicleGallery.get(j).getImage_file()).into(ivVehicle1);
                    }

                    if ("Y".equalsIgnoreCase(listOfVehicleGallery.get(j).getIs_default()) && !"".equals(listOfVehicleGallery.get(j).getIs_default())
                            && !"null".equals(listOfVehicleGallery.get(j).getIs_default())) {
                        ibDefaultIamgeOne.setImageResource(R.drawable.vehicle_info_blank_image_select_fill);
                        ibDefaultIamgeTwo.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                        ibDefaultIamgeThree.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                        strDefaultImageId = listOfVehicleGallery.get(j).getId();
                        strSetGallerySelected = "1";
                    }

                } else if (j == 1) {
                    if (!"".equals(listOfVehicleGallery.get(j).getImage_file()) && !"null".equals(listOfVehicleGallery.get(j).getImage_file())) {
                        strGalleryTwo = listOfVehicleGallery.get(j).getImage_file();
//                        strDefaultImageId = listOfVehicleGallery.get(j).getId();
                        strGalleryPath = listOfVehicleGallery.get(j).getImage_file();
                        Picasso.get().load(listOfVehicleGallery.get(j).getImage_file()).into(ivVehicle2);
                    }

                    if ("Y".equalsIgnoreCase(listOfVehicleGallery.get(j).getIs_default()) && !"".equals(listOfVehicleGallery.get(j).getIs_default())
                            && !"null".equals(listOfVehicleGallery.get(j).getIs_default())) {
                        ibDefaultIamgeOne.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                        ibDefaultIamgeTwo.setImageResource(R.drawable.vehicle_info_blank_image_select_fill);
                        ibDefaultIamgeThree.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                        strDefaultImageId = listOfVehicleGallery.get(j).getId();
                        strSetGallerySelected = "2";
                    }

                    if(!TextUtils.isEmpty(listOfVehicleGallery.get(j).getImage_file()) ){
                        iv_delete_icon2.setVisibility(View.VISIBLE);
                        image_id2 = listOfVehicleGallery.get(j).getId();
                        user_vehicle_id = listOfVehicleGallery.get(j).getUser_vehicle_id();
                    }

                } else if (j == 2) {
                    if (!"".equals(listOfVehicleGallery.get(j).getImage_file()) && !"null".equals(listOfVehicleGallery.get(j).getImage_file())) {
                        strGalleryThree = listOfVehicleGallery.get(j).getImage_file();
//                        strDefaultImageId = listOfVehicleGallery.get(j).getId();
                        strGalleryPath = listOfVehicleGallery.get(j).getImage_file();
                        Picasso.get().load(listOfVehicleGallery.get(j).getImage_file()).into(ivVehicle3);
                    }

                    if ("Y".equalsIgnoreCase(listOfVehicleGallery.get(j).getIs_default()) && !"".equals(listOfVehicleGallery.get(j).getIs_default())
                            && !"null".equals(listOfVehicleGallery.get(j).getIs_default())) {
                        ibDefaultIamgeOne.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                        ibDefaultIamgeTwo.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                        ibDefaultIamgeThree.setImageResource(R.drawable.vehicle_info_blank_image_select_fill);
                        strDefaultImageId = listOfVehicleGallery.get(j).getId();
                        strSetGallerySelected = "3";
                    }

                    if(!TextUtils.isEmpty(listOfVehicleGallery.get(j).getImage_file()) ){
                        iv_delete_icon3.setVisibility(View.VISIBLE);
                        image_id3 = listOfVehicleGallery.get(j).getId();
                        user_vehicle_id = listOfVehicleGallery.get(j).getUser_vehicle_id();
                    }
                }
            }

        }


        listOfPickUpLocation = vehicleTypeModel.getArr_PickUpLocationModel();
        listOfSelectedPickUpLocation = new ArrayList<String>();
        for (int i = 0; i < listOfPickUpLocation.size(); i++) {

            if (i == 0) {
                tvpickuploc_addmore.setText(listOfPickUpLocation.get(i).getPickup_location());
                listOfSelectedPickUpLocation.add(listOfPickUpLocation.get(i).getPickup_location());
            } else {
                listOfSelectedPickUpLocation.add(listOfPickUpLocation.get(i).getPickup_location());
            }
        }

        System.out.println("listOfSelectedPickUpLocation: " + listOfSelectedPickUpLocation.size());

        if (listOfSelectedPickUpLocation.size() > 0) {
            StaticClass.setAl_Location_company(listOfSelectedPickUpLocation);
            AddStopOverMethod();
        }


        if (cd.isConnectingToInternet()) {
            new VehicleMake_Webservice().vehicleMakeWebservice(activity, CompanyEditVehicleActivity.this);

        } else {
            new CustomToast(activity, getResources().getString(R.string.Network_not_availabl));

        }
    }

    @Override
    public void VehicleMakeList(final ArrayList<VehicleTypeModel> arrListVehicleMake) {

        if (arrListVehicleMake != null) {
            listOfVehicleMake = new ArrayList<String>();
            if (arrListVehicleMake.size() > 0) {
                for (int x = 0; x < arrListVehicleMake.size(); x++) {
                    listOfVehicleMake.add(arrListVehicleMake.get(x).getTitle());
                }
                Log.d("d", "Array list vehicle make size::" + arrListVehicleMake.size());
                arrayAdapterVehicleMake = new ArrayAdapter<String>(context, R.layout.countryitem, listOfVehicleMake);
                arrayAdapterVehicleMake.setDropDownViewResource(R.layout.simpledropdownitem);
                //  spnrVehicleMake.setPrompt(getResources().getString(R.string.tvPleaseselectvehicleMake));
                spnrVehicleMake.setAdapter(arrayAdapterVehicleMake);

                //spnrVehicleMake.setSelection(0);
                Log.d("d", "**listOfVehicleMake**" + listOfVehicleMake);
                Log.d("d", "**vehicleTypeModel.getBrand_name()**" + vehicleTypeModel.getBrand_name());

                for (int i = 0; i < arrListVehicleMake.size(); i++) {
                    if (arrListVehicleMake.get(i).getId().equalsIgnoreCase(vehicleTypeModel.getMake())) {
                        spnrVehicleMake.setSelection(i);
                    }
                }

                spnrVehicleMake.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                        strVehicleBrandId = arrListVehicleMake.get(position).getId();
                        Log.d("d", "Vehicle Brand Id: " + strVehicleBrandId);
                        strVehicleModelTitle = arrListVehicleMake.get(position).getTitle();
                        Log.d("d", "Vehicle brand code: " + strVehicleModelTitle);

                        strVehicleModelId = "";
                        new VehicleModel_Webservice().vehicleModelWebservice(activity, CompanyEditVehicleActivity.this, strVehicleBrandId);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }

        } else {
            new CustomToast(activity, getResources().getString(R.string.tvVehcileMakeNotFound));
        }
    }

    @Override
    public void VehicleModelList(final ArrayList<VehicleTypeModel> arrListVehicleModel) {

        if (arrListVehicleModel != null) {
            listOfVehicleModel = new ArrayList<String>();
            if (arrListVehicleModel.size() > 0) {
                for (int x = 0; x < arrListVehicleModel.size(); x++) {
                    listOfVehicleModel.add(arrListVehicleModel.get(x).getTitle());
                }

                Log.d("d", "Array list vehicle model size::" + arrListVehicleModel.size());

                arrayAdapterVehicleModel = new ArrayAdapter<String>(context, R.layout.countryitem, listOfVehicleModel);
                arrayAdapterVehicleModel.setDropDownViewResource(R.layout.simpledropdownitem);
                // spnrVehicleBrand.setPrompt(getResources().getString(R.string.tvPlsSelectVehicleBrand));
                spnrVehicleBrand.setAdapter(arrayAdapterVehicleModel);

                Log.d("d", "**listOfVehicleModel**" + listOfVehicleModel);
                Log.d("d", "**vehicleTypeModel.getModel_name()**" + vehicleTypeModel.getModel_name());

                for (int i = 0; i < arrListVehicleModel.size(); i++) {
                    if (arrListVehicleModel.get(i).getId().equalsIgnoreCase(vehicleTypeModel.getModel())) {
                        spnrVehicleBrand.setSelection(i);
                    }
                }


                spnrVehicleBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                        strVehicleModelId = arrListVehicleModel.get(position).getId();
                        Log.d("d", "Vehicle model id::" + strVehicleModelId);

                        strVehicleMakeId = arrListVehicleModel.get(position).getMake_id();
                        Log.d("d", "Vehicle make id::" + strVehicleMakeId);

                        strVehicleModelTitle = arrListVehicleModel.get(position).getTitle();
                        Log.d("d", "Vehicle model title: " + strVehicleModelTitle);

                        strVehicleTypeId = arrListVehicleModel.get(position).getVehicle_type_id();
                        Log.d("***Vehicle type id***", strVehicleTypeId);

                        strSittingCapacity = arrListVehicleModel.get(position).getSitting_capacity();
                        strLuggageCapacity = arrListVehicleModel.get(position).getLuggae_capacity();
                        Log.d("d", "***Sitting capacity***" + strSittingCapacity);
                        Log.d("d", "***Luggage capacity***" + strLuggageCapacity);


                        strACNightMin = arrListVehicleModel.get(position).getAc_min_night_rate();
                        strACNightMax = arrListVehicleModel.get(position).getAc_max_night_rate();
                        strNONACNightMin = arrListVehicleModel.get(position).getNonac_min_night_rate();
                        strNOnACNightMax = arrListVehicleModel.get(position).getNonac_max_night_rate();

                        //passenger count
                        intMaxPassenger = Integer.parseInt(strSittingCapacity);
                        listOfMaxPassenger = new ArrayList<Integer>();
                        for (int i = 0; i <= intMaxPassenger; i++) {
                            listOfMaxPassenger.add(i);
                        }

                        arrayAdapterMaxPassenger = new ArrayAdapter<Integer>(context, R.layout.countryitem, listOfMaxPassenger);
                        arrayAdapterMaxPassenger.setDropDownViewResource(R.layout.simpledropdownitem);
                        spnrMaxPassenger.setPrompt(getResources().getString(R.string.promtMaxPassenger));
                        spnrMaxPassenger.setAdapter(arrayAdapterMaxPassenger);

                        if (!"".equals(vehicleTypeModel.getMax_passenger()) && !"null".equals(vehicleTypeModel.getMax_passenger())) {

                            String max_passenger = vehicleTypeModel.getMax_passenger();

                            Log.d("max_passenger", max_passenger);
                            int pos = 0;


                            for (int i = 0; i < listOfMaxPassenger.size(); i++) {

                                if (max_passenger.equals(String.valueOf(listOfMaxPassenger.get(i)))) {
                                    pos = i;
                                }
                            }
                            spnrMaxPassenger.setSelection(pos);

                        }

                        // luggage
                        intMaxLuggage = Integer.parseInt(strLuggageCapacity);
                        listOfMaxLuggage = new ArrayList<Integer>();
                        for (int i = 1; i <= intMaxLuggage; i++) {
                            listOfMaxLuggage.add(i);
                        }

                        arrayAdapterMaxLuggage = new ArrayAdapter<Integer>(context, R.layout.countryitem, listOfMaxLuggage);
                        arrayAdapterMaxLuggage.setDropDownViewResource(R.layout.simpledropdownitem);
                        spnrMaxLuggage.setPrompt(getResources().getString(R.string.promptMaxLuggage));
                        spnrMaxLuggage.setAdapter(arrayAdapterMaxLuggage);


                        if (!"".equals(vehicleTypeModel.getMax_luggage()) && !"null".equals(vehicleTypeModel.getMax_luggage())) {

                            String max_luggage = vehicleTypeModel.getMax_luggage();

                            Log.d("max_luggage", max_luggage);
                            int pos = 0;


                            for (int i = 0; i < listOfMaxLuggage.size(); i++) {

                                if (max_luggage.equals(String.valueOf(listOfMaxLuggage.get(i)))) {
                                    pos = i;
                                }
                            }
                            spnrMaxLuggage.setSelection(pos);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                    }
                });
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

//         StaticClass.BottomProfileCompany = false;
//        if(StaticClass.BottomProfileCompany){
//            finish();
//        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }

        sharedPref.putBottomViewCompany(StaticClass.Menu_MyVehicles_company);
        bottomViewCompany = new BottomViewCompany();
        bottomViewCompany.BottomViewCompany(CompanyEditVehicleActivity.this, StaticClass.Menu_MyVehicles_company);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomViewCompany.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(CompanyEditVehicleActivity.this,
                CompanyEditVehicleActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                CompanyEditVehicleActivity.this.getResources().getString(R.string.yes),
                CompanyEditVehicleActivity.this.getResources().getString(R.string.no));
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();

    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(CompanyEditVehicleActivity.this, transitionflag);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) CompanyEditVehicleActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    @Override
    public void deleteImage(String status, String msg) {

        if(status.equalsIgnoreCase(StaticClass.SuccessResult)){

            final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(activity,
                    msg,
                    activity.getResources().getString(R.string.ok),"");
            alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialogYESNO.dismiss();
                    transitionflag = StaticClass.transitionflagBack;
                    finish();
                }
            });

            alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialogYESNO.dismiss();
                }
            });

            alertDialogYESNO.show();
        }

    }
}

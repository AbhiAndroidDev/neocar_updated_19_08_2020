package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.AddRatingModel;

import java.util.ArrayList;

/**
 * Created by parna on 13/9/18.
 */

public interface AddRatingInterface {

    void onAddRating(ArrayList<AddRatingModel> msg);
}

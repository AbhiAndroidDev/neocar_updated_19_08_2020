package com.neo.cars.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neo.cars.app.Interface.StateListInterface;
import com.neo.cars.app.SetGet.StateListModel;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.MessageText;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.StateList_Webservice;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;


import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class StateList extends AppCompatActivity implements StateListInterface {

    private RelativeLayout rlBackLayout;
    private int transitionflag = StaticClass.transitionflagNext;

    private EditText ET_ADDRESS_AUTOCOMPLETE;
    private LinearLayout LIST_ADDRESSES;

    private LinearLayout LIST_FAVOURITE;
    private Timer timer;
    boolean timerflag;
    private ArrayList<StateListModel> arr_listState;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;

    private ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_list);

        new AnalyticsClass(StateList.this);

        Initialize();
        Listener();

        if(cd.isConnectingToInternet()){
            new StateList_Webservice().stateListWebservice(StateList.this);

        }else{
            new CustomToast(this, MessageText.Network_not_availabl);
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }

    }

    private void Initialize(){

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.StateList));

        cd = new ConnectionDetector(this);
        rlBackLayout = findViewById(R.id.rlBackLayout);

        LIST_ADDRESSES=findViewById(R.id.LIST_ADDRESSES);

        ET_ADDRESS_AUTOCOMPLETE=findViewById(R.id.ET_ADDRESS_AUTOCOMPLETE);
        ET_ADDRESS_AUTOCOMPLETE.setMaxWidth(ET_ADDRESS_AUTOCOMPLETE.getWidth());

        ET_ADDRESS_AUTOCOMPLETE.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {

                LIST_ADDRESSES.removeAllViews();

                timerflag=false;
                try {
                    timer.cancel();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

                System.out.println("*****addTextChangedListener*****");
                try {
                    timer = new Timer();
                    timer.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            if(timerflag){
                                try {
                                    timer.cancel();
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }

                            }else{
                                timerflag=true;
                            }
                        }
                    },0, 1*500);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void Listener(){
        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                Intent resultintent=new Intent();
                resultintent.putExtra("StateName","");
                resultintent.putExtra("StateId","");
                setResult(StaticClass.StateLitRequestCode,resultintent);
                finish();
            }
        });
    }


    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        Intent resultintent=new Intent();
        resultintent.putExtra("StateName","");
        resultintent.putExtra("StateId","");
        setResult(StaticClass.StateLitRequestCode,resultintent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(StateList.this, transitionflag);

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
    }

    @Override
    public void StateList(ArrayList<StateListModel> arrlistState) {
        LIST_ADDRESSES.removeAllViews();
        arr_listState=new ArrayList<>();
        arr_listState=arrlistState;

        for(int i=0;i<arr_listState.size();i++)
        {
            LIST_ADDRESSES.addView(getView(i));
        }
    }

    public View getView(final int position){

        LayoutInflater inflater=getLayoutInflater();
        View rowView=inflater.inflate(R.layout.list_item, null,true);

      /*  Animation animation= AnimationUtils.loadAnimation(StateList.this,android.R.anim.slide_in_left);
        animation.setDuration(20);
        animation.setStartOffset(position*150);
        rowView.startAnimation(animation);*/

        TextView autotext=rowView.findViewById(R.id.autotext);

        autotext.setText(""+arr_listState.get(position).getName());

        rowView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                TextView addr=(TextView)v.findViewById(R.id.autotext);
                transitionflag = StaticClass.transitionflagBack;
                Intent resultintent=new Intent();
                resultintent.putExtra("StateName",arr_listState.get(position).getName());
                resultintent.putExtra("StateId",arr_listState.get(position).getId());
                setResult(Activity.RESULT_OK,resultintent);
                finish();

            }
        });

        return rowView;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) StateList.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}

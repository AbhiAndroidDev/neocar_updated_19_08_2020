package com.neo.cars.app.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.neo.cars.app.SharedPreference.ShareFcmDetails;


/**
 * Created by kamail on 22/9/16.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    private ShareFcmDetails sharedPrefDetails;

    @Override
    public void onTokenRefresh() {
        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        sharedPrefDetails = new ShareFcmDetails(this);

        if (sharedPrefDetails.getFcmId().equals("")) {
            sharedPrefDetails.setFcmId(token);
        }
    }
}
package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.CompanyDriverSave_interfae;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.DriverDataModel;
import com.neo.cars.app.SetGet.UserDocumentModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VehicleDriverAssociationWebService {

    private Activity activity;
    private Context mcontext;
    private String Status = "0", Msg = "", unique_id="", strUserDeleted="";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private DriverDataModel driverDataModel;
    private UserDocumentModel userDocumentModel;
    private JSONObject details;
    private ArrayList<DriverDataModel> arrlistDriverModel;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;
    private CompanyDriverSave_interfae companyDriverSave_interfae;


    public void vehicleDriverAssociationWebService(Context context, final Activity activity, CompanyDriverSave_interfae companyDriverSave_interfae, final String strDriverId, final String strBookingId) {

        this.activity = activity;
        this.mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        this.companyDriverSave_interfae = companyDriverSave_interfae;

        sharedPref = new SharedPrefUserDetails(mcontext);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();

        StringRequest userVehicleListRequest  = new StringRequest(Request.Method.POST, Urlstring.vehicle_driver_association,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response****", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(activity, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", sharedPref.getUserid());
                params.put("booking_id", strBookingId);
                params.put("driver_id", strDriverId);

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        userVehicleListRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(userVehicleListRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void Apiparsedata(String response){

        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("vehicle_driver_association").optString("user_deleted");
            Msg = jobj_main.optJSONObject("vehicle_driver_association").optString("message");
            Status= jobj_main.optJSONObject("vehicle_driver_association").optString("status");

            JSONArray details=jobj_main.optJSONObject("vehicle_driver_association").optJSONArray("details");
//            if (Status.equals(StaticClass.SuccessResult)){
//                arrlistDriver = new ArrayList<>();
//                for (int i=0; i<details.length(); i++){
//
//                    JSONObject jsonObject = details.optJSONObject(i);
//                    CompanyDriverListModel companyDriverListModel = new CompanyDriverListModel();
//                    companyDriverListModel.setDriver_id(jsonObject.optString("driver_id"));
//                    companyDriverListModel.setName(jsonObject.optString("name"));
//                    companyDriverListModel.setAge(jsonObject.optString("age"));
//                    companyDriverListModel.setContactno(jsonObject.optString("contact_no"));
//                    companyDriverListModel.setRecent_photograph(jsonObject.optString("recent_photograph"));
//
//                    arrlistDriver.add(companyDriverListModel);
//                }
//            }
        }catch (Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        }

        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            activity.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
                // ((UserVehicleList_Interface)).UserVehicleListInterface(arrlistVehicle);
//                companyDriverList_Interface.companyDriverList(arrlistDriver);
                companyDriverSave_interfae.companyDriverSave(Status, Msg);
            }
        }

    }
}

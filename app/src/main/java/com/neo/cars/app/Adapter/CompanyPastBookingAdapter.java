package com.neo.cars.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.neo.cars.app.CompanyAddRiderRatingActivity;
import com.neo.cars.app.CompanyBookingInformationActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.CompanyBookingListModel;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.CropCircleTransformation;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CompanyPastBookingAdapter extends RecyclerView.Adapter<CompanyPastBookingAdapter.MyViewHolder>  {

    private List<CompanyBookingListModel> myBookingModelList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircularImageViewBorder civUpcomingBookingPic;
        ImageView civ_mybookingstatus;
        CustomTitilliumTextViewSemiBold tvRiderRating, tvAddRiderRating;
        CustomTextviewTitilliumWebRegular tvFrom, tvTo, tvBooking, tvTravellerName, tvAssignDriverName, tvBookingRefrence;
        RelativeLayout rlParent;
        ImageView tvNeedsApproval;
        LinearLayout riderRatingll;

        public MyViewHolder(View view) {
            super(view);

            civUpcomingBookingPic= view.findViewById(R.id.civUpcomingBookingPic);
            civ_mybookingstatus= view.findViewById(R.id.civ_mybookingstatus);
            tvRiderRating = view.findViewById(R.id.tvRiderRating);
            tvAddRiderRating = view.findViewById(R.id.tvAddRiderRating);
            tvFrom = view.findViewById(R.id.tvFrom);
            tvTo = view.findViewById(R.id.tvTo);
            tvBooking = view.findViewById(R.id.tvBooking);
            tvTravellerName = view.findViewById(R.id.tvTravellerName);
            tvAssignDriverName = view.findViewById(R.id.tvAssignDriverName);
            tvBookingRefrence = view.findViewById(R.id.tvBookingRefrence);
            rlParent = view.findViewById(R.id.rlParent);

            tvNeedsApproval = view.findViewById(R.id.tvNeedsApproval);
            riderRatingll = view.findViewById(R.id.riderRatingll);
        }
    }

    public CompanyPastBookingAdapter(Context context,List<CompanyBookingListModel> myBookingModelList) {
        this.context = context;
        this.myBookingModelList = myBookingModelList;
    }

    @Override
    public CompanyPastBookingAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.company_ongoing_list_item, parent, false);
        return new CompanyPastBookingAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompanyPastBookingAdapter.MyViewHolder holder, final int position) {
        holder.tvFrom.setText(myBookingModelList.get(position).getPickup_location());
        holder.tvTo.setText(myBookingModelList.get(position).getDrop_location());
        holder.tvBooking.setText(myBookingModelList.get(position).getBooking_date());
        holder.tvBookingRefrence.setText(myBookingModelList.get(position).getBooking_id());
        if (!TextUtils.isEmpty(myBookingModelList.get(position).getDriver_name())) {
            holder.tvTravellerName.setText(myBookingModelList.get(position).getDriver_name());
        }else {
            holder.tvTravellerName.setVisibility(View.GONE);
            holder.tvAssignDriverName.setVisibility(View.VISIBLE);
        }
        Picasso.get().load(myBookingModelList.get(position).getVehicle_image()).transform(new CropCircleTransformation()).into(holder.civUpcomingBookingPic);



        if (myBookingModelList.get(position).getBooking_status().equalsIgnoreCase("2")){
            holder.civ_mybookingstatus.setVisibility(View.VISIBLE);

            holder.civ_mybookingstatus.setImageResource(R.drawable.ic_cancel);
            holder.tvAddRiderRating.setVisibility(View.GONE);
            holder.riderRatingll.setVisibility(View.GONE);

        }else if (myBookingModelList.get(position).getReview_added().equalsIgnoreCase("Y")){
                holder.civ_mybookingstatus.setVisibility(View.VISIBLE);
                holder.civ_mybookingstatus.setImageResource(R.drawable.my_vehicle_details_token_success_icon);
                holder.tvAddRiderRating.setVisibility(View.GONE);
                holder.riderRatingll.setVisibility(View.VISIBLE);
                holder.tvRiderRating.setText(myBookingModelList.get(position).getRider_rating());
            }else {
                holder.civ_mybookingstatus.setVisibility(View.VISIBLE);
                holder.civ_mybookingstatus.setImageResource(R.drawable.my_vehicle_details_token_success_icon);
                holder.tvAddRiderRating.setVisibility(View.VISIBLE);
                holder.riderRatingll.setVisibility(View.GONE);
            }


        holder.tvAddRiderRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addRatingIntent = new Intent(context, CompanyAddRiderRatingActivity.class);
                addRatingIntent.putExtra("rider_id", myBookingModelList.get(position).getCustomer_id());
                addRatingIntent.putExtra("booking_id", myBookingModelList.get(position).getBooking_id());
                context.startActivity(addRatingIntent);
            }
        });


        holder.rlParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent moreIntent = new Intent(context, CompanyBookingInformationActivity.class);
                moreIntent.putExtra("booking_id",   myBookingModelList.get(position).getBooking_id());
                moreIntent.putExtra("booking_Type", StaticClass.MyVehiclePast);
                context.startActivity(moreIntent);

            }
        });

    }


    @Override
    public int getItemCount() {
        return myBookingModelList.size();
    }
}

package com.neo.cars.app;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.neo.cars.app.Interface.BookingAvailabilityCheckInterface;
import com.neo.cars.app.Interface.BookingHours_Interface;
import com.neo.cars.app.Interface.CallBackCustomTimePicker;
import com.neo.cars.app.SetGet.BookingHoursModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.NoDefaultSpinner;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.BookingAvailablityCheckWebService;
import com.neo.cars.app.Webservice.BookingHours_Webservice;
import com.neo.cars.app.dialog.CustomTimePickerDialog1;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SearchActivity extends AppCompatActivity implements BookingHours_Interface,
        BookingAvailabilityCheckInterface, CallBackCustomTimePicker {

    private Context context;
    private Toolbar toolbar;
    private ConnectionDetector cd;
    private RelativeLayout rlBackLayout;
    private TextView tv_toolbar_title, tvSelectDate, tvPickUpTime, tvSelectDuration;
    private TextView tvSearchCity;
    private NoDefaultSpinner spnrFilterDuration;
    private int transitionflag = StaticClass.transitionflagNext;
    private String strAvailableFor = "", strBookingHourLabel = "", strBookingHourValue = "", strCityId = "", strCityName = "",
            isState = "", strStateId = "", strSelectDate = "", strSelectDate2Send = "", strPickUpTime="", strPickUpTime2Send="",  format = "",
            currentDate = "", currentTime = "", currentHour = "", currentMinute = "";
    private SpannableStringBuilder builder;
    private int start, end;
    private ArrayList<BookingHoursModel> arrlistBookingHours;
    private ArrayList<String> listOfBookingHour;
    private ArrayAdapter<String> arrayAdapterBookingHour;
    private Calendar mcalendar;
    private int day,month,year;
    private CustomTimePickerDialog1 customTimePickerDialog;
    private Button btnSearch;
    private String monthName;
    private RelativeLayout rlSearchCity;
    private SharedPrefUserDetails sharedPrefUser;
    private int selectedHour;
    private CustomDialog pdCusomeDialog;

    private BottomView bottomview = new BottomView();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        new AnalyticsClass(SearchActivity.this);
        initialize();


        if(cd.isConnectingToInternet()){
            new BookingHours_Webservice().bookinghoursWebservice(SearchActivity.this, strAvailableFor );

        }else{
            Intent i = new Intent(SearchActivity.this, NetworkNotAvailable.class);
            transitionflag = StaticClass.transitionflagBack;
            startActivity(i);
        }

        currentDate = getCurrentDate();
//        currentTime = getCurrentTime();
        strPickUpTime = getCurrentTime();
        Log.d("currentDate", currentDate);
        Log.d("strPickUpTime", strPickUpTime);
        currentHour = getCurrentHour();

        currentMinute = getCurrentMinute();

        Log.d("currentTime**", currentHour);
        Log.d("currentMinute**", currentMinute);

        listener();

    }

    private void initialize() {

        context = this;
        cd = new ConnectionDetector(this);

//        toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setDisplayShowHomeEnabled(false);
//
//        tv_toolbar_title =  findViewById(R.id.tv_toolbar_title);
//        tv_toolbar_title.setText(getResources().getString(R.string.tvBookingTitle));

//        rlBackLayout = findViewById(R.id.rlBackLayout);
//        rlBackLayout.setVisibility(View.GONE);

        btnSearch = findViewById(R.id.btnSearch);

        spnrFilterDuration = findViewById(R.id.spnrFilterDuration);
        tvSelectDate =  findViewById(R.id.tvSelectDate);
        tvSelectDuration =  findViewById(R.id.tvSelectDuration);
        tvPickUpTime =  findViewById(R.id.tvPickUpTime);

        rlSearchCity = findViewById(R.id.rlSearchCity);

        tvSearchCity = findViewById(R.id.tvSearchCity);

        mcalendar = Calendar.getInstance();
//        mcalendar.add(Calendar.DATE, +1);

        day=mcalendar.get(Calendar.DAY_OF_MONTH);
        year=mcalendar.get(Calendar.YEAR);
        month=mcalendar.get(Calendar.MONTH);

//        monthName =  mcalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

        sharedPrefUser = new SharedPrefUserDetails(SearchActivity.this);
        sharedPrefUser.putBottomViewCompany(StaticClass.Menu_Explore);

        bottomview.BottomView(SearchActivity.this, StaticClass.Menu_Explore);

    }

    private void listener() {


        tvPickUpTime.setText(setTimewith15MinInterval(Integer.parseInt(currentHour), Integer.parseInt(currentMinute)) );

        tvSelectDate.setText(currentDate);
//        tvPickUpTime.setText(strPickUpTime);

//        rlBackLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                transitionflag = StaticClass.transitionflagBack;
//                finish();
//            }
//        });

        tvSelectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                DateSelectFlag=false;
                DoBDialog();

            }
        });

        tvPickUpTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                strSelectDate = tvSelectDate.getText().toString();

                if (!TextUtils.isEmpty(strSelectDate)) {

                    Log.d("d", "***tvStartTime***");
                    customTimePickerDialog = new CustomTimePickerDialog1(SearchActivity.this, 24, 60, false);
                    customTimePickerDialog.show();
                }else {
                    new CustomToast(SearchActivity.this, "Please select date first");
                }
            }
        });

//        rlSearchCity.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                transitionflag=StaticClass.transitionflagNext;
//                Intent filterSearchCityIntent = new Intent(SearchActivity.this, SearchCityByLocationActivity.class);
//                startActivityForResult(filterSearchCityIntent, StaticClass.SearchCityRequestCode);
//            }
//        });


        tvSelectDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                setBottomSheetDuration();

            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                strSelectDate2Send = convertDateFormat(tvSelectDate.getText().toString());
                strSelectDate = tvSelectDate.getText().toString();

                strPickUpTime = tvPickUpTime.getText().toString();

                strPickUpTime2Send = convertTimeFormat(strPickUpTime);

                checkValidation();
            }
        });
    }

    private String setTimewith15MinInterval(int hour, int minute) {

        selectedHour = hour;
        hour += 2;


        //here minute have 4 value : 0 for 00, 1 for 15, 2 for 30, 3 for 45

        if(minute <15){
            minute = 15;
        }else if (minute > 14 && minute < 30){
            minute = 30;
        }else if (minute > 29 && minute < 45){
            minute = 45;
        }
        else if (minute > 44 && minute < 60){
            minute = 00;
            hour += 1;
        }

        //strPickUptime to send in webservice

        String time24="";
        if (hour < 10 && minute == 0){
            strPickUpTime = "0"+hour+":0"+minute;
        }else if (hour < 10){
            strPickUpTime = "0"+hour+":"+minute;
        }else if (minute == 0){
            strPickUpTime = hour+":0"+minute;
        }else{
            strPickUpTime = hour+":"+minute;
        }

        if (hour == 0) {
            hour += 12;
            format = "AM";

        } else if (hour == 12) {
            format = "PM";

        } else if (hour > 12) {
            hour -= 12;
            format = "PM";

        } else {
            format = "AM";
        }

//        strPickUpTime = strPickUpTime+" "+format;

        if (minute == 0){
            time24 = hour+":0"+minute+" "+format;
            if (hour < 10){
                time24 = "0"+hour+":0"+minute+" "+format;
            }
        }else if (hour < 10){
            time24 = "0"+hour+":"+minute+" "+format;
        }else {
            time24 = hour+":"+minute+" "+format;
        }
        return time24;
    }

    private void checkValidation() {

//        if (TextUtils.isEmpty(strCityId) && TextUtils.isEmpty(strStateId)) {
//            new CustomToast(SearchActivity.this, "Please select city or state");
//        }else

        if (TextUtils.isEmpty(strSelectDate)) {
            new CustomToast(SearchActivity.this, "Please select date");
        }else if (TextUtils.isEmpty(strPickUpTime)) {
            new CustomToast(SearchActivity.this, "Please select time");
        }else if(TextUtils.isEmpty(strBookingHourValue)){

            new CustomToast(SearchActivity.this, "Please select duration");

        }else if(NetWorkStatus.isNetworkAvailable(this)){

            new BookingAvailablityCheckWebService().bookingAvailablityCheck(this, strSelectDate2Send, strPickUpTime2Send);

        }else{
            Intent i = new Intent(SearchActivity.this, NetworkNotAvailable.class);
            transitionflag = StaticClass.transitionflagBack;
            startActivity(i);
        }
    }


    @Override
    public void BookingAvailabilityCheck(String status, String message) {

        if (status.equalsIgnoreCase(StaticClass.ErrorResult)){
            new CustomToast(this, message);

        }else if(status.equalsIgnoreCase(StaticClass.SuccessResult)){

            Intent applyintent = new Intent(SearchActivity.this, SearchResultActivity.class);
            applyintent.putExtra("FromDate", strSelectDate );
            applyintent.putExtra("Duration", strBookingHourValue );
            applyintent.putExtra("PickUpTime", strPickUpTime );
            applyintent.putExtra("MonthName", monthName );
            applyintent.putExtra("day", ""+day );
//                applyintent.putExtra("strCityId", strCityId );
            applyintent.putExtra("strCityId", "5583" ); //cityid fixed
            applyintent.putExtra("strStateId", strStateId );
//                applyintent.putExtra("CityName", strCityName );
            applyintent.putExtra("CityName", "Kolkata(West Bengal)" );
            startActivity(applyintent);

        }
    }

    private String getCurrentDate() {

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy");
        String formattedDate = df.format(c);
        Log.d("formattedDate", formattedDate.trim()); //format - 12 january 2020

        return formattedDate;
    }



    //Added 2 hours in current time
    private String getCurrentTime(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, 2);
        Date c = calendar.getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("hh:mm a", Locale.US);
        String currentTime = df.format(c);
        Log.d("formattedDate", currentTime.trim()); //format - 12 january 2020

        return currentTime;
    }


    public String convertDateFormat(String strDate)
    {
        String dateToSend = "";
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
            Date date = sdf.parse(strDate);
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            dateToSend = sdff.format(date);

        } catch (final Exception e) {
            e.printStackTrace();
        }
        return dateToSend;
    }

    public String convertTimeFormat(String strTIme)
    {
        String timeToSend = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.US);
            Date date = sdf.parse(strTIme);
            SimpleDateFormat sdff = new SimpleDateFormat("HH:mm", Locale.US);
            timeToSend = sdff.format(date);

        } catch (final Exception e) {
            e.printStackTrace();
        }

        return timeToSend;
    }


    private String getCurrentHour() {
        DateFormat dateFormat = new SimpleDateFormat("HH");
        Date date = new Date();
        return dateFormat.format(date);
    }


    private String getCurrentMinute(){
        DateFormat dateFormat = new SimpleDateFormat("mm");
        Date date = new Date();
        return dateFormat.format(date);
    }


    public void DoBDialog(){
//        monthName="";
//        day = 0;
        final DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Log.d("d", "monthOfYear:" +monthOfYear);

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date FromDate = sdf.parse(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    // Date ToDate = sdf.parse(tvToDate.getText().toString());

//                    tvSelectDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                    SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy");
//                    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                    String formattedDate = df.format(FromDate);
                    strSelectDate2Send = convertDateFormat(formattedDate);

                    tvSelectDate.setText(formattedDate);

                    if (currentDate.equalsIgnoreCase(strSelectDate)){
                        tvPickUpTime.setText(getResources().getString(R.string.tvPickUpTimeCaps));
                        strPickUpTime = "";
                    }

                    Log.d("d", "strSelectDate***"+strSelectDate);

                    day = dayOfMonth;

                    monthName =  StaticClass.MONTHS[monthOfYear];

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(SearchActivity.this, listener, year, month, day);
        dpDialog.getDatePicker().setMinDate(mcalendar.getTimeInMillis());
        dpDialog.show();
    }

    @Override
    public void BookingHoursList(ArrayList<BookingHoursModel> arr_listBookingHours) {
        arrlistBookingHours=new ArrayList<>();
        arrlistBookingHours=arr_listBookingHours;
//        BookingHoursList();


    }


    private void setBottomSheetDuration() {
        View view = LayoutInflater.from(this).inflate(R.layout.bottom_sheet_layout, null);
        BottomSheetDialog dialog = new BottomSheetDialog(this, R.style.BottomSheetDialog);
        dialog.setContentView(view);

        LinearLayout llSort = view.findViewById(R.id.ll_sort_option);
        llSort.removeAllViews();

        if(arrlistBookingHours != null)
        for(int i=0;i<arrlistBookingHours.size();i++){
            llSort.addView(addViewSort(i, dialog));
        }
        hideProgressDialog();
        dialog.show();
    }

    private View addViewSort(final int position, final BottomSheetDialog dialog) {

        View v;
        v = LayoutInflater.from(this).inflate(R.layout.bottom_sheet_duration, null);

        final TextView tvDuartion = v.findViewById(R.id.tv_duration);
        tvDuartion.setText(arrlistBookingHours.get(position).getLabel());

        tvDuartion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvSelectDuration.setText(arrlistBookingHours.get(position).getValue()+" HOURS");
                strBookingHourLabel = arrlistBookingHours.get(position).getLabel();
                strBookingHourValue = arrlistBookingHours.get(position).getValue();
                dialog.dismiss();
            }
        });

        return v;
    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(this ,getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void  BookingHoursList(){
        int pos=-1;
        if (arrlistBookingHours != null){
            listOfBookingHour = new ArrayList<String>();

            if(arrlistBookingHours.size() > 0){

                for (int x=0; x<arrlistBookingHours.size(); x++){
                    listOfBookingHour.add(arrlistBookingHours.get(x).getLabel());
                    if(arrlistBookingHours.get(x).getValue().equals(strBookingHourValue)){
                        pos=x;
                    }
                }
                Log.d("d", "Array list vehicle booking hour size::"+arrlistBookingHours.size());
                arrayAdapterBookingHour = new ArrayAdapter<String>(context, R.layout.countryitem, listOfBookingHour);
                arrayAdapterBookingHour.setDropDownViewResource(R.layout.simpledropdownitem);
//                spnrFilterDuration.setPrompt(getResources().getString(R.string.tvPlsSelectDuration));
                spnrFilterDuration.setAdapter(arrayAdapterBookingHour);

                spnrFilterDuration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {

                        tvSelectDuration.setVisibility(View.INVISIBLE);

                        strBookingHourLabel = arrlistBookingHours.get(position).getLabel();
                        strBookingHourValue = arrlistBookingHours.get(position).getValue();



//                        tvSelectDuration.setText(strBookingHourValue+" HOURS");


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                if(pos!=-1){
                    spnrFilterDuration.setSelection(pos);
                }
            }
        }else{
            new CustomToast(this, getResources().getString(R.string.tvDurationNtFound));
        }
    }

    @Override
    public void onButtonClick(TimePicker mTimePicker, int hour, int minute) {
        System.out.println("*****mTimePicker*****"+mTimePicker);
        System.out.println("*****hour*****"+hour);
        System.out.println("*****minute*****"+minute);

        Log.d("d", "***strPickUptime***"+strPickUpTime);

        selectedHour = hour;


        //here minute have 4 value : 0 for 00, 1 for 15, 2 for 30, 3 for 45

        if(minute == 0){
            minute = 0;
        }else if (minute == 1){
            minute = 15;
        }else if (minute == 2){
            minute = 30;
        }else if (minute == 3){
            minute = 45;
        }

        //strPickUptime to send in webservice

        String time24="";
        if (hour < 10 && minute == 0){
            strPickUpTime = "0"+hour+":0"+minute;
        }else if (hour < 10){
            strPickUpTime = "0"+hour+":"+minute;
        }else if (minute == 0){
            strPickUpTime = hour+":0"+minute;
        }else{
            strPickUpTime = hour+":"+minute;
        }

        if (hour == 0) {
            hour += 12;
            format = "AM";

        } else if (hour == 12) {
            format = "PM";

        } else if (hour > 12) {
            hour -= 12;
            format = "PM";

        } else {
            format = "AM";
        }

//        strPickUpTime = strPickUpTime+" "+format;

        if (minute == 0){
            time24 = hour+":0"+minute+" "+format;
            if (hour < 10){
                time24 = "0"+hour+":0"+minute+" "+format;
            }
        }else if (hour < 10){
            time24 = "0"+hour+":"+minute+" "+format;
        }else {
            time24 = hour+":"+minute+" "+format;
        }
//        tvPickUpTime.setText(time24);

        Log.d("currentDate", currentDate);
        Log.d("strSelectDate", strSelectDate);

        if (currentDate.equalsIgnoreCase(strSelectDate)) {
//            Toast.makeText(context, "Date is equal", Toast.LENGTH_SHORT).show();
            Log.d("currentDateEqualSDate", "Yes");

            int currentHourPlusOne;
            if (Integer.parseInt(currentHour) < 22) {
                currentHourPlusOne = Integer.parseInt(currentHour) + 2;
            } else {
                currentHourPlusOne = 1;
            }
            Log.d("currentHourPlusOne", "" + currentHourPlusOne);

            if (selectedHour < currentHourPlusOne) {

                showAlert();

            }else if (selectedHour == currentHourPlusOne && minute < Integer.parseInt(currentMinute)){

                showAlert();

            }else {
                tvPickUpTime.setText(time24);
                tvPickUpTime.setError(null);
            }

        } else {
            tvPickUpTime.setText(time24);
            tvPickUpTime.setError(null);
        }


        customTimePickerDialog.dismiss();
    }

    public void showAlert(){

        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage("Next available pick up time slot is 2 hours from now. Kindly edit booking details " +
                "to proceed further. Thank you.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        strPickUpTime = "";
                        tvPickUpTime.setText(getResources().getString(R.string.tvPickUpTimeCaps));

                    }
                });
        android.app.AlertDialog alert = builder.create();
//        alert.setTitle(getResources().getString(R.string.app_name));
        alert.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == StaticClass.SearchCityRequestCode) {

            if(data!=null) {
                if (!data.getExtras().getString("CityId").equals("")) {
//                    strCityId = data.getExtras().getString("CityId");
                    strCityName = data.getExtras().getString("CityName");
                    isState = data.getExtras().getString("isState");
                    Log.d("d", "City id::::" + strCityId);
                    Log.d("d", "City name::::" + strCityName);

                    if (!"".equals(strCityName) && !"null".equals(strCityName)) {
                        tvSearchCity.setText(strCityName);
//                        ivCancel.setVisibility(View.VISIBLE);
                    }

                    if (isState.equalsIgnoreCase("N")){
                        strCityId = data.getExtras().getString("CityId");
                        strStateId = "";
                    }else if (isState.equalsIgnoreCase("Y")){
                        strStateId = data.getExtras().getString("CityId");
                        strCityId = "";
                    }

//                    DataFetching();
                }
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(SearchActivity.this, transitionflag);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        transitionflag = StaticClass.transitionflagBack;
        finish();

    }

    @Override
    protected void onResume() {
        super.onResume();

        StaticClass.BottomExplore = false;

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }

//        sharedPrefUser.putBottomViewCompany(StaticClass.Menu_Explore);
//        bottomview = new BottomView();
//        bottomview.BottomView(SearchActivity.this, StaticClass.Menu_Explore);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) SearchActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

}

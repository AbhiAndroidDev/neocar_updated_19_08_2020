package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.PaymentInfoModel;

import java.util.ArrayList;

/**
 * Created by joydeep on 14/6/18.
 */

public interface OwnerDeactive_Interface {

    public void OnOwnerDeactive(int position);
}

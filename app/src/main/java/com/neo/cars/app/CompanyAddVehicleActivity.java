package com.neo.cars.app;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.neo.cars.app.Interface.CallBackButtonClick;
import com.neo.cars.app.Interface.VehicleMake_Interface;
import com.neo.cars.app.Interface.VehicleModel_Interface;
import com.neo.cars.app.Interface.VehicleSave_interface;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CommonUtility;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.Emailvalidation;
import com.neo.cars.app.Utils.ImageUtils;
import com.neo.cars.app.Utils.MessageText;
import com.neo.cars.app.Utils.NoDefaultSpinner;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Adapter.AddStopOverAdapterCompany;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.Webservice.VehicleAdd_Webservice;
import com.neo.cars.app.Webservice.VehicleMake_Webservice;
import com.neo.cars.app.Webservice.VehicleModel_Webservice;
import com.neo.cars.app.dialog.BottomSheetDialogPositiveNegative;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.dialog.YearMonthPickerDialog;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;
import com.neo.cars.app.font.CustomTextviewTitilliumBold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

//import me.iwf.photopicker.PhotoPicker;


public class CompanyAddVehicleActivity extends RootActivity implements CallBackButtonClick, VehicleMake_Interface,
        VehicleModel_Interface, VehicleSave_interface {

    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;

    private View mView;
    private Context context;
    private Activity activity;
    private RelativeLayout rlImage1, rlImage2, rlImage3, rlUploadLicensePlateImage, rlOvertimeAvailable, rlBackLayout, rlNightChargesRate;
    private FrameLayout flSpnrVehicleType, flSpnrVehicleBrand, flSpnrVehicleMake;
    private ImageView ivVehicle1, ivVehicle2, ivVehicle3, ivTaxToken, ivPuc, ivLicensePlate, ibDefaultIamgeOne, ibDefaultIamgeTwo,
            ibDefaultIamgeThree, ivRegionalTransportPermit, ivRegistrationCertificateFront, ivRegistrationCertificateBack, ivFitnessCertificate, ivInsuranceCertificate,
            ivInfoNightChargesRate, ivInfoParkingCharges, ivinfoRegionalTransportPermit, ivinfoRegistrationCertificateFront, ivinfoRegistrationCertificateBack,
            ivinfoFitnessCertificate, ivinfoInsuranceCertificate;
    private NoDefaultSpinner spnrVehicleBrand, spnrVehicleMake ;
    private String imageFilePath="", strVehicleTypeId="",  strImage="", strVehicleBrandId="", strVehicleModelId="",
            strVehicleMakeId="", strVehicleBrandCode="", strVehicleModelTitle="",  strSecondAddress="", strAddress = "",
            strLuggageCapacity="", strSittingCapacity="", strAirport="", strRailway="", strStateName="", strCityName="";
    private String mSelectedGalleryImagePath="", mGalleryImagePath="", userChoosenTask="";
    private File file, fileGalleryOne, fileGalleryTwo, fileGalleryThree, fileTax, filePuc, fileLicense,
            fileRegionalTransportPermit, fileRegistrationCertificateFront, fileRegistrationCertificateBack, fileFitnessCertificate, fileInsuranceCertificate;
    private ConnectionDetector cd;
    private ArrayList<String> listOfVehicleType, listOfVehicleMake, listOfVehicleModel, listOfParkingCharges;
    private ArrayAdapter<String> arrayAdapterVehicleType, arrayAdapterVehicleMake, arrayAdapterVehicleModel, arAdapterParkingCharges;
    private CustomButtonTitilliumSemibold btnSave,btnDelete;
    private CustomTextviewTitilliumBold tvLicensePlateTextview, tvRegionalTransportPermit, tvRegistrationCertificateFront, tvRegistrationCertificateBack,
            tvFitnessCertificate, tvInsuranceCertificate;
    private CustomEditTextTitilliumWebRegular tvHourlyRate, tvHourlyOvertimeRate, tvTotalKm, tvMaxPassengers, tvMaxLuggage, tvSpclInfo,
            etNightChargesRate, tvpickuploc2, tvLicenseno ;
    private CustomTextviewTitilliumWebRegular tvYrOfManufacture, tvpickuploc_airport, tvpickuploc_railway;
    private String strBrandName="", strVehicleMake="",strHourlyRate="", strOvertimeHourlyRate="", strYrOfManufacture="", strTotalKm="", strMaxPassenger="",
            strMaxLuggage="", strSpecialInfo="", strLicenseNo="",
            strOvertimeAvailableCheck="N", strSetGallerySelected="", strOvertimeACMin = "",strOvertimeACMax = "", strOvertimeNonACMin = "", strOvertimeNonACMax = "";
    private CustomTextviewTitilliumWebRegular spnrVehicleType;

    private AppCompatImageView ivCheck;
    boolean isSelected = false;
    boolean isACSelected = false;
    boolean isOtherLocSelected = false;

    public static final int REQUEST_IMAGE_CAPTURE = 0;
    public static final int PICK_IMAGE_REQUEST = 902;
    private Calendar mcalendar;
    private int day,month,year;

    private String strGalleryPath="", strGalleryOnePath="", strGalleryTwoPath="", strGalleryThreePath="", strTaxPath="", strPcuPath="", strLicenseIamgePath="",
            strStateId="", strCityId="",  strSelectedGalOne="", strSelectedGalTwo="", strSelectedGalThree="", strSelectedTax="", strSelectedPcu="", strSelectedLicense="",
            strSelectedRegionalTransport = "", strParkingCharge = "", strRegionalTransportImgPath = "",
            strRegistrationCertificateImgPath = "", strFitnessCertificateImgPath = "", strInsuranceCertificateImgPath = "", strSelectedRegistrationCertificate = "",
            strSelectedFitnessCertificate = "", strSelectedInsuranceCertificate = "";


    private LinearLayout lladdmore_pickuplocation,llAddMoreLayout;
    private CustomTextviewTitilliumWebRegular tvpickuploc_addmore;
    private ImageView iv_licenceplatealert;

    boolean boolSetDefault = false, isNightChargeSelected = false;
    private FrameLayout flSpnrMaxPassenger, flSpnrMaxLuggage;
    private NoDefaultSpinner spnrMaxPassenger, spnrMaxLuggage, spnrParkingCharges;
    int intMaxPassenger=0, intMaxLuggage=0;
    private ArrayList<Integer> listOfMaxPassenger, listOfMaxLuggage;
    private ArrayAdapter<Integer> arrayAdapterMaxPassenger, arrayAdapterMaxLuggage;
    private RelativeLayout llHourlyOvertimeLayout;
    private CustomDialog pdCusomeDialog;
    private CustomTextviewTitilliumWebRegular tvLocation;
    private CustomTextviewTitilliumWebRegular spnrStateList, spnrCityList;
    private CustomTitilliumTextViewSemiBold tvACAvailable;
    private AppCompatImageView ivACCheck;
    private String strACCheck="", strACMin="", strACMax="", strNonACMin="", strNonACMax="", strOtherLoc="N";
    private ImageView iv_hourlyratealert, iv_pickupinfo;

    private int transitionflag = StaticClass.transitionflagNext;

    public static final int StateListRequestCode = 701;
    public static final int CityListRequestCode = 702;

    private CustomTitilliumTextViewSemiBold tvStateListheader, tvCityListheader, tvVehicleTypeheader,tvVehiclemakerheader, tvBrandnameheader,
            tvYrOfManufactureheader, tvTotalKmheader, tvMaxPassengersheader, tvMaxLuggageheader, tvHourlyRateheader,
            tvCheck, tvLicenseheader ;

    private CustomTextviewTitilliumBold tvTaxTokenTextview, tvPucTextview;

    private String strVehiclEStateHeader="", colored="", strVehicleCityListHeader="", strVehicletypeHeader="", strVehicleMakeHeader="", strVehicleBrandNAmeHeader="",
            strAcAvailableHeader="", strYrManufacture="", strTotalKmHeader="", strMaxPassengerHeader="", strMaxLuggageHeader="",
            strHourlyRateHeader="", strOvertimeAvailableHeader="", strLicensePlateHeader="", strTaxTokenHeader="", strPucHeader="",
            strVehicleTypeName="", strACNightMin = "", strACNightMax = "", strNONACNightMin = "", strNOnACNightMax, strNightChargesCheck = "N", strNightChargesRate = "", toll_parking_charge = "";


    private SpannableStringBuilder builder;
    int start, end;

    private CustomTitilliumTextViewSemiBold tvHourlyOvertimeRateheader, tvShowOtherLoc;
    private String strHourlyOvertimeRateHeader="";
    private ImageView iv_hourlyovertimeratealert, iv_otherloc;
    private AppCompatImageView ivOtherLocCheck, ivCheckNightChargesAvailable;

    private BottomViewCompany bottomViewCompany = new BottomViewCompany();
    private SharedPrefUserDetails sharedPref;

    private ProgressDialog dialog;
    private Uri imageUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_add_vehicle);
        new AnalyticsClass(CompanyAddVehicleActivity.this);
        Initialize();

        if(cd.isConnectingToInternet()){
            showProgressDialog();
            new VehicleMake_Webservice().vehicleMakeWebservice(activity, CompanyAddVehicleActivity.this);

        }else{
            new CustomToast(activity, getResources().getString(R.string.Network_not_availabl));
        }
        Listener();
    }

    private void Initialize(){

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        rlBackLayout = findViewById(R.id.rlBackLayout);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvMyVehicleHeader));
        context = CompanyAddVehicleActivity.this;
        activity = CompanyAddVehicleActivity.this;
        cd = new ConnectionDetector(CompanyAddVehicleActivity.this);
        mcalendar = Calendar.getInstance();
        day=mcalendar.get(Calendar.DAY_OF_MONTH);
        year=mcalendar.get(Calendar.YEAR);
        month=mcalendar.get(Calendar.MONTH);

        // Vehicle State field mandatory
        tvStateListheader = findViewById(R.id.tvStateListheader);
        tvCityListheader = findViewById(R.id.tvCityListheader);
        tvVehicleTypeheader = findViewById(R.id.tvVehicleTypeheader);
        tvVehiclemakerheader =  findViewById(R.id.tvVehiclemakerheader);
        tvBrandnameheader =  findViewById(R.id.tvBrandnameheader);
        tvACAvailable =  findViewById(R.id.tvACAvailable);
        tvYrOfManufactureheader =  findViewById(R.id.tvYrOfManufactureheader);
        tvTotalKmheader =  findViewById(R.id.tvTotalKmheader);
        tvMaxPassengersheader =  findViewById(R.id.tvMaxPassengersheader);
        tvMaxLuggageheader =  findViewById(R.id.tvMaxLuggageheader);
        tvHourlyRateheader =  findViewById(R.id.tvHourlyRateheader);
        tvCheck =  findViewById(R.id.tvCheck);
        tvLicenseheader =  findViewById(R.id.tvLicenseheader);
        tvTaxTokenTextview = findViewById(R.id.tvTaxTokenTextview);
        tvPucTextview = findViewById(R.id.tvPucTextview);
        tvHourlyOvertimeRateheader =  findViewById(R.id.tvHourlyOvertimeRateheader);

        ivVehicle1 = findViewById(R.id.ivVehicle1);
        ivVehicle2 = findViewById(R.id.ivVehicle2);
        ivVehicle3 = findViewById(R.id.ivVehicle3);

        flSpnrVehicleType = findViewById(R.id.flSpnrVehicleType);
        spnrVehicleType =  findViewById(R.id.spnrVehicleType);
        spnrParkingCharges = findViewById(R.id.spnrParkingCharges);
        btnSave = findViewById(R.id.btnSave);
        tvTaxTokenTextview = findViewById(R.id.tvTaxTokenTextview);
        tvTaxTokenTextview.setPaintFlags(tvTaxTokenTextview.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvPucTextview = findViewById(R.id.tvPucTextview);
        tvPucTextview.setPaintFlags(tvPucTextview.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvHourlyRate = findViewById(R.id.tvHourlyRate);
        tvHourlyOvertimeRate = findViewById(R.id.tvHourlyOvertimeRate);
        tvYrOfManufacture =  findViewById(R.id.tvYrOfManufacture);
        tvTotalKm = findViewById(R.id.tvTotalKm);
        tvSpclInfo = findViewById(R.id.tvSpclInfo);
        tvLicenseno = findViewById(R.id.tvLicenseno);
        flSpnrVehicleBrand = findViewById(R.id.flSpnrVehicleBrand);
        flSpnrVehicleMake = findViewById(R.id.flSpnrVehicleMake);
        spnrVehicleBrand = findViewById(R.id.spnrVehicleBrand);
        spnrVehicleMake = findViewById(R.id.spnrVehicleMake);
        ivTaxToken = findViewById(R.id.ivTaxToken);
        ivPuc = findViewById(R.id.ivPuc);
        rlUploadLicensePlateImage = findViewById(R.id.rlUploadLicensePlateImage);
        ivLicensePlate = findViewById(R.id.ivLicensePlate);
        tvLicensePlateTextview = findViewById(R.id.tvLicensePlateTextview);
        tvLicensePlateTextview.setPaintFlags(tvLicensePlateTextview.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvRegionalTransportPermit = findViewById(R.id.tvRegionalTransportPermit);
        tvRegistrationCertificateFront = findViewById(R.id.tvRegistrationCertificateFront);
        tvRegistrationCertificateBack = findViewById(R.id.tvRegistrationCertificateBack);
        tvFitnessCertificate = findViewById(R.id.tvFitnessCertificate);
        tvInsuranceCertificate = findViewById(R.id.tvInsuranceCertificate);

        etNightChargesRate = findViewById(R.id.etNightChargesRate);

        rlOvertimeAvailable = findViewById(R.id.rlOvertimeAvailable);
        rlNightChargesRate = findViewById(R.id.rlNightChargesRate);

        ivCheck =  findViewById(R.id.ivCheck);
        ivCheckNightChargesAvailable = findViewById(R.id.ivCheckNightChargesAvailable);
        ibDefaultIamgeOne = findViewById(R.id.ibDefaultIamgeOne);
        ibDefaultIamgeTwo = findViewById(R.id.ibDefaultIamgeTwo);
        ibDefaultIamgeThree = findViewById(R.id.ibDefaultIamgeThree);

        ivRegionalTransportPermit =  findViewById(R.id.ivRegionalTransportPermit);
        ivRegistrationCertificateFront =  findViewById(R.id.ivRegistrationCertificateFront);
        ivRegistrationCertificateBack =  findViewById(R.id.ivRegistrationCertificateBack);
        ivFitnessCertificate =  findViewById(R.id.ivFitnessCertificate);
        ivInsuranceCertificate =  findViewById(R.id.ivInsuranceCertificate);

        ivInfoNightChargesRate=findViewById(R.id.ivInfoNightChargesRate);
        ivInfoParkingCharges=findViewById(R.id.ivInfoParkingCharges);
        ivinfoRegionalTransportPermit=findViewById(R.id.ivinfoRegionalTransportPermit);
        ivinfoRegistrationCertificateFront=findViewById(R.id.ivinfoRegistrationCertificateFront);
        ivinfoRegistrationCertificateBack=findViewById(R.id.ivinfoRegistrationCertificateBack);
        ivinfoFitnessCertificate=findViewById(R.id.ivinfoFitnessCertificate);
        ivinfoInsuranceCertificate=findViewById(R.id.ivinfoInsuranceCertificate);

        tvpickuploc_addmore=findViewById(R.id.tvpickuploc_addmore);
        lladdmore_pickuplocation= findViewById(R.id.lladdmore_pickuplocation);
        llAddMoreLayout= findViewById(R.id.llAddMoreLayout);
        iv_licenceplatealert=findViewById(R.id.iv_licenceplatealert);

        tvpickuploc_airport =  findViewById(R.id.tvpickuploc_airport);
        tvpickuploc_railway =  findViewById(R.id.tvpickuploc_railway);

        flSpnrMaxPassenger = findViewById(R.id.flSpnrMaxPassenger);
        spnrMaxPassenger = findViewById(R.id.spnrMaxPassenger);

        flSpnrMaxLuggage = findViewById(R.id.flSpnrMaxLuggage);
        spnrMaxLuggage = findViewById(R.id.spnrMaxLuggage);

        llHourlyOvertimeLayout = findViewById(R.id.llHourlyOvertimeLayout);
        tvLocation =  findViewById(R.id.tvLocation);

        btnDelete = findViewById(R.id.btnDelete);
        btnDelete.setVisibility(View.GONE);

        spnrStateList =  findViewById(R.id.spnrStateList);
        spnrCityList =  findViewById(R.id.spnrCityList);


        ivACCheck =  findViewById(R.id.ivACCheck);

        iv_hourlyratealert = findViewById(R.id.iv_hourlyratealert);
        iv_pickupinfo = findViewById(R.id.iv_pickupinfo);
        iv_hourlyovertimeratealert = findViewById(R.id.iv_hourlyovertimeratealert);

        tvShowOtherLoc =  findViewById(R.id.tvShowOtherLoc);
        iv_otherloc = findViewById(R.id.iv_otherloc);
        ivOtherLocCheck =  findViewById(R.id.ivOtherLocCheck);

        listOfParkingCharges = new ArrayList<>();

        listOfParkingCharges.add(getResources().getString(R.string.included));
        listOfParkingCharges.add(getResources().getString(R.string.atActuals));

        arAdapterParkingCharges = new ArrayAdapter<String>(context, R.layout.countryitem, listOfParkingCharges);
        arAdapterParkingCharges.setDropDownViewResource(R.layout.simpledropdownitem);
        spnrParkingCharges.setPrompt(getResources().getString(R.string.tvSelectParkingCharges));
        spnrParkingCharges.setAdapter(arAdapterParkingCharges);

        sharedPref = new SharedPrefUserDetails(this);

    }

    private void Listener() {

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(activity,
                        activity.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                        activity.getResources().getString(R.string.yes),
                        activity.getResources().getString(R.string.no));
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                        transitionflag = StaticClass.transitionflagBack;
                        finish();

                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();
            }
        });

        ivCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSelected == true) {
                    ivCheck.setImageResource(R.drawable.uncheck);
                    isSelected = false;
                    strOvertimeAvailableCheck = "N";
                    llHourlyOvertimeLayout.setVisibility(View.GONE);

                } else if (isSelected == false) {
                    ivCheck.setImageResource(R.drawable.check);
                    isSelected = true;
                    strOvertimeAvailableCheck = "Y";
                    llHourlyOvertimeLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        ivCheckNightChargesAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNightChargeSelected){
                    ivCheckNightChargesAvailable.setImageResource(R.drawable.uncheck);
                    isNightChargeSelected = false;
                    strNightChargesCheck = "N";
                    rlNightChargesRate.setVisibility(View.GONE);
                }else {
                    ivCheckNightChargesAvailable.setImageResource(R.drawable.check);
                    isNightChargeSelected = true;
                    strNightChargesCheck = "Y";
                    rlNightChargesRate.setVisibility(View.VISIBLE);
                }
            }
        });

        ivACCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isACSelected == true) {
                    ivACCheck.setImageResource(R.drawable.uncheck);
                    isACSelected = false;
                    strACCheck = "N";

                } else if (isACSelected == false) {
                    ivACCheck.setImageResource(R.drawable.check);
                    isACSelected = true;
                    strACCheck = "Y";
                }
            }
        });

        ivOtherLocCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isOtherLocSelected == true){
                    ivOtherLocCheck.setImageResource(R.drawable.uncheck);
                    isOtherLocSelected = false;
                    strOtherLoc = "N";

                }else  if (isOtherLocSelected == false) {
                    ivOtherLocCheck.setImageResource(R.drawable.check);
                    isOtherLocSelected = true;
                    strOtherLoc = "Y";
                }
            }
        });


        tvLicensePlateTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "License";
                selectImage(getResources().getString(R.string.msgUpldLicenseplateImage), strImage);
            }
        });

        tvTaxTokenTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "Tax";
                selectImage(getResources().getString(R.string.msgUpldTaxTokenImage), strImage);
            }
        });

        tvPucTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "Puc";
                selectImage(getResources().getString(R.string.msgUpldPUCPaper), strImage);
            }
        });

        tvRegionalTransportPermit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "RegionalTransportPermit";
                selectImage(getResources().getString(R.string.msgRegionalTransportPermit), strImage);
            }
        });

        tvRegistrationCertificateFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "RegistrationCertificateFront";
                selectImage(getResources().getString(R.string.msgRegistrationCertificate), strImage);
            }
        });

        tvRegistrationCertificateBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "RegistrationCertificateBack";
                selectImage(getResources().getString(R.string.msgRegistrationCertificate), strImage);
            }
        });

        tvFitnessCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "FitnessCertificate";
                selectImage(getResources().getString(R.string.msgFitnessCertificate), strImage);
            }
        });

        tvInsuranceCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "InsuranceCertificate";
                selectImage(getResources().getString(R.string.msgInsuranceCertificate), strImage);
            }
        });

        tvYrOfManufacture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                YrOfManufactureDialog();
            }
        });

        ivVehicle1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "Gallery1";
                selectImage(getResources().getString(R.string.msgUpldVehicleOneImage), strImage);
            }
        });

        ibDefaultIamgeOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(strGalleryOnePath)) {
                    strImage = "Gallery1";
                    ibDefaultIamgeOne.setImageResource(R.drawable.vehicle_info_blank_image_select_fill);
                    ibDefaultIamgeTwo.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                    ibDefaultIamgeThree.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                    strSetGallerySelected = "1";

                }else {
                    new CustomToast(activity, getResources().getString(R.string.tvUploadImageFirst));
                }
            }
        });

        ivVehicle2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "Gallery2";

                selectImage(getResources().getString(R.string.msgUpldVehicleOneImage), strImage);
            }
        });

        ibDefaultIamgeTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(strGalleryTwoPath)) {
                    strImage = "Gallery2";
                    ibDefaultIamgeOne.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                    ibDefaultIamgeTwo.setImageResource(R.drawable.vehicle_info_blank_image_select_fill);
                    ibDefaultIamgeThree.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                    strSetGallerySelected = "2";
                }else {
                    new CustomToast(activity, getResources().getString(R.string.tvUploadImageFirst));
                }
            }
        });

        ivVehicle3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "Gallery3";

                selectImage(getResources().getString(R.string.msgUpldVehicleOneImage), strImage);
            }
        });

        ibDefaultIamgeThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(strGalleryThreePath)) {
                    strImage = "Gallery3";
                    ibDefaultIamgeOne.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                    ibDefaultIamgeTwo.setImageResource(R.drawable.vehicle_info_blank_image_select_blank);
                    ibDefaultIamgeThree.setImageResource(R.drawable.vehicle_info_blank_image_select_fill);
                    strSetGallerySelected = "3";
                }else {
                    new CustomToast(activity, getResources().getString(R.string.tvUploadImageFirst));
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                spnrStateList.setError(null);
                spnrCityList.setError(null);
                tvHourlyRate.setError(null);
                tvYrOfManufacture.setError(null);
                tvTotalKm.setError(null);
                tvLicenseno.setError(null);
                spnrVehicleType.setError(null);
                spnrCityList.setError(null);

                boolean cancel = false;
                View focusView = null;

                strHourlyRate = tvHourlyRate.getText().toString().trim();
                strOvertimeHourlyRate = tvHourlyOvertimeRate.getText().toString().trim();
                strNightChargesRate = etNightChargesRate.getText().toString().trim();
                strYrOfManufacture = tvYrOfManufacture.getText().toString().trim();
                strTotalKm = tvTotalKm.getText().toString().trim();

                //strMaxPassenger = tvMaxPassengers.getText().toString().trim();

                Log.d("d", "Spinner:::"+spnrMaxPassenger.getSelectedItem());
                if(spnrMaxPassenger != null && spnrMaxPassenger.getSelectedItem() != null){
                    strMaxPassenger = spnrMaxPassenger.getSelectedItem().toString() ;
                    Log.d("d", "strMaxPassenger:::"+strMaxPassenger);
                }

                if(spnrMaxLuggage != null && spnrMaxLuggage.getSelectedItem() != null){
                    strMaxLuggage = spnrMaxLuggage.getSelectedItem().toString();
                    Log.d("d", "**strMaxLuggage***"+strMaxLuggage);
                }


                // strMaxLuggage = tvMaxLuggage.getText().toString().trim();
                strLicenseNo = tvLicenseno.getText().toString().trim();

                // strAirport = tvpickuploc_airport.getText().toString().trim();
                // strRailway = tvpickuploc_railway.getText().toString().trim();

                //strSecondAddress = strAirport + ", " + strRailway + ", " + tvpickuploc_addmore.getText().toString().trim();
                strSecondAddress = tvpickuploc_addmore.getText().toString().trim();
                Log.d("d", "***strSecondAddress***"+strSecondAddress);

                if (TextUtils.isEmpty(strStateName)){
                    spnrStateList.setError(getString(R.string.error_field_required));
                    spnrStateList.requestFocus();

                }

                //Add vehicle field validation check
                if (TextUtils.isEmpty(strHourlyRate)) {
                    tvHourlyRate.setError(getString(R.string.error_field_required));
//                    tvHourlyRate.requestFocus();
                    //focusView = tvHourlyRate;
                    // cancel = true;

                }/*else if (TextUtils.isEmpty(strOvertimeHourlyRate)){
                    tvHourlyOvertimeRate.setError(getString(R.string.error_field_required));
                    focusView = tvHourlyOvertimeRate;
                    cancel = true;

                }*/
                else if (TextUtils.isEmpty(strYrOfManufacture)){
                    tvYrOfManufacture.setError(getString(R.string.error_field_required));
                    tvYrOfManufacture.requestFocus();
                    //focusView = tvYrOfManufacture;
                    //cancel = true;

                }else if (TextUtils.isEmpty(strTotalKm)){
                    tvTotalKm.setError(getString(R.string.error_field_required));
                    tvTotalKm.requestFocus();
                    // focusView = tvTotalKm;
                    // cancel = true;

                }/*else if (TextUtils.isEmpty(strMaxPassenger)){
                    tvMaxPassengers.setError(getString(R.string.error_field_required));
                    tvMaxPassengers.requestFocus();
                   // focusView = tvMaxPassengers;
                   // cancel = true;

                }*/
                /*else if (TextUtils.isEmpty(strMaxLuggage)){
                    tvMaxLuggage.setError(getString(R.string.error_field_required));
                    tvMaxLuggage.requestFocus();
                   // focusView = tvMaxLuggage;
                   // cancel = true;

                }*/
                else if (TextUtils.isEmpty(strLicenseNo)){
                    tvLicenseno.setError(getString(R.string.error_field_required));
                    tvLicenseno.requestFocus();
                    //focusView = tvLicenseno;
                    //cancel = true;

                }else if (!new Emailvalidation().lincescePlate(strLicenseNo.toUpperCase())){
                    tvLicenseno.setError(getString(R.string.Entervalid_Vehicleregistrationplates));
                    tvLicenseno.requestFocus();
                    //focusView = tvLicenseno;
                    //cancel = true;

                }/*else if (cancel) {
                    // form field with an error.
                    focusView.requestFocus();

                }*/
                else if (strGalleryPath.equalsIgnoreCase("")){
                    new CustomToast(activity, getResources().getString(R.string.msgUploadGalleryPic));
                }
//                else if (filePuc == null){
//                    new CustomToast(activity, getResources().getString(R.string.msgUpldPUCPaper));
//                }else if (fileTax == null){
//                    new CustomToast(activity, getResources().getString(R.string.msgUpldTaxTokenImage));
//                }
                else{
                    ArrayList<String> myLocation = new ArrayList<String>();

                    if (StaticClass.getAl_Location_company() != null) {
                        myLocation = StaticClass.getAl_Location_company();
                    }

                    JSONArray jsonArrayAddress = new JSONArray();
                    if(!"".equals(strSecondAddress)){
                        jsonArrayAddress.put(strSecondAddress);
                    }

                    for (int i=0;i< myLocation.size();i++){
                        if(!myLocation.get(i).equals("")) {
                            Log.d("d", "String address 123445::"+strAddress);
                            jsonArrayAddress.put(myLocation.get(i));
                        }
                    }

                    strAddress = jsonArrayAddress.toString();
                    Log.d("d", "String address::"+strAddress);

                    Log.d("d", "***strOtherLoc***"+strOtherLoc);

                    new VehicleAdd_Webservice().VehicleAdd(activity, strVehicleTypeId, strVehicleBrandId, strVehicleModelId,
                            strACCheck, strYrOfManufacture, strTotalKm, strStateId, strCityId, strMaxPassenger, strMaxLuggage, strHourlyRate,
                            strOvertimeAvailableCheck, strOvertimeHourlyRate, strSpecialInfo, strLicenseNo, fileLicense, fileTax,
                            filePuc, strAddress, strOtherLoc,   fileGalleryOne, fileGalleryTwo, fileGalleryThree,
                            fileRegionalTransportPermit, fileRegistrationCertificateFront, fileRegistrationCertificateBack, fileFitnessCertificate, fileInsuranceCertificate,
                            strSetGallerySelected, StaticClass.COMPANY ,
                            toll_parking_charge, strNightChargesCheck, strNightChargesRate);
                }
            }
        });

        llAddMoreLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if(tvpickuploc_addmore.getText().toString().trim().equals("")){
                    new CustomToast(getActivity(), MessageText.Enter_all_added_field);
                }else{*/
                AddStopOverMethod();
                //  }
            }
        });


        iv_licenceplatealert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CustomToast(activity, MessageText.Pleaeuseaformatasfollows);
            }
        });


        ivInfoNightChargesRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean dialogFlag=true;
                if(strACCheck.equalsIgnoreCase("N")){
                    if(strNonACMin.equals("") || strNonACMax.equals("")){
                        dialogFlag=false;
                    }
                }else{
                    if(strACMax.equals("") || strACMin.equals("")){
                        dialogFlag=false;
                    }
                }

                if (dialogFlag){
                    final Dialog customdialog = new Dialog(CompanyAddVehicleActivity.this);
                    customdialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                    customdialog.setContentView(R.layout.dialog_car_night_rate);

                    CustomTextviewTitilliumBold tvNightACMinRate = customdialog.findViewById(R.id.tvNightACMinRate);
                    CustomTextviewTitilliumBold tvNightACMaxRate = customdialog.findViewById(R.id.tvNightACMaxRate);
                    CustomTextviewTitilliumBold tvNightNonACMinRate = customdialog.findViewById(R.id.tvNightNonACMinRate);
                    CustomTextviewTitilliumBold tvNightNonACMaxRate = customdialog.findViewById(R.id.tvNightNonACMaxRate);
                    CustomButtonTitilliumSemibold btnOk = customdialog.findViewById(R.id.btnOk);

                    if (!TextUtils.isEmpty(strACNightMin) && !TextUtils.isEmpty(strACNightMax)){
                        tvNightACMinRate.setText(strACNightMin);
                        tvNightACMaxRate.setText(strACNightMax);

                    }else {
                        tvNightACMinRate.setText("N/A");
                        tvNightACMaxRate.setText("N/A");
                    }
                    if (!TextUtils.isEmpty(strNONACNightMin) && !TextUtils.isEmpty(strNOnACNightMax)){

                        tvNightNonACMinRate.setText(strNONACNightMin);
                        tvNightNonACMaxRate.setText(strNOnACNightMax);

                    }else {
                        tvNightNonACMinRate.setText("N/A");
                        tvNightNonACMaxRate.setText("N/A");
                    }


                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customdialog.dismiss();
                        }
                    });
                    customdialog.show();

                }else {
                    new CustomToast(CompanyAddVehicleActivity.this, "Please select vehicle model");
                }
            }
        });

        ivInfoParkingCharges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CustomToast(activity, MessageText.tvParkingChargesHeader);

            }
        });

        ivinfoRegionalTransportPermit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CustomToast(activity, MessageText.tvinfoRegionalPermitHeader);

            }
        });

        ivinfoRegistrationCertificateFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CustomToast(activity, MessageText.tvinfoRegistrationCertificate);

            }
        });

        ivinfoRegistrationCertificateBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CustomToast(activity, MessageText.tvinfoRegistrationCertificateBack);

            }
        });

        ivinfoFitnessCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CustomToast(activity, MessageText.tvinfoFitnessCertificate);
            }
        });

        ivinfoInsuranceCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CustomToast(activity, MessageText.tvinfoInsuranceCertificate);
            }
        });

        iv_pickupinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CustomToast(activity, MessageText.PickUpLocationInfoText);
            }
        });

        iv_hourlyratealert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //set custom dialog for rate chart
                boolean dialogFlag=true;
                if(strACCheck.equalsIgnoreCase("N")){
                    if(strNonACMin.equals("") || strNonACMax.equals("")){
                        dialogFlag=false;
                    }
                }else{
                    if(strACMax.equals("") || strACMin.equals("")){
                        dialogFlag=false;
                    }
                }

                if(dialogFlag) {
                    final Dialog customdialog = new Dialog(activity);
                    customdialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                    customdialog.setContentView(R.layout.dialog_car_rate);

                    // set the custom dialog components - text, image and button
                    CustomTextviewTitilliumBold tvVehicletype = customdialog.findViewById(R.id.tvVehicletype);
                    tvVehicletype.setText(strVehicleTypeName);

                    CustomTextviewTitilliumBold tvACMinRate = customdialog.findViewById(R.id.tvACMinRate);
                    CustomTextviewTitilliumBold tvACMaxRate = customdialog.findViewById(R.id.tvACMaxRate);
                    CustomTextviewTitilliumBold tvNonACMinRate = customdialog.findViewById(R.id.tvNonACMinRate);
                    CustomTextviewTitilliumBold tvNonACMaxRate = customdialog.findViewById(R.id.tvNonACMaxRate);
                    CustomButtonTitilliumSemibold btnOk = customdialog.findViewById(R.id.btnOk);

                    if (!TextUtils.isEmpty(strACMin)) {
                        tvACMinRate.setText(strACMin);
                    } else {
                        tvACMinRate.setText("N/A");
                    }

                    if (!TextUtils.isEmpty(strACMax)) {
                        tvACMaxRate.setText(strACMax);
                    } else {
                        tvACMaxRate.setText("N/A");
                    }

                    if (!TextUtils.isEmpty(strNonACMin)) {
                        tvNonACMinRate.setText(strNonACMin);
                    } else {
                        tvNonACMinRate.setText("N/A");
                    }

                    if (!TextUtils.isEmpty(strNonACMax)) {
                        tvNonACMaxRate.setText(strNonACMax);
                    } else {
                        tvNonACMaxRate.setText("N/A");
                    }

                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customdialog.dismiss();
                        }
                    });
                    customdialog.show();
                }else{
                    new CustomToast(activity, "Please select vehicle model");
                }
            }
        });

        iv_hourlyovertimeratealert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean dialogFlag=true;
                if(strACCheck.equalsIgnoreCase("N")){
                    if(strNonACMin.equals("") || strNonACMax.equals("")){
                        dialogFlag=false;
                    }
                }else{
                    if(strACMax.equals("") || strACMin.equals("")){
                        dialogFlag=false;
                    }
                }

                if(dialogFlag) {
                    final Dialog customdialog = new Dialog(activity);
                    customdialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                    customdialog.setContentView(R.layout.dialog_car_rate);

                    // set the custom dialog components - text, image and button
                    CustomTextviewTitilliumBold tvVehicletype = customdialog.findViewById(R.id.tvVehicletype);
                    tvVehicletype.setText(strVehicleTypeName);

                    CustomTextviewTitilliumBold tvACMinRate = customdialog.findViewById(R.id.tvACMinRate);
                    CustomTextviewTitilliumBold tvACMaxRate = customdialog.findViewById(R.id.tvACMaxRate);
                    CustomTextviewTitilliumBold tvNonACMinRate = customdialog.findViewById(R.id.tvNonACMinRate);
                    CustomTextviewTitilliumBold tvNonACMaxRate = customdialog.findViewById(R.id.tvNonACMaxRate);
                    CustomButtonTitilliumSemibold btnOk = customdialog.findViewById(R.id.btnOk);

                    if (!"".equals(strOvertimeACMin) && !"null".equals(strOvertimeACMin)) {
                        tvACMinRate.setText(strOvertimeACMin);
                    } else {
                        tvACMinRate.setText("N/A");
                    }

                    if (!"".equals(strOvertimeACMax) && !"null".equals(strOvertimeACMax)) {
                        tvACMaxRate.setText(strOvertimeACMax);
                    } else {
                        tvACMaxRate.setText("N/A");
                    }

                    if (!"".equals(strOvertimeNonACMin) && !"null".equals(strOvertimeNonACMin)) {
                        tvNonACMinRate.setText(strOvertimeNonACMin);
                    } else {
                        tvNonACMinRate.setText("N/A");
                    }

                    if (!"".equals(strOvertimeNonACMax) && !"null".equals(strOvertimeNonACMax)) {
                        tvNonACMaxRate.setText(strOvertimeNonACMax);
                    } else {
                        tvNonACMaxRate.setText("N/A");
                    }

                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customdialog.dismiss();
                        }
                    });
                    customdialog.show();
                }else{
                    new CustomToast(activity, "Please select vehicle model");
                }
            }
        });

        iv_otherloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CustomToast(activity, MessageText.OtherLocationWhileBookingText);
            }
        });

        spnrStateList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitionflag = StaticClass.transitionflagNext;
                Intent stateintent = new Intent(activity, StateList.class);
                startActivityForResult(stateintent, StateListRequestCode);
            }
        });

        spnrCityList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strStateId != null && !strStateId.equals("")) {
                    transitionflag = StaticClass.transitionflagNext;
                    Intent cityintent = new Intent(activity, CityList.class);
                    cityintent.putExtra("strStateId", strStateId);
                    startActivityForResult(cityintent, CityListRequestCode);
                }else {
                    new CustomToast(activity, "Please select State first!");
                }
            }
        });

        spnrParkingCharges.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                strParkingCharge = listOfParkingCharges.get(position);
                Log.d("strParkingCharge*", strParkingCharge);


                if (strParkingCharge.equalsIgnoreCase("Included")){

                    toll_parking_charge = "I";
                }else if (strParkingCharge.equalsIgnoreCase("At Actuals")){
                    toll_parking_charge = "A";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomViewCompany.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        sharedPref.putBottomViewCompany(StaticClass.Menu_MyVehicles_company);
        bottomViewCompany = new BottomViewCompany();
        bottomViewCompany.BottomViewCompany(CompanyAddVehicleActivity.this, StaticClass.Menu_MyVehicles_company);

    }

    @Override
    public void onBackPressed() {
        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(activity,
                activity.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                activity.getResources().getString(R.string.yes),
                activity.getResources().getString(R.string.no));
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();

    }

    private void AddStopOverMethod(){
        new AddStopOverAdapterCompany(activity,lladdmore_pickuplocation);
    }

    public void YrOfManufactureDialog(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR),01,01);

        YearMonthPickerDialog yearMonthPickerDialog = new YearMonthPickerDialog(activity,
                calendar,
                new YearMonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onYearMonthSet(int year) {
                        System.out.println("**YearMonthPickerDialog**"+year);
                        tvYrOfManufacture.setText(String.valueOf(year));
                    }
                });

        yearMonthPickerDialog.show();
    }


    public void selectImage(final String strMessage, String filetype) {

        BottomSheetDialogPositiveNegative bsd = new BottomSheetDialogPositiveNegative(context,
                activity,
                CompanyAddVehicleActivity.this,
                strMessage,
                CompanyAddVehicleActivity.this.getResources().getString(R.string.Camera),
                CompanyAddVehicleActivity.this.getResources().getString(R.string.Gallery));
    }



    //take photo through camera
    private void takePhoto() {

        // set a image file path
        imageFilePath = ImageUtils.getFile().getAbsolutePath();
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        imageUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", ImageUtils.getFile());// return content:///..
        //imageUri=Uri.fromFile(getFile()); // returns file:///...
        Log.d("@ file uri :", imageUri.toString());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); //API >24
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);

    }

    //select image from gallery
    private void mCallPhotoGallary() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select a photo"), PICK_IMAGE_REQUEST);

//        try {
//            PhotoPicker.builder()
//                    .setPhotoCount(1)
//                    .setShowCamera(true)
//                    .setShowGif(false)
//                    .setPreviewEnabled(false)
//                    .start(this, PICK_IMAGE_REQUEST);
//
//        } catch (Exception e) {
//            //To catch unknown Null pointer exception inside library
//            e.printStackTrace();
//        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case StateListRequestCode:

                    if (!data.getExtras().getString("StateName").equals("") && !data.getExtras().getString("StateId").equals("")) {
                        strStateId = data.getExtras().getString("StateId");
                        Log.d("State id: ", strStateId);
                        strStateName = data.getExtras().getString("StateName");
                        Log.d("State Name: ", strStateName);

                        spnrStateList.setText(strStateName);
                        spnrCityList.setText("");
                        strCityId = "";
                        strCityName = "";
                    }

                    break;

                case CityListRequestCode:

                    if (!data.getExtras().getString("CityName").equals("") && !data.getExtras().getString("CityId").equals("")) {
                        strCityId = data.getExtras().getString("CityId");
                        Log.d("CityId id: ", strCityId);
                        strCityName = data.getExtras().getString("CityName");
                        Log.d("CityName: ", strCityName);

                        spnrCityList.setText(strCityName);
                    }

                    break;

                case REQUEST_IMAGE_CAPTURE:
                    dialog = new ProgressDialog(context);
                    try {

                        dialog.setMessage("Image processing...");
                        dialog.show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try
                                {
                                    Bitmap fullImage = MediaStore.Images.Media.getBitmap(getContentResolver(),imageUri);

                                    InputStream input = context.getContentResolver().openInputStream(imageUri);
                                    ExifInterface ei;
                                    if (Build.VERSION.SDK_INT > 23)
                                        ei = new ExifInterface(input);
                                    else
                                        ei = new ExifInterface(imageUri.getPath());

                                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                                    switch (orientation) {
                                        case ExifInterface.ORIENTATION_ROTATE_90:
                                            fullImage = rotateImage(fullImage, 90);
                                            break;
                                        case ExifInterface.ORIENTATION_ROTATE_180:
                                            fullImage = rotateImage(fullImage, 180);
                                            break;
                                        case ExifInterface.ORIENTATION_ROTATE_270:
                                            fullImage = rotateImage(fullImage, 270);
                                            break;
                                        default:
                                            break;
                                    }


                                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                    fullImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                                    byte[] byteArray = bytes.toByteArray();
                                    Bitmap scaledBitmap=ImageUtils.decodeSampledBitmapFromResource(byteArray,400,400);
                                    file = ImageUtils.saveImage(scaledBitmap, context);

                                    if (file != null) {
                                        if (strImage == "Gallery1"){
                                            strGalleryOnePath=imageFilePath;
                                            fileGalleryOne = file;
                                            Log.d("d", "Camera Gallery One file:" + fileGalleryOne);
                                            strGalleryPath = strGalleryOnePath;
//                                            Picasso.get().load(fileGalleryOne).into(ivVehicle1);
                                            Picasso.get().load(fileGalleryOne).into(ivVehicle1);

                                            if (!boolSetDefault){
                                                boolSetDefault = true;
                                                ibDefaultIamgeOne.setImageResource(R.drawable.vehicle_info_blank_image_select_fill);
                                                strSetGallerySelected = "1";
                                            }

                                        }else if (strImage == "Gallery2"){
                                            strGalleryTwoPath=imageFilePath;
                                            fileGalleryTwo = file;
                                            Log.d("d", "Camera Gallery Two file:" + fileGalleryTwo);
                                            strGalleryPath = strGalleryTwoPath;
                                            Picasso.get().load(fileGalleryTwo).into(ivVehicle2);

                                            if (!boolSetDefault){
                                                boolSetDefault = true;
                                                ibDefaultIamgeTwo.setImageResource(R.drawable.vehicle_info_blank_image_select_fill);
                                                strSetGallerySelected = "2";
                                            }

                                        }else if (strImage == "Gallery3"){
                                            strGalleryThreePath=imageFilePath;
                                            fileGalleryThree = file;
                                            Log.d("d", "Camera Gallery Three file:" + fileGalleryThree);
                                            strGalleryPath = strGalleryThreePath;
                                            Picasso.get().load(fileGalleryThree).into(ivVehicle3);

                                            if (!boolSetDefault){
                                                boolSetDefault = true;
                                                ibDefaultIamgeThree.setImageResource(R.drawable.vehicle_info_blank_image_select_fill);
                                                strSetGallerySelected = "3";
                                            }


                                        }else if (strImage == "Tax"){
                                            fileTax = file;
                                            Log.d("d", "Camera Tax file:" + fileTax);
                                            Picasso.get().load(fileTax).resize(200, 200).into(ivTaxToken);

                                        }else if (strImage == "Puc"){
                                            filePuc = file;
                                            Log.d("d", "Camera Puc file:" + filePuc);
                                            Picasso.get().load(filePuc).resize(200, 200).into(ivPuc);

                                        }else if (strImage == "License"){
                                            fileLicense = file;
                                            Log.d("d", "Camera License file: "+fileLicense);
                                            Picasso.get().load(fileLicense).resize(200, 200).into(ivLicensePlate);

                                        }else if (strImage.equalsIgnoreCase("RegionalTransportPermit")){
                                            fileRegionalTransportPermit = file;
                                            Log.d("d", "fileRegionalTransportPermit"+fileRegionalTransportPermit);
                                            Picasso.get().load(fileRegionalTransportPermit).resize(200, 200).into(ivRegionalTransportPermit);

                                        }

                                        else if (strImage.equalsIgnoreCase("RegistrationCertificateFront")){
                                            fileRegistrationCertificateFront = file;
                                            Log.d("d", "fileRegistrationCertificate"+fileRegistrationCertificateFront);
                                            Picasso.get().load(fileRegistrationCertificateFront).resize(200, 200).into(ivRegistrationCertificateFront);

                                        }

                                        else if (strImage.equalsIgnoreCase("RegistrationCertificateBack")){
                                            fileRegistrationCertificateBack = file;
                                            Log.d("d", "fileRegistrationCertificate"+fileRegistrationCertificateBack);
                                            Picasso.get().load(fileRegistrationCertificateBack).resize(200, 200).into(ivRegistrationCertificateBack);

                                        }


                                        else if (strImage.equalsIgnoreCase("FitnessCertificate")){
                                            fileFitnessCertificate = file;
                                            Log.d("d", "fileFitnessCertificate"+fileFitnessCertificate);
                                            Picasso.get().load(fileFitnessCertificate).resize(200, 200).into(ivFitnessCertificate);

                                        }else if (strImage.equalsIgnoreCase("InsuranceCertificate")){
                                            fileInsuranceCertificate = file;
                                            Log.d("d", "fileInsuranceCertificate"+fileInsuranceCertificate);
                                            Picasso.get().load(fileInsuranceCertificate).resize(200, 200).into(ivInsuranceCertificate);
                                        }

                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(context, "Failed saving!", Toast.LENGTH_SHORT).show();
                                }
                                finally{
                                    dialog.dismiss();
                                }
                            }
                        }, 4000);  // 4 sec to allow processing of image captured


                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, "Failed camera!", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case PICK_IMAGE_REQUEST:

                    try {

//                        ArrayList<String> photos = data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);

                        BufferedInputStream bufferedInputStream;
                        Bitmap bmp;

                        if(data != null) {
                            InputStream inputStream = context.getContentResolver().openInputStream(Objects.requireNonNull(data.getData()));
                            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...

                            bufferedInputStream = new BufferedInputStream(inputStream);
                            bmp = BitmapFactory.decodeStream(bufferedInputStream);

                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                            byte[] byteArray = bytes.toByteArray();
                            Bitmap scaledBitmap = ImageUtils.decodeSampledBitmapFromResource(byteArray, 400, 400);
                            file = ImageUtils.saveImageGallery(scaledBitmap, context);
                        }

                        if (strImage == "Gallery1") {
//                            strGalleryOnePath = "file://" + photos.get(0);
//                            Log.d("d", "Gallery 1 path : " + strGalleryOnePath);
//                            strSelectedGalOne = photos.get(0);
//                            Log.d("d", "Gallery image 1 to be uploaded: " + strSelectedGalOne);
                            fileGalleryOne = file;
                            Log.d("d", "Gallery 1 Image File::" + fileGalleryOne);
                            strGalleryPath = file.getAbsolutePath();
                            Picasso.get().load(fileGalleryOne).resize(200, 200).into(ivVehicle1);

                                if (!boolSetDefault){
                                    boolSetDefault = true;
                                    ibDefaultIamgeOne.setImageResource(R.drawable.vehicle_info_blank_image_select_fill);
                                    strSetGallerySelected = "1";
                                }

                        } else if (strImage == "Gallery2") {
//                            strGalleryTwoPath = "file://" + photos.get(0);
//                            Log.d("d", "Gallery 2 path : " + strGalleryTwoPath);
//                            strSelectedGalTwo = photos.get(0);
//                            Log.d("d", "Gallery image 2 to be uploaded: " + strSelectedGalTwo);
                            fileGalleryTwo = file;
                            Log.d("d", "Gallery 2 Image File::" + fileGalleryTwo);
                            strGalleryPath = file.getAbsolutePath();
                            Picasso.get().load(fileGalleryTwo).resize(200, 200).into(ivVehicle2);


                                if (!boolSetDefault){
                                    boolSetDefault = true;
                                    ibDefaultIamgeTwo.setImageResource(R.drawable.vehicle_info_blank_image_select_fill);
                                    strSetGallerySelected = "2";
                                }

                        } else if (strImage == "Gallery3") {
//                            strGalleryThreePath = "file://" + photos.get(0);
//                            Log.d("d", "Gallery 3 path : " + strGalleryThreePath);
//                            strSelectedGalThree = photos.get(0);
//                            Log.d("d", "Gallery image 3 to be uploaded: " + strSelectedGalThree);
                            fileGalleryThree = file;
                            Log.d("d", "Gallery 3 Image File::" + fileGalleryThree);
                            strGalleryPath = file.getAbsolutePath();
                            Picasso.get().load(fileGalleryThree).resize(200, 200).into(ivVehicle3);


                                if (!boolSetDefault){
                                    boolSetDefault = true;
                                    ibDefaultIamgeThree.setImageResource(R.drawable.vehicle_info_blank_image_select_fill);
                                    strSetGallerySelected = "3";
                                }

                        } else if (strImage == "Tax") {
//                            strTaxPath = "file://" + photos.get(0);
//                            Log.d("d", "Tax path : " + strTaxPath);
//                            strSelectedTax = photos.get(0);
//                            Log.d("d", "Tax to be uploaded: " + strSelectedTax);
                            fileTax = file;
                            Log.d("d", "Tax Image File::" + fileTax);

                            Picasso.get().load(file).resize(200, 200).into(ivTaxToken);

                        } else if (strImage == "Puc") {
//                            strPcuPath = "file://" + photos.get(0);
//                            Log.d("d", "Puc path : " + strPcuPath);
//                            strSelectedPcu = photos.get(0);
//                            Log.d("d", "Puc to be uploaded: " + strSelectedPcu);
                            filePuc = file;
                            Log.d("d", "Puc Image File::" + filePuc);

                            Picasso.get().load(file).resize(200, 200).into(ivPuc);

                        } else if (strImage == "License") {
//                            strLicenseIamgePath = "file://" + photos.get(0);
//                            Log.d("d", "License image path : " + strLicenseIamgePath);
//                            strSelectedLicense = photos.get(0);
//                            Log.d("d", "License to be uploaded: " + strSelectedLicense);
                            fileLicense = file;
                            Log.d("d", "License Image File::" + fileLicense);

                            Picasso.get().load(file).resize(200, 200).into(ivLicensePlate);

                        }else if (strImage.equalsIgnoreCase("RegionalTransportPermit")){
//                            strRegionalTransportImgPath = "file://" + photos.get(0);
//                            Log.d("d", "strRegionalTransportImgPath : " + strRegionalTransportImgPath);
//                            strSelectedRegionalTransport = photos.get(0);
//                            Log.d("d", "strSelectedRegionalTransport" + strSelectedRegionalTransport);
                            fileRegionalTransportPermit = file;
                            Log.d("d", "fileRegionalTransportPermit" + fileRegionalTransportPermit);

                            Picasso.get().load(file).resize(200, 200).into(ivRegionalTransportPermit);

                        }

                        else if (strImage.equalsIgnoreCase("RegistrationCertificateFront")){
//                            strRegistrationCertificateImgPath = "file://" + photos.get(0);
//                            Log.d("d", "strRegistrationCertificateImgPath : " + strRegistrationCertificateImgPath);
//                            strSelectedRegistrationCertificate = photos.get(0);
//                            Log.d("d", "strSelectedRegistrationCertificate" + strSelectedRegistrationCertificate);
                            fileRegistrationCertificateFront = file;
                            Log.d("d", "fileRegistrationCertificate" + fileRegistrationCertificateFront);

                            Picasso.get().load(file).resize(200, 200).into(ivRegistrationCertificateFront);

                        }

                        else if (strImage.equalsIgnoreCase("RegistrationCertificateBack")){
//                            strRegistrationCertificateImgPath = "file://" + photos.get(0);
//                            Log.d("d", "strRegistrationCertificateImgPath : " + strRegistrationCertificateImgPath);
//                            strSelectedRegistrationCertificate = photos.get(0);
//                            Log.d("d", "strSelectedRegistrationCertificate" + strSelectedRegistrationCertificate);
                            fileRegistrationCertificateBack = file;
                            Log.d("d", "fileRegistrationCertificate" + fileRegistrationCertificateBack);

                            Picasso.get().load(file).resize(200, 200).into(ivRegistrationCertificateBack);

                        }

                        else if (strImage.equalsIgnoreCase("FitnessCertificate")){

//                            strFitnessCertificateImgPath = "file://" + photos.get(0);
//                            Log.d("d", "strFitnessCertificateImgPath : " + strFitnessCertificateImgPath);
//                            strSelectedFitnessCertificate = photos.get(0);
//                            Log.d("d", "strSelectedFitnessCertificate" + strSelectedFitnessCertificate);
                            fileFitnessCertificate = file;
                            Log.d("d", "fileFitnessCertificate" + fileFitnessCertificate);

                            Picasso.get().load(file).resize(200, 200).into(ivFitnessCertificate);


                        }else if (strImage.equalsIgnoreCase("InsuranceCertificate")){

//                            strInsuranceCertificateImgPath = "file://" + photos.get(0);
//                            Log.d("d", "strInsuranceCertificateImgPath : " + strInsuranceCertificateImgPath);
//                            strSelectedInsuranceCertificate = photos.get(0);
//                            Log.d("d", "strSelectedInsuranceCertificate" + strSelectedInsuranceCertificate);
                            fileInsuranceCertificate = file;
                            Log.d("d", "fileInsuranceCertificate" + fileInsuranceCertificate);

                            Picasso.get().load(file).resize(200, 200).into(ivInsuranceCertificate);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }



    @Override
    public void onButtonClick(String strButtonText) {
        System.out.println("****strButtonText***"+strButtonText);

//        boolean result = CommonUtility.checkPermission(context);

        if (strButtonText.equals(activity.getResources().getString(R.string.Camera))) {
            new PrintClass("Alert Camera");
            userChoosenTask = "Camera";
//            if (result) {

                Dexter.withActivity(this).withPermissions( Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener()
                        {
                            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if(report.areAllPermissionsGranted()){

                                    takePhoto();
                                }

                                // check for permanent denial of any permission
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    // permission is denied permenantly, navigate user to app settings
                                    showAlert();
                                }
                            }
                            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                            {/* ... */

                                token.continuePermissionRequest();
                            }
                        }).check();
//            }


        } else if (strButtonText.equals(activity.getResources().getString(R.string.Gallery))) {
            new PrintClass("Alert Gallery");
            userChoosenTask = "Gallery";
//            if (result) {

                Dexter.withActivity(this).withPermissions( Manifest.permission.READ_EXTERNAL_STORAGE )
                        .withListener(new MultiplePermissionsListener()
                        {
                            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if(report.areAllPermissionsGranted()){

                                    mCallPhotoGallary();
                                }

                                // check for permanent denial of any permission
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    // permission is denied permenantly, navigate user to app settings
                                    showAlert();
                                }
                            }
                            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                            {/* ... */

                                token.continuePermissionRequest();
                            }
                        }).check();
//            }
        }

    }

    @Override
    public void VehicleMakeList(final ArrayList<VehicleTypeModel> arrListVehicleMake) {
        hideProgressDialog();
        if (arrListVehicleMake != null){
            listOfVehicleMake = new ArrayList<String>();
            if (arrListVehicleMake.size() > 0){
                for (int x=0;x<arrListVehicleMake.size();x++){
                    listOfVehicleMake.add(arrListVehicleMake.get(x).getTitle());
                }
                Log.d("d", "Array list vehicle make size::"+arrListVehicleMake.size());
                arrayAdapterVehicleMake = new ArrayAdapter<String>(context, R.layout.countryitem, listOfVehicleMake);
                arrayAdapterVehicleMake.setDropDownViewResource(R.layout.simpledropdownitem);
                spnrVehicleMake.setPrompt(getResources().getString(R.string.tvPleaseselectvehicleMake));
                spnrVehicleMake.setAdapter(arrayAdapterVehicleMake);

                spnrVehicleMake.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                        //brand means make
                        strVehicleBrandId = arrListVehicleMake.get(position).getId();
                        Log.d("d", "Vehicle Brand Id: "+strVehicleBrandId);
                        strVehicleModelTitle = arrListVehicleMake.get(position).getTitle();
                        Log.d("d", "Vehicle brand code: "+strVehicleModelTitle);

                        new VehicleModel_Webservice().vehicleModelWebservice(activity, CompanyAddVehicleActivity.this, strVehicleBrandId );
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }

        }else {
            new CustomToast(activity, getResources().getString(R.string.tvVehcileMakeNotFound));
        }
    }

    @Override
    public void VehicleModelList(final ArrayList<VehicleTypeModel> arrListVehicleModel) {

        hideProgressDialog();

        if(arrListVehicleModel != null){
            listOfVehicleModel = new ArrayList<String>();
            if(arrListVehicleModel.size() > 0){
                for (int x=0; x<arrListVehicleModel.size(); x++){
                    // listOfVehicleModel.add(arrListVehicleModel.get(x).getCode());
                    listOfVehicleModel.add(arrListVehicleModel.get(x).getTitle());

                }

                Log.d("d", "Array list vehicle model size::"+arrListVehicleModel.size());
                Log.d("d", "***listOfVehicleModel***"+listOfVehicleModel);

                arrayAdapterVehicleModel = new ArrayAdapter<String>(context, R.layout.countryitem, listOfVehicleModel);
                arrayAdapterVehicleModel.setDropDownViewResource(R.layout.simpledropdownitem);
                spnrVehicleBrand.setPrompt(getResources().getString(R.string.tvPlsSelectVehicleBrand));
                spnrVehicleBrand.setAdapter(arrayAdapterVehicleModel);

                spnrVehicleBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                        strVehicleModelId = arrListVehicleModel.get(position).getId();
                        Log.d("d", "Vehicle model id::"+strVehicleModelId);

                        strVehicleMakeId = arrListVehicleModel.get(position).getMake_id();
                        Log.d("d", "Vehicle make id::"+strVehicleMakeId);

                        strVehicleModelTitle = arrListVehicleModel.get(position).getTitle();
                        Log.d("d", "Vehicle model title: "+strVehicleModelTitle);

                        strVehicleTypeName = arrListVehicleModel.get(position).getType_name();
                        Log.d("d", "***strVehicleTypeName***"+strVehicleTypeName);
                        spnrVehicleType.setText(strVehicleTypeName);

                        strVehicleTypeId = arrListVehicleModel.get(position).getVehicle_type_id();
                        Log.d("***Vehicle type id***", strVehicleTypeId);

                        strSittingCapacity = arrListVehicleModel.get(position).getSitting_capacity();
                        strLuggageCapacity = arrListVehicleModel.get(position).getLuggae_capacity();
                        Log.d("d", "***strSittingCapacity***"+strSittingCapacity);
                        Log.d("d", "***strLuggageCapacity***"+strLuggageCapacity);

                        strACMin = arrListVehicleModel.get(position).getAc_hourly_min_rate();
                        strACMax = arrListVehicleModel.get(position).getAc_hourly_max_rate();
                        strNonACMin = arrListVehicleModel.get(position).getNonac_hourly_min_rate();
                        strNonACMax = arrListVehicleModel.get(position).getNonac_hourly_max_rate();

                        strACNightMin = arrListVehicleModel.get(position).getAc_min_night_rate();
                        strACNightMax = arrListVehicleModel.get(position).getAc_max_night_rate();
                        strNONACNightMin = arrListVehicleModel.get(position).getNonac_min_night_rate();
                        strNOnACNightMax = arrListVehicleModel.get(position).getNonac_max_night_rate();

                        strOvertimeACMin = arrListVehicleModel.get(position).getAc_hourly_overtime_min_rate();
                        strOvertimeACMax = arrListVehicleModel.get(position).getAc_hourly_overtime_max_rate();
                        strOvertimeNonACMin = arrListVehicleModel.get(position).getNonac_hourly_overtime_min_rate();
                        strOvertimeNonACMax = arrListVehicleModel.get(position).getNonac_hourly_overtime_max_rate();

                        intMaxPassenger = Integer.parseInt(strSittingCapacity);
                        listOfMaxPassenger = new ArrayList<Integer>();
                        for (int i=1; i<=intMaxPassenger; i++ ){
                            listOfMaxPassenger.add(i);
                        }

                        arrayAdapterMaxPassenger = new ArrayAdapter<Integer>(context, R.layout.countryitem, listOfMaxPassenger);
                        arrayAdapterMaxPassenger.setDropDownViewResource(R.layout.simpledropdownitem);
                        spnrMaxPassenger.setPrompt(getResources().getString(R.string.promtMaxPassenger));
                        spnrMaxPassenger.setAdapter(arrayAdapterMaxPassenger);

                        intMaxLuggage = Integer.parseInt(strLuggageCapacity);
                        listOfMaxLuggage = new ArrayList<Integer>();
                        for (int i=0; i<=intMaxLuggage; i++){
                            listOfMaxLuggage.add(i);
                        }

                        arrayAdapterMaxLuggage = new ArrayAdapter<Integer>(context, R.layout.countryitem, listOfMaxLuggage);
                        arrayAdapterMaxLuggage.setDropDownViewResource(R.layout.simpledropdownitem);
                        spnrMaxLuggage.setPrompt(getResources().getString(R.string.promptMaxLuggage));
                        spnrMaxLuggage.setAdapter(arrayAdapterMaxLuggage);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                    }
                });
            }
        }
    }

    @Override
    public void vehicleSaveOnClick(String success, String msg, String strVehicleId, final String userType) {
        this.strVehicleTypeId = strVehicleId;
        ImageUtils.deleteImageGallery();
    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(activity, activity.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) CompanyAddVehicleActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}

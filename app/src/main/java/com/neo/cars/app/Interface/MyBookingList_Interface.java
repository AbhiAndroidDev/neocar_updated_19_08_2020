package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.MyBookingListModel;
import com.neo.cars.app.SetGet.MyVehicleBookingModel;

import java.util.ArrayList;

/**
 * Created by joydeep on 4/5/18.
 */

public interface MyBookingList_Interface {

    public void onMyBookingList(ArrayList<MyBookingListModel> arrlistVehicle);
}

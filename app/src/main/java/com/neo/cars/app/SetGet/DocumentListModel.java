package com.neo.cars.app.SetGet;

/**
 * Created by parna on 26/3/18.
 */

public class DocumentListModel {

    String document_code;
    String document_name;


    public String getDocument_code() {
        return document_code;
    }

    public void setDocument_code(String document_code) {
        this.document_code = document_code;
    }

    public String getDocument_name() {
        return document_name;
    }

    public void setDocument_name(String document_name) {
        this.document_name = document_name;
    }
}

package com.neo.cars.app.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.PaymentInfoModel;

import java.util.List;

/**
 * Created by joydeep on 18/5/18.
 */

public class PaymentInformationAdapter extends RecyclerView.Adapter<PaymentInformationAdapter.MyViewHolder> {

    private List<PaymentInfoModel> PaymentInfoList;

    private Context context;
    private LinearLayout refund_amount_info_ll;


    public class MyViewHolder extends RecyclerView.ViewHolder {
       public TextView tv_BookingDate,tv_pickuplocation,tv_droplocation, amount, refund, tv_booking_refrence_id, tv_trip_status;
       public RelativeLayout paymentinfo_relative;

        public MyViewHolder(View view) {
            super(view);
            tv_BookingDate= view.findViewById(R.id.tv_BookingDate);
            tv_pickuplocation= view.findViewById(R.id.tv_pickuplocation);
            tv_droplocation= view.findViewById(R.id.tv_droplocation);
            amount = view.findViewById(R.id.tv_Amount);
            refund = view.findViewById(R.id.tv_refund);
            paymentinfo_relative=view.findViewById(R.id.paymentinfo_relative);
            refund_amount_info_ll=view.findViewById(R.id.refund_amount_info_ll);
            tv_booking_refrence_id=view.findViewById(R.id.tv_booking_refrence_id);
            tv_trip_status=view.findViewById(R.id.tv_trip_status);

            refund_amount_info_ll.setVisibility(View.VISIBLE);

        }
    }

    public PaymentInformationAdapter(Context context, List<PaymentInfoModel> myPaymentInfoList) {
        this.context = context;
        PaymentInfoList = myPaymentInfoList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.paymentinfo_inflate, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_BookingDate.setText(PaymentInfoList.get(position).getBooking_date());
        holder.tv_pickuplocation.setText(PaymentInfoList.get(position).getPickup_location());
        holder.tv_droplocation.setText(PaymentInfoList.get(position).getDrop_location());

        holder.tv_booking_refrence_id.setText(PaymentInfoList.get(position).getBooking_ref_id());
        holder.tv_trip_status.setText(PaymentInfoList.get(position).getTrip_status());

        if(!PaymentInfoList.get(position).getAmount().equalsIgnoreCase("NA")) {
            holder.amount.setText(context.getResources().getString(R.string.Rs)+" "+ PaymentInfoList.get(position).getAmount());
        }else {
            holder.amount.setText(PaymentInfoList.get(position).getAmount());
        }

        if(!PaymentInfoList.get(position).getRefund().equalsIgnoreCase("NA")) {
            holder.refund.setText(context.getResources().getString(R.string.Rs)+" "+PaymentInfoList.get(position).getRefund());
        }else {
            holder.refund.setText(PaymentInfoList.get(position).getRefund());
        }

    }


    @Override
    public int getItemCount() {
        return PaymentInfoList.size();

    }
}

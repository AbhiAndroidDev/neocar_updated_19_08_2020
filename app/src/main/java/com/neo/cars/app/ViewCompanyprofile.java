package com.neo.cars.app;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.neo.cars.app.Interface.OtpVerification_Interface_company;
import com.neo.cars.app.Interface.Profile_Interface;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.Webservice.CompPCMobnoVerifyWebService;
import com.neo.cars.app.Webservice.CompanyProfile_WebService;
import com.neo.cars.app.dialog.CustomDialogOTP;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ViewCompanyprofile extends RootActivity implements Profile_Interface, OtpVerification_Interface_company {

    private Context context;
    private ConnectionDetector cd;
    private Toolbar toolbar;
    private RelativeLayout rlParent, rlCompanyPicName, rlCompanyDetails, rlBackLayout, rlAddLayout;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvCompanyName, tvRegisteredAddress, tvCompanyEmail,
            tvCompanyPhoneNo, tvCompMobNo, tvGSTNo, tvPANNo, tvPCPName, tvPCMNumber, tvSCPName, tvSCMNumber,
            tvBANKACumber, tvIFSCcode;

    private ImageView ivPrimaryContactID, ivPrimaryContactIDBack, ivSecondaryContactID, ivSecondaryContactIDBack, ivSCPANCardID, ivCancelledCheque;
    private UserLoginDetailsModel userLoginDetails;
    private ImageButton ibEditMenu;
    private Gson gson;
    private SharedPrefUserDetails sharedPref;
    private CircularImageViewBorder civCompanyProfilePic;
    private int transitionflag = StaticClass.transitionflagNext;
    private ImageView ivAadhaarCard, ivSecondaryDoc, ivCompPhnoVerify, ivCompMobnoVerify, ivCompanyEmailVerify, ivCompPCMobnoVerify, ivCompSCMobnoVerify;
    boolean phNoVerified=false, primaryMobileNoVerified = false, secondaryMobileNoVerified = false;  //true for mob no verified and false for not verified mob no
    boolean emailVerified = false; // true for email verified and false for not verified email
    private String strProfileEditable = "", strProfileEditableMessage = "", strMobileNo="", strPrimaryMobileNo="", strSecondaryMobileNo="";

    final int PERMISSION_REQUEST_CODE = 111;
    private UserLoginDetailsModel muserLoginDetailsModel;

    private BottomViewCompany bottomViewCompany = new BottomViewCompany();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_companyprofile);

        gson = new Gson();
        userLoginDetails = new UserLoginDetailsModel();
        sharedPref = new SharedPrefUserDetails(ViewCompanyprofile.this);
        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        userLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        new AnalyticsClass(ViewCompanyprofile.this);

        initialize();
        if (cd.isConnectingToInternet()) {
            new CompanyProfile_WebService().companyProfile(ViewCompanyprofile.this);

        } else {
            startActivity(new Intent(this, NetworkNotAvailable.class));
            transitionflag = StaticClass.transitionflagBack;
        }
        listener();
    }

    private void initialize() {
        context = this;
        cd = new ConnectionDetector(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvProfileHeader));

        tvCompanyName = findViewById(R.id.tvCompanyName);
        tvRegisteredAddress = findViewById(R.id.tvRegisteredAddress);
        tvCompanyEmail = findViewById(R.id.tvCompanyEmail);
        tvCompanyPhoneNo = findViewById(R.id.tvCompanyPhoneNo);
        tvCompMobNo = findViewById(R.id.tvCompMobNo);
        tvGSTNo = findViewById(R.id.tvGSTNo);
        tvPANNo = findViewById(R.id.tvPANNo);
        tvPCPName = findViewById(R.id.tvPCPName);
        tvPCMNumber = findViewById(R.id.tvPCMNumber);
        tvSCPName = findViewById(R.id.tvSCPName);
        tvSCMNumber = findViewById(R.id.tvSCMNumber);
        tvBANKACumber = findViewById(R.id.tvBANKACumber);
        tvIFSCcode = findViewById(R.id.tvIFSCcode);

        rlBackLayout = findViewById(R.id.rlBackLayout);
        rlAddLayout = findViewById(R.id.rlAddLayout);
        rlAddLayout.setVisibility(View.VISIBLE);

        ibEditMenu = findViewById(R.id.ibEditMenu);
        ibEditMenu.setVisibility(View.VISIBLE);

        civCompanyProfilePic = findViewById(R.id.civCompanyProfilePic);
        ivPrimaryContactID = findViewById(R.id.ivPrimaryContactID);
        ivPrimaryContactIDBack = findViewById(R.id.ivPrimaryContactIDBack);
        ivSecondaryContactID = findViewById(R.id.ivSecondaryContactID);
        ivSecondaryContactIDBack = findViewById(R.id.ivSecondaryContactIDBack);
        ivSCPANCardID = findViewById(R.id.ivSCPANCardID);
//        ivCompPhnoVerify = findViewById(R.id.ivCompPhnoVerify);
        ivCompanyEmailVerify = findViewById(R.id.ivCompanyEmailVerify);
        ivCompMobnoVerify = findViewById(R.id.ivCompMobnoVerify);
        ivCompPCMobnoVerify = findViewById(R.id.ivCompPCMobnoVerify);
        ivCompSCMobnoVerify = findViewById(R.id.ivCompSCMobnoVerify);
        ivCancelledCheque = findViewById(R.id.ivCancelledCheque);

        bottomViewCompany.BottomViewCompany(ViewCompanyprofile.this, StaticClass.Menu_profile_company);
    }

    private void listener() {

        civCompanyProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ViewCompanyprofile.this, ImageFullActivity.class);
                i.putExtra("imgUrl", muserLoginDetailsModel.getProfile_pic());
                startActivity(i);

            }
        });

        ibEditMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if("N".equals(strProfileEditable)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ViewCompanyprofile.this);
                    builder.setMessage(strProfileEditableMessage)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }else{

                    transitionflag = StaticClass.transitionflagNext;
                    Intent editprofileIntent = new Intent(ViewCompanyprofile.this, EditCompanyProfileActivity.class);
                    startActivity(editprofileIntent);
                    finish();

                }
            }
        });

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

//        ivCompMobnoVerify.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(!phNoVerified){
//                    if(cd.isConnectingToInternet()){
//
//                        new CompanyGetLoginOTPWebService().GetLoginOtpCompany(ViewCompanyprofile.this, strMobileNo);
//                    }else{
//                        startActivity(new Intent(ViewCompanyprofile.this, NetworkNotAvailable.class));
//                    }
//                }
//            }
//        });

        ivCompPCMobnoVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!primaryMobileNoVerified){
                    if(cd.isConnectingToInternet()){
                        String strUserId = muserLoginDetailsModel.getId();
                        new CompPCMobnoVerifyWebService().GetPCMobileOtp(ViewCompanyprofile.this, strPrimaryMobileNo, "P");

                    }else{
                        startActivity(new Intent(ViewCompanyprofile.this, NetworkNotAvailable.class));
                    }
                }
            }
        });

        tvPCMNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!TextUtils.isEmpty(muserLoginDetailsModel.getPrimary_contact_mobile())){

                    contactPermission(muserLoginDetailsModel.getPrimary_contact_mobile());
//                    callMethod(muserLoginDetailsModel.getPrimary_contact_mobile());
                }
            }
        });


        tvSCMNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!TextUtils.isEmpty(muserLoginDetailsModel.getSecondary_contact_mobile())){
                    contactPermission(muserLoginDetailsModel.getSecondary_contact_mobile());
//                    callMethod(muserLoginDetailsModel.getSecondary_contact_mobile());
                }
            }
        });

        ivCompSCMobnoVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!secondaryMobileNoVerified){
                    if(cd.isConnectingToInternet()){
                        new CompPCMobnoVerifyWebService().GetPCMobileOtp(ViewCompanyprofile.this, strSecondaryMobileNo, "S");

                    }else{
                        startActivity(new Intent(ViewCompanyprofile.this, NetworkNotAvailable.class));
                    }
                }
            }
        });


//        ivCompanyEmailVerify.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!emailVerified){
//                    new CustomToast(ViewCompanyprofile.this, getResources().getString(R.string.strEmailVerificationCheck));
//                }
//            }
//        });

    }


    private void contactPermission(final String contact_mobile){

        Dexter.withActivity(ViewCompanyprofile.this).withPermissions( Manifest.permission.CALL_PHONE,
                Manifest.permission.READ_PHONE_STATE)
                .withListener(new MultiplePermissionsListener()
                {
                    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
                    @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if(report.areAllPermissionsGranted()){

                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + contact_mobile));
                            startActivity(intent);
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                            showAlert();
                        }
                    }
                    @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                    {/* ... */

                        token.continuePermissionRequest();
                    }
                }).check();
    }


    private void callMethod(String passenger_contact) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                requestPermission();
            } else {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + passenger_contact));
                startActivity(intent);
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + passenger_contact));
            startActivity(intent);
        }

    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (cd.isConnectingToInternet()) {
            new CompanyProfile_WebService().companyProfile(ViewCompanyprofile.this);

        } else {
            startActivity(new Intent(this, NetworkNotAvailable.class));
            transitionflag = StaticClass.transitionflagBack;
        }

//        if(StaticClass.BottomProfileCompany){
//            finish();
//        }
//
//        if(StaticClass.EditProfileIsSaveCompany){
//            finish();
//        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
    }

    @Override
    public void MyProfile(UserLoginDetailsModel userLoginDetailsModel) {
        muserLoginDetailsModel = new UserLoginDetailsModel();
        muserLoginDetailsModel = userLoginDetailsModel;

        if (!userLoginDetailsModel.getCompany_name().equals("") && !userLoginDetailsModel.getCompany_name().equals("null")) {
            tvCompanyName.setText(userLoginDetailsModel.getCompany_name());
        }else{
            tvCompanyName.setText("N/A");
        }

        if (!userLoginDetailsModel.getCompany_address().equals("") && !userLoginDetailsModel.getCompany_address().equals("null")) {
            tvRegisteredAddress.setText(userLoginDetailsModel.getCompany_address());
        }else{
            tvRegisteredAddress.setText("N/A");
        }

        if (!userLoginDetailsModel.getEmail().equals("") && !userLoginDetailsModel.getEmail().equals("null")) {
            tvCompanyEmail.setText(userLoginDetailsModel.getEmail());
        }else{
            tvCompanyEmail.setText("N/A");
        }

//        if (!userLoginDetailsModel.getEmail_verified().equals("") && !userLoginDetailsModel.getEmail_verified().equals("null")){
//            if("Y".equalsIgnoreCase(userLoginDetailsModel.getEmail_verified())){
//                emailVerified = true;
//                ivCompanyEmailVerify.setVisibility(View.VISIBLE);
//                ivCompanyEmailVerify.setImageResource(R.drawable.verified_icon);
//
//            }else if ("N".equalsIgnoreCase(userLoginDetailsModel.getEmail_verified())){
//                emailVerified = false;
//                ivCompanyEmailVerify.setVisibility(View.VISIBLE);
//                ivCompanyEmailVerify.setImageResource(R.drawable.not_verified_icon);
//
//            }
//        }

        if (!userLoginDetailsModel.getCompany_phone_number().equals("") && !userLoginDetailsModel.getCompany_phone_number().equals("null")) {
            tvCompanyPhoneNo.setText(userLoginDetailsModel.getCompany_phone_number());
        }else{
            tvCompanyPhoneNo.setText("N/A");
        }

        if (!userLoginDetailsModel.getCompany_mobile_number().equals("") && !userLoginDetailsModel.getCompany_mobile_number().equals("null")) {
            strMobileNo = userLoginDetailsModel.getCompany_mobile_number();
            tvCompMobNo.setText(userLoginDetailsModel.getCompany_mobile_number());
        }else{
            tvCompMobNo.setText("N/A");
        }

//        if(!userLoginDetailsModel.getMobile_verified().equals("") && !userLoginDetailsModel.getMobile_verified().equals("null")){
//            if("Y".equalsIgnoreCase(userLoginDetailsModel.getMobile_verified())){
//                phNoVerified = true;
//                ivCompMobnoVerify.setVisibility(View.VISIBLE);
//                ivCompMobnoVerify.setImageResource(R.drawable.verified_icon);
//
//            }else if ("N".equalsIgnoreCase(userLoginDetailsModel.getMobile_verified())){
//                phNoVerified = false;
//                ivCompMobnoVerify.setVisibility(View.VISIBLE);
//                ivCompMobnoVerify.setImageResource(R.drawable.not_verified_icon);
//
//            }
//        }

        if (!userLoginDetailsModel.getCompany_gst_number().equals("") && !userLoginDetailsModel.getCompany_gst_number().equals("null")) {
            tvGSTNo.setText(userLoginDetailsModel.getCompany_gst_number());
        }else{
            tvGSTNo.setText("N/A");
        }

        if (!userLoginDetailsModel.getCompany_pan_number().equals("") && !userLoginDetailsModel.getCompany_pan_number().equals("null")) {
            tvPANNo.setText(userLoginDetailsModel.getCompany_pan_number());
        }else{
            tvPANNo.setText("N/A");
        }

        if (!userLoginDetailsModel.getPrimary_contact_name().equals("") && !userLoginDetailsModel.getPrimary_contact_name().equals("null")) {
            tvPCPName.setText(userLoginDetailsModel.getPrimary_contact_name());
        }else{
            tvPCPName.setText("N/A");
        }

        if (!userLoginDetailsModel.getPrimary_contact_mobile().equals("") && !userLoginDetailsModel.getPrimary_contact_mobile().equals("null")) {
            strPrimaryMobileNo = userLoginDetailsModel.getPrimary_contact_mobile();
            tvPCMNumber.setText(userLoginDetailsModel.getPrimary_contact_mobile());

            tvPCMNumber.setTextColor(Color.parseColor("#1e65a6"));
            tvPCMNumber.setPaintFlags(tvPCMNumber.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        }else{
            tvPCMNumber.setText("N/A");
        }

        if(!userLoginDetailsModel.getPrimary_mobile_verified().equals("") && !userLoginDetailsModel.getPrimary_mobile_verified().equals("null")){
            if("Y".equalsIgnoreCase(userLoginDetailsModel.getPrimary_mobile_verified())){
                primaryMobileNoVerified = true;
                ivCompPCMobnoVerify.setVisibility(View.VISIBLE);
                ivCompPCMobnoVerify.setImageResource(R.drawable.verified_icon);

            }else if ("N".equalsIgnoreCase(userLoginDetailsModel.getPrimary_mobile_verified())){
                primaryMobileNoVerified = false;
                ivCompPCMobnoVerify.setVisibility(View.VISIBLE);
                ivCompPCMobnoVerify.setImageResource(R.drawable.not_verified_icon);

            }
        }

        if (!userLoginDetailsModel.getSecondary_contact_name().equals("") && !userLoginDetailsModel.getSecondary_contact_name().equals("null")) {
            tvSCPName.setText(userLoginDetailsModel.getSecondary_contact_name());
        }else{
            tvSCPName.setText("N/A");
        }

        if (!userLoginDetailsModel.getSecondary_contact_mobile().equals("") && !userLoginDetailsModel.getSecondary_contact_mobile().equals("null")) {
            strSecondaryMobileNo = userLoginDetailsModel.getSecondary_contact_mobile();
            tvSCMNumber.setText(userLoginDetailsModel.getSecondary_contact_mobile());

            tvSCMNumber.setTextColor(Color.parseColor("#1e65a6"));
            tvSCMNumber.setPaintFlags(tvPCMNumber.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        }else{
            tvSCMNumber.setText("N/A");
        }

        if(!userLoginDetailsModel.getSecondary_mobile_verified().equals("") && !userLoginDetailsModel.getSecondary_mobile_verified().equals("null")){
            if("Y".equalsIgnoreCase(userLoginDetailsModel.getSecondary_mobile_verified())){
                secondaryMobileNoVerified = true;
                ivCompSCMobnoVerify.setVisibility(View.VISIBLE);
                ivCompSCMobnoVerify.setImageResource(R.drawable.verified_icon);

            }else if ("N".equalsIgnoreCase(userLoginDetailsModel.getSecondary_mobile_verified())){
                secondaryMobileNoVerified = false;
                ivCompSCMobnoVerify.setVisibility(View.VISIBLE);
                ivCompSCMobnoVerify.setImageResource(R.drawable.not_verified_icon);

            }
        }

        if (!userLoginDetailsModel.getBank_account_number().equals("") && !userLoginDetailsModel.getBank_account_number().equals("null")) {
            tvBANKACumber.setText(userLoginDetailsModel.getBank_account_number());
        }else{
            tvBANKACumber.setText("N/A");
        }

        if (!userLoginDetailsModel.getIfsc_code().equals("") && !userLoginDetailsModel.getIfsc_code().equals("null")) {
            tvIFSCcode.setText(userLoginDetailsModel.getIfsc_code());
        }else{
            tvIFSCcode.setText("N/A");
        }

        if (!TextUtils.isEmpty(userLoginDetailsModel.getPrimary_contact_id())) {
            try {
                Picasso.get().load(userLoginDetailsModel.getPrimary_contact_id()).into(ivPrimaryContactID);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!TextUtils.isEmpty(userLoginDetailsModel.getPrimary_id_back())) {
            try {
                Picasso.get().load(userLoginDetailsModel.getPrimary_id_back()).into(ivPrimaryContactIDBack);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!TextUtils.isEmpty(userLoginDetailsModel.getSecondary_contact_id())) {
            try {
                Picasso.get().load(userLoginDetailsModel.getSecondary_contact_id()).into(ivSecondaryContactID);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!TextUtils.isEmpty(userLoginDetailsModel.getSecondary_id_back())) {
            try {
                Picasso.get().load(userLoginDetailsModel.getSecondary_id_back()).into(ivSecondaryContactIDBack);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!userLoginDetailsModel.getCompany_pan_card().equals("") && !userLoginDetailsModel.getCompany_pan_card().equals("null")) {
            try {
                Picasso.get().load(userLoginDetailsModel.getCompany_pan_card()).into(ivSCPANCardID);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!userLoginDetailsModel.getCancel_check().equals("") && !userLoginDetailsModel.getCancel_check().equals("null")) {
            try {
                Picasso.get().load(userLoginDetailsModel.getCancel_check()).into(ivCancelledCheque);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(!userLoginDetailsModel.getProfile_pic().equals("") && !userLoginDetailsModel.getProfile_pic().equals("null")){
            try {
                Picasso.get().load(userLoginDetailsModel.getProfile_pic()).into(civCompanyProfilePic);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomViewCompany.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(ViewCompanyprofile.this, transitionflag);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        transitionflag = StaticClass.transitionflagBack;

    }


    @Override
    public void OtpVerificationCompany(String st_mobile, String otp, String numberType) {
        new CustomDialogOTP(context,ViewCompanyprofile.this, "" , st_mobile, sharedPref.getUserType(), numberType);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) ViewCompanyprofile.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}

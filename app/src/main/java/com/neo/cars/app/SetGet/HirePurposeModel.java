package com.neo.cars.app.SetGet;

/**
 * Created by parna on 10/5/18.
 */

public class HirePurposeModel {

    String id;
    String purpose;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }
}



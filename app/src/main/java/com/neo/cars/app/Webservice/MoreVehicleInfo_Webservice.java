package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.MoreVehicleInfo_Interface;
import com.neo.cars.app.Interface.Profile_Interface;
import com.neo.cars.app.Interface.UserVehicleList_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.PickUpLocationModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleGalleryModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 23/4/18.
 */

public class MoreVehicleInfo_Webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private VehicleTypeModel vehicleTypeModel;
    private JSONObject details;
    private ArrayList<VehicleTypeModel> arrListVehicleTypeModel;
    private PickUpLocationModel pickUpLocationModel;
    private VehicleGalleryModel vehicleGalleryModel;
    private MoreVehicleInfo_Interface ImoreVehicleinfo_Interface;
    private int transitionflag = StaticClass.transitionflagNext;


    public void moreVehicleInfo (Activity context, MoreVehicleInfo_Interface mMoreVehicleInfo_Interface,final String strvehicleId){
        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        ImoreVehicleinfo_Interface = mMoreVehicleInfo_Interface;

        sharedPref = new SharedPrefUserDetails(mcontext);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();
        vehicleTypeModel = new VehicleTypeModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();

        StringRequest morevehicleinforequest = new StringRequest(Request.Method.POST, Urlstring.vehicle_details,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d(":: Response details:: ", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("vehicle_id", strvehicleId);
                params.put("user_id", UserLoginDetails.getId());

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        morevehicleinforequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(morevehicleinforequest);
    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response){
        JSONObject jobj_main = null;

        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("vehicle_details").optString("user_deleted");
            Msg = jobj_main.optJSONObject("vehicle_details").optString("message");
            Status= jobj_main.optJSONObject("vehicle_details").optString("status");

            details=jobj_main.optJSONObject("vehicle_details").optJSONObject("details");
            arrListVehicleTypeModel = new ArrayList<VehicleTypeModel>();

            if (Status.equals(StaticClass.SuccessResult)){
                vehicleTypeModel.setId(details.optString("id"));
                vehicleTypeModel.setVehicle_type_id(details.optString("vehicle_type_id"));
                vehicleTypeModel.setMake(details.optString("make"));
                vehicleTypeModel.setModel(details.optString("model"));
                vehicleTypeModel.setYear(details.optString("year"));
                vehicleTypeModel.setKm_travelled(details.optString("km_travelled"));
                vehicleTypeModel.setDescription(details.optString("description"));
                vehicleTypeModel.setAc_available(details.optString("ac_available"));
                vehicleTypeModel.setHourly_rate(details.optString("hourly_rate"));
                vehicleTypeModel.setHourly_overtime_rate(details.optString("hourly_overtime_rate"));
                vehicleTypeModel.setMax_luggage(details.optString("max_luggage"));
                vehicleTypeModel.setMax_passenger(details.optString("max_passenger"));
                vehicleTypeModel.setOvertime_available(details.optString("overtime_available"));


                vehicleTypeModel.setNight_charge_available(details.optString("night_charge_available"));
                vehicleTypeModel.setNight_rate(details.optString("night_rate"));
                vehicleTypeModel.setToll_parking_charge(details.optString("toll_parking_charge"));


                vehicleTypeModel.setUser_id(details.optString("user_id"));
                vehicleTypeModel.setDriver_id(details.optString("driver_id"));
                vehicleTypeModel.setVehicle_reg_number(details.optString("vehicle_reg_number"));
                vehicleTypeModel.setLicense_plate_no(details.optString("license_plate_no"));
                vehicleTypeModel.setLicense_plate_image(details.optString("license_plate_image"));
                vehicleTypeModel.setTax_token_image(details.optString("tax_token_image"));
                vehicleTypeModel.setPcu_paper_image(details.optString("pcu_paper_image"));


                vehicleTypeModel.setValid_permit(details.optString("valid_permit"));
                vehicleTypeModel.setRegistration_certificate(details.optString("registration_certificate"));
                vehicleTypeModel.setRegistration_certificate_back(details.optString("registration_certificate_back"));
                vehicleTypeModel.setFitness_certificate(details.optString("fitness_certificate"));
                vehicleTypeModel.setInsurance_certificate(details.optString("insurance_certificate"));


                vehicleTypeModel.setBrand_name(details.optString("brand_name"));
                vehicleTypeModel.setModel_name(details.optString("model_name"));
                vehicleTypeModel.setVehicle_type_name(details.optString("vehicle_type_name"));
                vehicleTypeModel.setState_name(details.optString("state_name"));
                vehicleTypeModel.setCity_name(details.optString("city_name"));
                vehicleTypeModel.setAc_available(details.optString("ac_available"));
                vehicleTypeModel.setState_id(details.optString("state_id"));
                vehicleTypeModel.setCity_id(details.optString("city_id"));
                vehicleTypeModel.setOther_pickup_location(details.optString("other_pickup_location"));

                //fetching AC price details
                JSONObject jsonObjectAC = details.optJSONObject("ac_price_details");
                vehicleTypeModel.setAc_ac_facility(jsonObjectAC.optString("ac_facility"));
                vehicleTypeModel.setAc_hourly_min_rate(jsonObjectAC.optString("hourly_min_rate"));
                vehicleTypeModel.setAc_hourly_max_rate(jsonObjectAC.optString("hourly_max_rate"));
                vehicleTypeModel.setAc_hourly_overtime_min_rate(jsonObjectAC.optString("hourly_overtime_min_rate"));
                vehicleTypeModel.setAc_hourly_overtime_max_rate(jsonObjectAC.optString("hourly_overtime_max_rate"));

                //fetching non-AC price details
                JSONObject jsonObjectNonAC = details.optJSONObject("non_ac_price_details");
                vehicleTypeModel.setNonac_ac_facility(jsonObjectNonAC.optString("non_ac_facility"));
                vehicleTypeModel.setNonac_hourly_min_rate(jsonObjectNonAC.optString("hourly_min_rate"));
                vehicleTypeModel.setNonac_hourly_max_rate(jsonObjectNonAC.optString("hourly_max_rate"));
                vehicleTypeModel.setNonac_hourly_overtime_min_rate(jsonObjectNonAC.optString("hourly_overtime_min_rate"));
                vehicleTypeModel.setNonac_hourly_overtime_max_rate(jsonObjectNonAC.optString("hourly_overtime_max_rate"));

                arrListVehicleTypeModel.add(vehicleTypeModel);

                JSONArray jsonArrPickUpLocation = details.optJSONArray("pickup_location");
                if(jsonArrPickUpLocation != null){
                    ArrayList<PickUpLocationModel> arrlistPickUpLocation = new ArrayList<PickUpLocationModel>();
                    for (int i=0; i<jsonArrPickUpLocation.length(); i++){
                        JSONObject jsonObjectPickUpLocation = jsonArrPickUpLocation.getJSONObject(i);
                        pickUpLocationModel = new PickUpLocationModel();

                        pickUpLocationModel.setId(jsonObjectPickUpLocation.optString("id"));
                        pickUpLocationModel.setPickup_location(jsonObjectPickUpLocation.optString("pickup_location"));;

                        arrlistPickUpLocation.add(pickUpLocationModel);
                    }
                    vehicleTypeModel.setArr_PickUpLocationModel(arrlistPickUpLocation);
                }

                JSONArray jsonArrVehicleGallery = details.optJSONArray("vehicle_gallery");
                if(jsonArrVehicleGallery != null){
                    ArrayList<VehicleGalleryModel> arrlistVehicleGallery = new ArrayList<VehicleGalleryModel>();
                    for (int j=0; j<jsonArrVehicleGallery.length(); j++){
                        JSONObject jsonObjectVehicleGallery = jsonArrVehicleGallery.getJSONObject(j);
                        vehicleGalleryModel = new VehicleGalleryModel();

                        vehicleGalleryModel.setId(jsonObjectVehicleGallery.optString("id"));
                        vehicleGalleryModel.setUser_vehicle_id(jsonObjectVehicleGallery.optString("user_vehicle_id"));
                        vehicleGalleryModel.setImage_file(jsonObjectVehicleGallery.optString("image_file"));
                        vehicleGalleryModel.setIs_default(jsonObjectVehicleGallery.optString("is_default"));

                        arrlistVehicleGallery.add(vehicleGalleryModel);

                    }
                    vehicleTypeModel.setArr_VehicleGalleryModel(arrlistVehicleGallery);
                }

            }else if (Status.equals(StaticClass.ErrorResult)){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("vehicle_details").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                }

                if(!TextUtils.isEmpty(Msg))
                new CustomToast(mcontext, Msg);
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
                ImoreVehicleinfo_Interface.MoreVehicleInfo(vehicleTypeModel);
            }
        }

    }

}

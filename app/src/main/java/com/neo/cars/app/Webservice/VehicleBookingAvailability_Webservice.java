package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.BookingHours_Interface;
import com.neo.cars.app.Interface.VehicleBookingAvailability_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.PickUpLocationModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleGalleryModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLStreamHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 9/5/18.
 */

public class VehicleBookingAvailability_Webservice {

    Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private VehicleTypeModel vehicleTypeModel;
    JSONObject details;
    ArrayList<VehicleTypeModel> arrListVehicleTypeModel;
    PickUpLocationModel pickUpLocationModel;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;


    public void vehicleBookingAvailability(Activity context, final String strVehicleId) {

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);

        gson = new Gson();
        vehicleTypeModel = new VehicleTypeModel();
        UserLoginDetails = new UserLoginDetailsModel();

        showProgressDialog();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        StringRequest vehicleBookingavailabilityRequest = new StringRequest(Request.Method.POST, Urlstring.vehicle_booking_availability,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d(":: Response details:: ", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("vehicle_id", strVehicleId);
                params.put("user_id", UserLoginDetails.getId());

                new PrintClass("params******getParams***" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        vehicleBookingavailabilityRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(vehicleBookingavailabilityRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext, mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response){
        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("vehicle_booking_availibility").optString("user_deleted");
            Msg = jobj_main.optJSONObject("vehicle_booking_availibility").optString("message");
            Status= jobj_main.optJSONObject("vehicle_booking_availibility").optString("status");

            details=jobj_main.optJSONObject("vehicle_booking_availibility").optJSONObject("details");
            arrListVehicleTypeModel = new ArrayList<VehicleTypeModel>();

            if(Status.equals(StaticClass.SuccessResult)){
                vehicleTypeModel.setMax_luggage(details.optString("max_luggage"));
                vehicleTypeModel.setMax_addl_passenger(details.optString("max_addl_passenger"));
                vehicleTypeModel.setDriver_id(details.optString("driver_id"));

                arrListVehicleTypeModel.add(vehicleTypeModel);

                JSONArray jsonArrayPickUpLocation = details.optJSONArray("pickup_location");
                if(jsonArrayPickUpLocation != null){
                    ArrayList<PickUpLocationModel> arrlistPickUpLocation = new ArrayList<PickUpLocationModel>();
                    for (int i=0; i<jsonArrayPickUpLocation.length(); i++){
                        JSONObject jsonObjectPickUpLocation = jsonArrayPickUpLocation.getJSONObject(i);
                        pickUpLocationModel = new PickUpLocationModel();
                        pickUpLocationModel.setId(jsonObjectPickUpLocation.optString("id"));
                        pickUpLocationModel.setPickup_location(jsonObjectPickUpLocation.optString("location"));

                        arrlistPickUpLocation.add(pickUpLocationModel);
                    }
                    vehicleTypeModel.setArr_PickUpLocationModel(arrlistPickUpLocation);
                }


            }else if (Status.equals(StaticClass.ErrorResult)){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("vehicle_details").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                }
                new CustomToast(mcontext, Msg);
            }



        }catch (Exception e){
            e.printStackTrace();
        }



        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
                //((BookingHours_Interface)mcontext).BookingHoursList(arrlistBookingHours);
                ((VehicleBookingAvailability_Interface)mcontext).vehiclebookingavailabilityinfo(vehicleTypeModel);
            }
        }

    }


}



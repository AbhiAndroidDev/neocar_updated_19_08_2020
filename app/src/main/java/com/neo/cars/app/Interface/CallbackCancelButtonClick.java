package com.neo.cars.app.Interface;

/**
 * Created by parna on 15/5/18.
 */

public interface CallbackCancelButtonClick {
    void onButtonClick(String strButtonText, String strBookingFlag, String booking_Type);
}

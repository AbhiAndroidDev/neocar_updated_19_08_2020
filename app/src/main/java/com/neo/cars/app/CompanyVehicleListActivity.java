package com.neo.cars.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.neo.cars.app.Adapter.CompanyVehicleListAdapter;
import com.neo.cars.app.Interface.OwnerDeactive_Interface;
import com.neo.cars.app.Interface.UserEmailMobileVerifyInterface;
import com.neo.cars.app.Interface.UserVehicleList_Interface;
import com.neo.cars.app.Interface.VehicleListAdapter_Interface;
import com.neo.cars.app.SetGet.MyVehicleModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.ImageUtils;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.Webservice.OwnerDeactiveVehicle_Webservice;
import com.neo.cars.app.Webservice.UserMobileEmailVerifyWebService;
import com.neo.cars.app.Webservice.UserVehicleList_Webservice;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import java.util.ArrayList;
import java.util.List;

public class CompanyVehicleListActivity extends AppCompatActivity implements UserVehicleList_Interface,
        SwipeRefreshLayout.OnRefreshListener,OwnerDeactive_Interface,VehicleListAdapter_Interface, UserEmailMobileVerifyInterface {

//    View mView;
    private Toolbar toolbar;
    private Context context;
    private Activity activity;
    private RecyclerView rcvMyBooking;

    private LinearLayoutManager layoutManagerVertical;
    private CompanyVehicleListAdapter companyVehicleListAdapter;
    private ImageView ib_add_vehicle;
    private List<MyVehicleModel> myVehicleModelList = new ArrayList<>();
    private int transitionflag = StaticClass.transitionflagNext;
    private ConnectionDetector cd;
    private CustomTitilliumTextViewSemiBold tvNoVehicleFound;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, rightTopBarText;
    private String strCanAddVehicle="Y";
    private SwipeRefreshLayout swipeRefreshLayout;
    private int selectionPosition=0;
    private RelativeLayout rlAddLayout, rlBackLayout;
    private SharedPrefUserDetails sharedPref;

    private BottomViewCompany bottomViewCompany = new BottomViewCompany();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_vehicle_list);
        new AnalyticsClass(CompanyVehicleListActivity.this);
        Initialize();
        Listener();

        refreshList();
    }

    private void Initialize(){

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        sharedPref = new SharedPrefUserDetails(this);

        activity = CompanyVehicleListActivity.this;
        context = CompanyVehicleListActivity.this;
        cd = new ConnectionDetector(activity);

        swipeRefreshLayout = findViewById(R.id.refreshLayoutC);
        swipeRefreshLayout.setOnRefreshListener(this);

        rcvMyBooking = findViewById(R.id.rcvMyBooking);
        //rightTopBarText.setVisibility(View.VISIBLE);
        tvNoVehicleFound = findViewById(R.id.tvNoVehicleFound);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvMyVehicleHeader));

        rightTopBarText = findViewById(R.id.rightTopBarText);
        rightTopBarText.setVisibility(View.VISIBLE);

        rlAddLayout = findViewById(R.id.rlAddLayout);
        rlAddLayout.setVisibility(View.VISIBLE);

        rlBackLayout = findViewById(R.id.rlBackLayout);

    }

    private void Listener(){
        rlAddLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(cd.isConnectingToInternet()){
                    new UserMobileEmailVerifyWebService().UserMobileEmailVerification(CompanyVehicleListActivity.this);

                }else{
                    new CustomToast(activity, getResources().getString(R.string.Network_not_availabl));
                }
            }
        });

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

    }

    @Override
    public void onRefresh(){
        swipeRefreshLayout.setRefreshing(true);
        selectionPosition=0;
        refreshList();
    }

    private void  refreshList(){
        //fetch user vehicle list
        if (NetWorkStatus.isNetworkAvailable(activity)) {
            new UserVehicleList_Webservice().userVehicleList(activity, CompanyVehicleListActivity.this);

        } else {
            StaticClass.MyVehicleAddUpdate=true;
            Intent i = new Intent(activity, NetworkNotAvailable.class);
            startActivity(i);
        }
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void OnOwnerDeactive(String VehicleId, int position, String Type) {
        selectionPosition=position;
        if(cd.isConnectingToInternet()){
            if(Type.equals(StaticClass.MyVehicleActiveDeactive)) {
                new OwnerDeactiveVehicle_Webservice().ownerDeactivateVehicle(activity, CompanyVehicleListActivity.this, VehicleId, position);
            }/*else{
                new VehicleSoftDelete_Webservice().VehicleSoftDelete(getActivity(), VehicleListFragment.this, VehicleId, position );
            }*/
        }else{
            Intent i = new Intent(context, NetworkNotAvailable.class);
            context.startActivity(i);
        }
    }

    @Override
    public void UserVehicleListInterface(ArrayList<VehicleTypeModel> arrlistVehicle, String strCanAddVehicle) {
        Log.d("d", "arrlistVehicle.size():::"+arrlistVehicle.size());

//        StaticClass.canAddVehicle = strCanAddVehicle;

        if(arrlistVehicle.size() > 0){
            companyVehicleListAdapter = new CompanyVehicleListAdapter(context,CompanyVehicleListActivity.this, arrlistVehicle);
            layoutManagerVertical = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rcvMyBooking.setLayoutManager(layoutManagerVertical);
            rcvMyBooking.setItemAnimator(new DefaultItemAnimator());
            rcvMyBooking.setHasFixedSize(true);
            rcvMyBooking.setAdapter(companyVehicleListAdapter);
            rcvMyBooking.getLayoutManager().scrollToPosition(selectionPosition);

            rcvMyBooking.setVisibility(View.VISIBLE);
            tvNoVehicleFound.setVisibility(View.GONE);

        }else if (arrlistVehicle.size() == 0 || arrlistVehicle == null){
            rcvMyBooking.setVisibility(View.GONE);
            tvNoVehicleFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void OnOwnerDeactive(int position) {
        selectionPosition=position;
        refreshList();
    }

    @Override
    public void onResume() {
        super.onResume();


        refreshList();
//        StaticClass.BottomMyVehicleCompany = false;

        sharedPref.putBottomViewCompany(StaticClass.Menu_MyVehicles_company);
        bottomViewCompany = new BottomViewCompany();
        bottomViewCompany.BottomViewCompany(CompanyVehicleListActivity.this, StaticClass.Menu_MyVehicles_company);


    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(CompanyVehicleListActivity.this, transitionflag);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomViewCompany.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) CompanyVehicleListActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    @Override
    public void userEmailMobileVerify(String status) {
        if (status.equalsIgnoreCase(StaticClass.SuccessResult)) {
            Intent addvehicledriverintent = new Intent(CompanyVehicleListActivity.this, CompanyAddVehicleActivity.class);
            startActivity(addvehicledriverintent);
        }
    }
}

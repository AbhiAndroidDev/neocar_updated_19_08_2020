package com.neo.cars.app.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.neo.cars.app.R;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

/**
 * Created by parna on 10/10/18.
 */

public class CustomDialogWhite extends Dialog {
    private Context mContext;
    private Activity mActivity;
    private String mStrMsg;
    CustomTextviewTitilliumWebRegular tvInfoMsg;
    CustomButtonTitilliumSemibold btnOk;
    Dialog dialog;

    public CustomDialogWhite(@NonNull Context context, String message) {
        super(context);
        this.mContext = context;
        this.mStrMsg = message;
        setTitle("NeoCar");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog_white);
        tvInfoMsg = (CustomTextviewTitilliumWebRegular) findViewById(R.id.tvInfoMsg);
        btnOk = (CustomButtonTitilliumSemibold) findViewById(R.id.btnOk);
        if (mStrMsg != null && !mStrMsg.equals("")) {
            tvInfoMsg.setText(mStrMsg);
        }
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }
}

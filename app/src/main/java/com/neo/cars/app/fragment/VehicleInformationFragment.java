package com.neo.cars.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.neo.cars.app.ImageFullActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.VehicleInformationModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.squareup.picasso.Picasso;

/**
 * Created by parna on 15/3/18.
 */

public class VehicleInformationFragment extends Fragment {

    private View mView;

    private ViewPager view_pager;
    private RelativeLayout rlFirstRelativeLayout;
    private ScrollView scvCarDetails;
    private int[] layouts;
    private TextView[] dots;
    private LinearLayout dotsLayout;

    private SharedPrefUserDetails prefs;

    private MyViewPagerAdapter myViewPagerAdapter;
    private Gson gson;
    private VehicleInformationModel vehicleInformationModel;
    private String vehicleInformation, strKm="";
    private TextView tvHourlyRate, tvTypeName, tvBrandName,tvVehicleMake,tvYrManufacture,tvTotalKm, tvMaxPassengers, tvMaxLuggage,
            tvLicensePlateNo, tvHourlyRateHeader;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (mView != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null)
                parent.removeView(mView);
        }
        try {
            mView = inflater.inflate(R.layout.fragment_vehicle_information, container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (InflateException e) {
            e.printStackTrace();
        }

        if(getArguments() != null){
            vehicleInformation = getArguments().getString("vehicleInformation");
            Log.d("d", "Get strvehicleId: "+vehicleInformation);
        }

        gson = new Gson();
        vehicleInformationModel=new VehicleInformationModel();
        vehicleInformationModel = gson.fromJson(vehicleInformation, VehicleInformationModel.class);

        Initialize();
        SetData();


        // adding bottom dots
        addBottomDots(0);

        myViewPagerAdapter = new MyViewPagerAdapter();
        view_pager.setAdapter(myViewPagerAdapter);
        view_pager.addOnPageChangeListener(viewPagerPageChangeListener);

        return mView;

    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[vehicleInformationModel.getImage_gallery().size()];

        try {
            int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
            int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

            dotsLayout.removeAllViews();
            for (int i = 0; i < dots.length; i++) {
                dots[i] = new TextView(getActivity());
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(35);
                dots[i].setTextColor(colorsInactive[currentPage]);
                dotsLayout.addView(dots[i]);
            }

            if (dots.length > 0)
                dots[currentPage].setTextColor(colorsActive[currentPage]);

        }catch (Exception e){
            e.printStackTrace();
        }
    }



    public void Initialize(){

        prefs = new SharedPrefUserDetails(getActivity());

        view_pager = mView.findViewById(R.id.view_pager);
        scvCarDetails = mView.findViewById(R.id.scvCarDetails);
        rlFirstRelativeLayout = mView.findViewById(R.id.rlFirstRelativeLayout);
        dotsLayout = mView.findViewById(R.id.layoutDots);

        tvTypeName= mView.findViewById(R.id.tvTypeName);
        tvHourlyRate= mView.findViewById(R.id.tvHourlyRate);
        tvHourlyRateHeader= mView.findViewById(R.id.tvHourlyRateHeader);
        tvLicensePlateNo= mView.findViewById(R.id.tvLicensePlateNo);

        tvBrandName= mView.findViewById(R.id.tvBrandName);
        tvVehicleMake= mView.findViewById(R.id.tvVehicleMake);
        tvYrManufacture= mView.findViewById(R.id.tvYrManufacture);
        tvTotalKm= mView.findViewById(R.id.tvTotalKm);

        tvMaxPassengers = mView.findViewById(R.id.tvMaxPassengers);
        tvMaxLuggage = mView.findViewById(R.id.tvMaxLuggage);
    }

    public void SetData(){

        Log.d("vehicleIBrandName",vehicleInformationModel.getVehicle_make());
        Log.d("vehicleModelName",vehicleInformationModel.getVehicle_model());

        tvLicensePlateNo.setText(vehicleInformationModel.getLicense_plate_no());
        tvHourlyRate.setText(getResources().getString(R.string.Rs)+" "+vehicleInformationModel.getHourly_rate());
        tvTypeName.setText(vehicleInformationModel.getVehicle_type());
        tvBrandName.setText(vehicleInformationModel.getVehicle_model());
        tvVehicleMake.setText(vehicleInformationModel.getVehicle_make());
        tvYrManufacture.setText(vehicleInformationModel.getVehicle_year());

        tvTotalKm.setText(vehicleInformationModel.getVehicle_km_travelled());

        if(!TextUtils.isEmpty(vehicleInformationModel.getMax_passenger()) ){
            tvMaxPassengers.setText(vehicleInformationModel.getMax_passenger());
        }

        if(!TextUtils.isEmpty(vehicleInformationModel.getMax_luggage()) ){
            tvMaxLuggage.setText(vehicleInformationModel.getMax_luggage());
        }
    }

    //	viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {


        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };


    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            //View view = layoutInflater.inflate(layouts[position], container, false);
            View view = layoutInflater.inflate(R.layout.view_pager_vehicle_gallery, container, false);
            final ImageView ivVehicleGallery = (ImageView)view.findViewById(R.id.ivVehicleGallery);
//            ivVehicleGallery.setScaleType(ImageView.ScaleType.CENTER_CROP);


//            Picasso.get().load(vehicleInformationModel.getImage_gallery().get(position).getImage_file()).into(ivVehicleGallery);

            Glide.with(getActivity())
                    .load(vehicleInformationModel.getImage_gallery().get(position).getImage_file())
                    .fitCenter()
                    .into(ivVehicleGallery);


            ivVehicleGallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), ImageFullActivity.class);
                    i.putExtra("imgUrl",vehicleInformationModel.getImage_gallery().get(position).getImage_file());
                    startActivity(i);
                }
            });

            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return vehicleInformationModel.getImage_gallery().size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}

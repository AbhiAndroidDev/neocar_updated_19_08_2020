package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.VehicleBrandModel;

import java.util.ArrayList;

/**
 * Created by parna on 7/5/18.
 */

public interface FilterVehicleBrand_Interface {

    void VehicleBrandList(ArrayList<VehicleBrandModel> arrlistVehicleBrand);

}

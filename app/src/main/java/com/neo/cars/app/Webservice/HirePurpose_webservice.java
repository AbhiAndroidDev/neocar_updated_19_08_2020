package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.HirePurpose_Interface;
import com.neo.cars.app.Interface.StateListInterface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.HirePurposeModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 10/5/18.
 */

public class HirePurpose_webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private HirePurposeModel hirepurposeListModel;
    private ArrayList<HirePurposeModel> arrlistHirePurpose;
    private JSONObject jsonObjectHirePurpose;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;

    public void hirePurposeListWebservice (Activity context){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        sharedPref = new SharedPrefUserDetails(mcontext);
        arrlistHirePurpose = new ArrayList<>();
        gson = new Gson();
        hirepurposeListModel = new HirePurposeModel();
        UserLoginDetails=new UserLoginDetailsModel();

        showProgressDialog();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        StringRequest hirepurposestringrequest = new StringRequest(Request.Method.POST, Urlstring.hire_purpose,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", UserLoginDetails.getId());

                new PrintClass("hire purpose params******getParams***"+params);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };
        hirepurposestringrequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(hirepurposestringrequest);
    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response){
        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("booking_purpose").optString("user_deleted");
            Msg = jobj_main.optJSONObject("booking_purpose").optString("message");
            Status = jobj_main.optJSONObject("booking_purpose").optString("status");

            JSONArray jsonArrayDetails = jobj_main.optJSONObject("booking_purpose").optJSONArray("details");
            for (int i=0; i<jsonArrayDetails.length(); i++){
                jsonObjectHirePurpose = jsonArrayDetails.optJSONObject(i);

                HirePurposeModel hirePurposeModel = new HirePurposeModel();
                hirePurposeModel.setId(jsonObjectHirePurpose.optString("id"));
                hirePurposeModel.setPurpose(jsonObjectHirePurpose.optString("purpose"));

                arrlistHirePurpose.add(hirePurposeModel);
            }
            Log.d("Hirepurpose size:", "" + arrlistHirePurpose.size());
            Log.e("Hirepurpose", "" + arrlistHirePurpose);

        }catch (Exception e){
            e.printStackTrace();
        }



        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
                ((HirePurpose_Interface)mcontext).HirePurposeList(arrlistHirePurpose);
            }
        }

    }


}

package com.neo.cars.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.neo.cars.app.Adapter.VehicleListAdapter;
import com.neo.cars.app.Interface.OwnerDeactive_Interface;
import com.neo.cars.app.Interface.UserVehicleList_Interface;
import com.neo.cars.app.Interface.VehicleListAdapter_Interface;
import com.neo.cars.app.NetworkNotAvailable;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.MyVehicleModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.OwnerDeactiveVehicle_Webservice;
import com.neo.cars.app.Webservice.UserVehicleList_Webservice;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by parna on 12/3/18.
 */

public class VehicleListFragment extends Fragment implements UserVehicleList_Interface,
        SwipeRefreshLayout.OnRefreshListener,OwnerDeactive_Interface,VehicleListAdapter_Interface {

    private View mView;
    private Context context;
    private RecyclerView rcvMyBooking;

    private LinearLayoutManager layoutManagerVertical;
    private VehicleListAdapter vehicleListAdapter;
    private ImageView ib_add_vehicle;
    private List<MyVehicleModel> myVehicleModelList = new ArrayList<>();
    private int transitionflag = StaticClass.transitionflagNext;
    private ConnectionDetector cd;
    private CustomTitilliumTextViewSemiBold tvNoVehicleFound;
    private String strCanAddVehicle="Y";
    private SwipeRefreshLayout swipeRefreshLayout;
    private int selectionPosition=0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (mView != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null)
                parent.removeView(mView);
        }
        try {
            mView = inflater.inflate(R.layout.fragment_vehicle_list, container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (InflateException e) {
            e.printStackTrace();
        }

        Initialize();
        Listener();

        refreshList();

        return mView;
    }

    private void Initialize(){

        context = getActivity();
        cd = new ConnectionDetector(getActivity());

        swipeRefreshLayout = mView.findViewById(R.id.refresh);
        swipeRefreshLayout.setOnRefreshListener(this);

        rcvMyBooking = mView.findViewById(R.id.rcvMyBooking);
        //rightTopBarText.setVisibility(View.VISIBLE);
        tvNoVehicleFound = mView.findViewById(R.id.tvNoVehicleFound);
    }

    private void Listener(){

    }

    @Override
    public void onRefresh(){
        swipeRefreshLayout.setRefreshing(true);
        selectionPosition=0;
        refreshList();
    }

   private void  refreshList(){
       //fetch user vehicle list
       if (NetWorkStatus.isNetworkAvailable(getActivity())) {
           new UserVehicleList_Webservice().userVehicleList(getActivity(), VehicleListFragment.this);

       } else {
           StaticClass.MyVehicleAddUpdate=true;
           Intent i = new Intent(getActivity(), NetworkNotAvailable.class);
           startActivity(i);
       }
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void UserVehicleListInterface(ArrayList<VehicleTypeModel> arrlistVehicle, String str_CanAddVehicle) {

        Log.d("d", "arrlistVehicle.size():::"+arrlistVehicle.size());

        StaticClass.canAddVehicle = str_CanAddVehicle;

        if(arrlistVehicle.size() > 0){
            vehicleListAdapter = new VehicleListAdapter(context,VehicleListFragment.this,arrlistVehicle);
            layoutManagerVertical = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rcvMyBooking.setLayoutManager(layoutManagerVertical);
            rcvMyBooking.setItemAnimator(new DefaultItemAnimator());
            rcvMyBooking.setHasFixedSize(true);
            rcvMyBooking.setAdapter(vehicleListAdapter);
            rcvMyBooking.getLayoutManager().scrollToPosition(selectionPosition);

            rcvMyBooking.setVisibility(View.GONE); //as per client need vehicle list is hided and coming soon text will be show
            tvNoVehicleFound.setVisibility(View.VISIBLE);

        }else{
            rcvMyBooking.setVisibility(View.GONE);
            tvNoVehicleFound.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        StaticClass.BottomProfile = false;
        refreshList();
    }

    @Override
    public void OnOwnerDeactive(int position) {
        selectionPosition=position;
        refreshList();
    }

    @Override
    public void OnOwnerDeactive(String VehicleId, int position,String Type) {
        selectionPosition=position;
        if(cd.isConnectingToInternet()){
            if(Type.equals(StaticClass.MyVehicleActiveDeactive)) {
                new OwnerDeactiveVehicle_Webservice().ownerDeactivateVehicle(getActivity(), VehicleListFragment.this, VehicleId, position);
            }/*else{
                new VehicleSoftDelete_Webservice().VehicleSoftDelete(getActivity(), VehicleListFragment.this, VehicleId, position );
            }*/
        }else{
            Intent i = new Intent(context, NetworkNotAvailable.class);
            context.startActivity(i);
        }
    }

}
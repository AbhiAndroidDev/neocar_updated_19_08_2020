package com.neo.cars.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.neo.cars.app.BookACarActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.OvertimeAvailabilityVehicleModel;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.font.CustomTextviewTitilliumBold;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;

/**
 * Created by parna on 28/9/18.
 */

public class ExploreOvertimeGridCarAdapter extends RecyclerView.Adapter<ExploreOvertimeGridCarAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<OvertimeAvailabilityVehicleModel> listOfVehical;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivCarOne;
        CustomTextviewTitilliumBold tvCarDescription;
        RelativeLayout rlCarLayout;

        public MyViewHolder(View view) {
            super(view);
            ivCarOne = view.findViewById(R.id.ivCarOne);
            tvCarDescription = view.findViewById(R.id.tvCarDescription);
            rlCarLayout= view.findViewById(R.id.rlCarLayout);

        }
    }

    public ExploreOvertimeGridCarAdapter(Context mContext, ArrayList<OvertimeAvailabilityVehicleModel> listOfVehical) {
        this.mContext = mContext;
        this.listOfVehical = listOfVehical;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_explore_car_image, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Transformation transformation1 = new Transformation() {

            @Override
            public Bitmap transform(Bitmap source) {
                Bitmap result=null;
                try {
                    int targetWidth = 0;
                    int targetHeight = 0;
                    try {
                        targetWidth = holder.ivCarOne.getWidth();
                        if(targetWidth<1){
                            targetWidth=100;
                        }
                        double aspectRatio = (double) source.getHeight()/(double) source.getWidth() ;
                        targetHeight = (int) (targetWidth * aspectRatio);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if(targetHeight<1){
                        targetHeight=100;
                    }

                    result= Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                    if (result != source) {
                        source.recycle();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            public String key() {
                return "transformation" + " desiredWidth";
            }
        };


        if(!"".equals(listOfVehical.get(position).getVehicle_image()) && !"null".equals(listOfVehical.get(position).getVehicle_image())){
            Picasso.get().load(listOfVehical.get(position).getVehicle_image()).transform(transformation1).into(holder.ivCarOne);
        }

        holder.rlCarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //move to car details page for book now option
                Intent bookacarintent = new Intent(mContext, BookACarActivity.class);
                bookacarintent.putExtra("VehicleId", listOfVehical.get(position).getId());
                bookacarintent.putExtra("IsComingFrom", StaticClass.ExploreCar);
                Log.d("d", "Vehicle id:: " + listOfVehical.get(position).getId());
                mContext.startActivity(bookacarintent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listOfVehical.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

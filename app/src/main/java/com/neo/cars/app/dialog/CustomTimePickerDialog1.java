package com.neo.cars.app.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.neo.cars.app.Interface.CallBackCustomTimePicker;
import com.neo.cars.app.R;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by parna on 17/8/18.
 */
public class CustomTimePickerDialog1 extends Dialog{

    private final static int TIME_PICKER_INTERVAL = 15;
    private TimePicker mTimePicker;
    Context mcontext;
    private int hour;
    private int minute;
    private boolean is24HourView;
    static final int TIME_DIALOG_ID = 999;
    private Button btn_ok_time, btn_Cancel_time;

    public CustomTimePickerDialog1(@NonNull Context context, int hourOfDay, int minute, boolean is24HourView) {
        super(context);
        this.mcontext = context;
        this.hour = hourOfDay;
        this.minute = minute;
        this.is24HourView = is24HourView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.time_picker_layout);

        setCurrentTimeOnView();
        addListenerOnButton();
    }

//     display current time
    public void setCurrentTimeOnView() {

        mTimePicker = findViewById(R.id.timePicker1);

        final Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);

        mTimePicker.setIs24HourView(is24HourView);
//        mTimePicker.setCurrentHour(hour);
        mTimePicker.setCurrentMinute(minute);
        mTimePicker.setCurrentHour(hour);

        setTimePickerInterval(mTimePicker);

    }

    @SuppressLint("NewApi")
    private void setTimePickerInterval(TimePicker timePicker) {
        try {
            Class<?> classForid = Class.forName("com.android.internal.R$id");
            // Field timePickerField = classForid.getField("timePicker");

            Field field = classForid.getField("minute");
            NumberPicker minutePicker = timePicker
                    .findViewById(field.getInt(null));

            minutePicker.setMinValue(0);
//            minutePicker.setMaxValue(3);
            minutePicker.setMaxValue((60 / TIME_PICKER_INTERVAL) - 1);
            ArrayList<String> displayedValues = new ArrayList<String>();
            for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                displayedValues.add(String.format("%02d", i));
            }
            minutePicker.setDisplayedValues(displayedValues
                    .toArray(new String[0]));
            minutePicker.setWrapSelectorWheel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addListenerOnButton() {

        btn_ok_time = findViewById(R.id.btn_ok_time);
        btn_Cancel_time = findViewById(R.id.btn_Cancel_time);

        btn_ok_time.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                int currentApiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentApiVersion > android.os.Build.VERSION_CODES.LOLLIPOP_MR1){
                    hour = mTimePicker.getHour();
                    minute = mTimePicker.getMinute();
                } else {
                    hour = mTimePicker.getCurrentHour();
                    minute = mTimePicker.getCurrentMinute();
                }

                ((CallBackCustomTimePicker)mcontext).onButtonClick(mTimePicker, hour,
                            minute);
            }
        });

        btn_Cancel_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }



//    private static String pad(int c) {
//        if (c >= 10)
//            return String.valueOf(c);
//        else
//            return "0" + String.valueOf(c);
//    }

}

package com.neo.cars.app.SetGet;

/**
 * Created by joydeep on 27/3/18.
 */

public class WishlistModel {

    String wishlist_id;
    String vehicle_id;
    String brand;
    String make;


    public String getWishlist_id() {
        return wishlist_id;
    }

    public void setWishlist_id(String wishlist_id) {
        this.wishlist_id = wishlist_id;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getTotal_no_km() {
        return total_no_km;
    }

    public void setTotal_no_km(String total_no_km) {
        this.total_no_km = total_no_km;
    }

    public String getMax_passenger() {
        return max_passenger;
    }

    public void setMax_passenger(String max_passenger) {
        this.max_passenger = max_passenger;
    }

    public String getMax_luggage() {
        return max_luggage;
    }

    public void setMax_luggage(String max_luggage) {
        this.max_luggage = max_luggage;
    }

    public String getHourly_rate() {
        return hourly_rate;
    }

    public void setHourly_rate(String hourly_rate) {
        this.hourly_rate = hourly_rate;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    String total_no_km;
    String max_passenger;
    String max_luggage;
    String hourly_rate;
    String image_url;




}

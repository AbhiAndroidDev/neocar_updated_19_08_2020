package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.CompanyBookingListModel;

import java.util.ArrayList;

public interface CompanyBookingList_Interface {

    public void companyBookingList(ArrayList<CompanyBookingListModel> bookingList);
}

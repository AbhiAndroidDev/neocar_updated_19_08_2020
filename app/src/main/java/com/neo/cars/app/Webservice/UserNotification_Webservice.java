package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.InboxModalInterface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.InboxModal;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by joydeep on 23/3/18.
 */

public class UserNotification_Webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "",count_unread_message="0", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private ArrayList<InboxModal> arr_InboxModal=new ArrayList<>();
    private int transitionflag = StaticClass.transitionflagNext;

    public void UserNotification(Activity context) {

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        sharedPref = new SharedPrefUserDetails(mcontext);
        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();
        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);
        showProgressDialog();

        StringRequest loginRequest = new StringRequest(Request.Method.POST, Urlstring.user_notification,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", sharedPref.getUserid());
                params.put("user_type", "U"); // Always send "U" as user type

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        loginRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(loginRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try {
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("user_notification").optString("user_deleted");
            Msg = jobj_main.optJSONObject("user_notification").optString("message");
            Status= jobj_main.optJSONObject("user_notification").optString("status");
            count_unread_message= jobj_main.optJSONObject("user_notification").optString("count_unread_message");

            JSONArray details=jobj_main.optJSONObject("user_notification").optJSONArray("details");

            if (Status.equals(StaticClass.SuccessResult)) {
                arr_InboxModal=new ArrayList<>();
                for(int i=0;i<details.length();i++){

                    InboxModal ibm=new InboxModal();

                    ibm.setId(details.getJSONObject(i).optString("id"));
                    ibm.setTitle(details.getJSONObject(i).optString("title"));
                    ibm.setDescription(details.getJSONObject(i).optString("description"));
                    ibm.setIs_read(details.getJSONObject(i).optString("user_read"));
                    ibm.setNotification_type(details.getJSONObject(i).optString("notification_type"));
                    ibm.setCreated_at(details.getJSONObject(i).optString("created_at"));

                    arr_InboxModal.add(ibm);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        }

        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {

            if (Status.equals(StaticClass.SuccessResult)) {
                sharedPref.setNewNotificationStatus(false);

                ((InboxModalInterface)mcontext).OnInboxModal(arr_InboxModal);

            }
        }
    }
}

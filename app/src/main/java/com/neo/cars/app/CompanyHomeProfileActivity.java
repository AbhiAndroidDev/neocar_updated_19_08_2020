package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.neo.cars.app.Interface.Profile_Interface;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.ImageUtils;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.Webservice.CompanyProfile_WebService;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.squareup.picasso.Picasso;

public class CompanyHomeProfileActivity extends AppCompatActivity implements Profile_Interface {

    private CircularImageViewBorder civProfilePic;
    private TextView tvEnlistMyBooking, tvMyVehicle, tvSettings, tvHelp, tvInviteFriends, tvManageCoupons ;
    private TextView tvAlertText, tvCompanyNameHome, tvPhoneHeader, tvMobile, tvGSTNo, tvGSTNoHeader;
    private RelativeLayout rlALert;
    private ImageView ibEdit;
    private int transitionflag = StaticClass.transitionflagNext;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private ImageButton ibNavigateMenu;
    private ImageView iVsmallLogo;
    private SharedPrefUserDetails sharedPref;
    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private RelativeLayout rlBackLayout;
    private RelativeLayout rlUserDetails, rlInfoRelativeLayout;
    private BottomViewCompany bottomViewCompany = new BottomViewCompany();
    private ConnectionDetector cd;
    private Context context;
    private int resultValue = 11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_home_profile);

        new AnalyticsClass(CompanyHomeProfileActivity.this);

        Initialize();
        Listener();
    }

    private void Initialize() {
        context = this;
        sharedPref = new SharedPrefUserDetails(CompanyHomeProfileActivity.this);
        cd = new ConnectionDetector(this);
        UserLoginDetails=new UserLoginDetailsModel();

        rlALert= findViewById(R.id.rlALert);
        tvAlertText= findViewById(R.id.tvAlertText);
        tvPhoneHeader= findViewById(R.id.tvPhoneHeader);
        tvCompanyNameHome = findViewById(R.id.tvCompanyNameHome);
        tvMobile= findViewById(R.id.tvMobile);
        tvGSTNo= findViewById(R.id.tvGSTNo);
        tvGSTNoHeader= findViewById(R.id.tvGSTNoHeader);
        civProfilePic = findViewById(R.id.civProfilePic);

        ibEdit = findViewById(R.id.ibEdit);
        tvEnlistMyBooking = findViewById(R.id.tvEnlistMyBooking);
        tvMyVehicle =  findViewById(R.id.tvMyVehicle);
        tvSettings = findViewById(R.id.tvSettings);
        tvHelp = findViewById(R.id.tvHelp);
        tvInviteFriends = findViewById(R.id.tvInviteFriends);
        tvManageCoupons = findViewById(R.id.tvManageCoupons);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.app_name));

        rlBackLayout = findViewById(R.id.rlBackLayout);
        rlBackLayout.setVisibility(View.INVISIBLE);
        iVsmallLogo = findViewById(R.id.iVsmallLogo);
        iVsmallLogo.setVisibility(View.VISIBLE);

        rlUserDetails = findViewById(R.id.rlUserDetails);
        rlInfoRelativeLayout = findViewById(R.id.rlInfoRelativeLayout);

//        tvInviteFriends.setVisibility(View.GONE);
        tvManageCoupons.setVisibility(View.GONE);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//
//            if( getApplicationContext().checkSelfPermission(android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED ) {
//                ActivityCompat.requestPermissions(CompanyHomeProfileActivity.this, new String[]{android.Manifest.permission.READ_CONTACTS}, resultValue);
//            }
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomViewCompany.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void Listener() {

        rlInfoRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag=StaticClass.transitionflagNext;
                    startActivity(new Intent(CompanyHomeProfileActivity.this, ViewCompanyprofile.class));

            }
        });

        tvEnlistMyBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    transitionflag = StaticClass.transitionflagNext;
                    Intent mybookingIntent = new Intent(CompanyHomeProfileActivity.this, EnlistActivity.class);
                    startActivity(mybookingIntent);

            }
        });

        tvMyVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag=StaticClass.transitionflagNext;
                Intent myvehicleIntent = new Intent(CompanyHomeProfileActivity.this, MyVehicleActivity.class);
                startActivity(myvehicleIntent);
            }
        });

        tvSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag=StaticClass.transitionflagNext;
                Intent settingsIntent = new Intent(CompanyHomeProfileActivity.this, CompanySettingsActivity.class);
                startActivity(settingsIntent);

            }
        });

        tvHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag=StaticClass.transitionflagNext;
                Intent helpIntent = new Intent(CompanyHomeProfileActivity.this, CompanyHelpActivity.class);
                startActivity(helpIntent);

            }
        });

        tvInviteFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag=StaticClass.transitionflagNext;
                Intent invitefriendsIntent = new Intent(CompanyHomeProfileActivity.this, CompanyInviteFriendsActivity.class);
                startActivity(invitefriendsIntent);
            }
        });

        tvManageCoupons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                transitionflag=StaticClass.transitionflagNext;
//                Intent manageCouponsIntent = new Intent(CompanyHomeProfileActivity.this, ManageCouponsActivity.class);
//                startActivity(manageCouponsIntent);
            }
        });

        civProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(CompanyHomeProfileActivity.this, ImageFullActivity.class);
                i.putExtra("imgUrl", UserLoginDetails.getProfile_pic());
                startActivity(i);

            }
        });

    }


    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(CompanyHomeProfileActivity.this, transitionflag);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        transitionflag = StaticClass.transitionflagBack;
    }


    @Override
    protected void onResume() {
        super.onResume();

        StaticClass.BottomProfileCompany = false;

        //Fetch my profile details
        if (cd.isConnectingToInternet()) {
                    new CompanyProfile_WebService().companyProfile(CompanyHomeProfileActivity.this);

        } else {
            startActivity(new Intent(this, NetworkNotAvailable.class));
            transitionflag = StaticClass.transitionflagBack;

        }

        if (StaticClass.isLoginFalg) {

            sharedPref.setClear();
            StaticClass.isLoginFalg=false;
            transitionflag = StaticClass.transitionflagBack;
            Intent i = new Intent(CompanyHomeProfileActivity.this, LoginStepOneActivity.class);
            startActivity(i);
            finish();

        } else {

            gson = new Gson();


            //sharedPref = new SharedPrefUserDetails(HomeProfileActivity.this);

            String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
            UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        }


        if(StaticClass.EditProfileIsSaveCompany){

            StaticClass.EditProfileIsSaveCompany = false;

//            sharedPref.putBottomView(StaticClass.Menu_Explore);

//            Intent invitefriendsIntent = new Intent(CompanyHomeProfileActivity.this, ExploreActivity.class);
//            startActivity(invitefriendsIntent);


        }else{

            sharedPref.putBottomViewCompany(StaticClass.Menu_profile_company);
            bottomViewCompany = new BottomViewCompany();
            bottomViewCompany.BottomViewCompany(CompanyHomeProfileActivity.this, StaticClass.Menu_profile_company);
        }

        ImageUtils.deleteImageGallery();

    }

    @Override
    public void MyProfile(UserLoginDetailsModel userLoginDetailsModel) {

        this.UserLoginDetails = userLoginDetailsModel;
        if (!userLoginDetailsModel.getIs_profile_complete().toString().equals("") && !userLoginDetailsModel.getIs_profile_complete().toString().equals("null")) {
            if(userLoginDetailsModel.getIs_profile_complete().toString().equals("Y")){
                rlALert.setVisibility(View.GONE);
            }else{
                rlALert.setVisibility(View.VISIBLE);
                tvAlertText.setText(userLoginDetailsModel.getProfile_incomplete_message());
            }
        }

            if (UserLoginDetails != null) {
                tvCompanyNameHome.setText(UserLoginDetails.getCompany_name());
                tvMobile.setText(UserLoginDetails.getCompany_mobile_number());
                tvGSTNo.setText(UserLoginDetails.getCompany_gst_number());

                new PrintClass(UserLoginDetails.getProfile_pic());

                try {
                    Picasso.get().load(UserLoginDetails.getProfile_pic()).into(civProfilePic);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) CompanyHomeProfileActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}

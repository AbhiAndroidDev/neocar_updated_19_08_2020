package com.neo.cars.app.Adapter;

import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.neo.cars.app.R;
import java.util.ArrayList;

public class SortOptionAdapter extends RecyclerView.Adapter<SortOptionAdapter.MyViewHolder> {

    private ArrayList<String> sortOptionList;
    private BottomSheetBehavior mBottomSheetBehavior;

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView ctv_sort_option;

        public MyViewHolder(View view) {
            super(view);
            ctv_sort_option = view.findViewById(R.id.ctv_sort_option);
        }
    }

    public SortOptionAdapter (BottomSheetBehavior _mBottomSheetBehavior, ArrayList<String> _sortOptionList){
        this.mBottomSheetBehavior = _mBottomSheetBehavior;
        this.sortOptionList = _sortOptionList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.bottom_sheet_sort, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final  int position) {

        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        holder.ctv_sort_option.setText(sortOptionList.get(position));
    }

    @Override
    public int getItemCount() {
        return sortOptionList.size();
    }
}

package com.neo.cars.app.View;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.neo.cars.app.NetworkNotAvailable;
import com.neo.cars.app.R;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.Emailvalidation;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.GetLoginOtp_Webservice;

/**
 * Created by joydeep on 2/3/18.
 */

public class Login_with_mobile_number_View {

    private View v;
    private Activity mcontext;
    private Context ab;
    private EditText etmobilenumber;
    private Button btnGenerateOtp;
    private ConnectionDetector cd;

    public View Login_with_mobile_number_View(Activity context){
        mcontext = context;
        ab = context;
        v = LayoutInflater.from(mcontext).inflate(R.layout.login_with_mobile_number, null);
        Initialize();
        Listener();
        return v;
    }

    private void Initialize() {
        cd = new ConnectionDetector(ab);
        etmobilenumber = v.findViewById(R.id.etmobilenumber);
        btnGenerateOtp = v.findViewById(R.id.btnGenerateOtp);

        if (StaticClass.Is_MobileNo &&!TextUtils.isEmpty(StaticClass.mobileNo)){
            etmobilenumber.setText(StaticClass.mobileNo);
        }
    }

    private void Listener(){

        btnGenerateOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean cancel = false;
                View focusView = null;
                String smobile=etmobilenumber.getText().toString().trim();

                if (TextUtils.isEmpty(smobile)) {
                    etmobilenumber.setError(mcontext.getString(R.string.error_field_required));
                    etmobilenumber.requestFocus();
                  //  focusView = etmobilenumber;
                   // cancel = true;

                }else if (!(new Emailvalidation().phone_validation(smobile))) {
                    etmobilenumber.setError(mcontext.getString(R.string.error_telphone_format));
                    etmobilenumber.requestFocus();
                /*focusView = etRegMob;
                cancel = true;*/

                } else{

                    if(cd.isConnectingToInternet()){
                        new GetLoginOtp_Webservice().GetLoginOtp(mcontext,smobile);

                    }else{
                        mcontext.startActivity(new Intent(mcontext, NetworkNotAvailable.class));
                    }
                }

                if (!TextUtils.isEmpty(smobile)){
                    StaticClass.Is_MobileNo = true;
                    StaticClass.mobileNo = smobile;
                }
            }

        });
    }
}

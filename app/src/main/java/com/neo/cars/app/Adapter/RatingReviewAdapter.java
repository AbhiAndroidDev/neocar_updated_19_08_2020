package com.neo.cars.app.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.RatingReviewAllListModel;
import com.neo.cars.app.SetGet.RatingReviewListModel;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by parna on 12/9/18.
 */

public class RatingReviewAdapter extends RecyclerView.Adapter<RatingReviewAdapter.MyViewHolder> {

    private List<RatingReviewAllListModel> myRatingReviewList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
       CustomTextviewTitilliumWebRegular tvRatingReview, tvRatingGiverName, tvRatingDate;
       RatingBar ratingBar;


        public MyViewHolder(View view) {
            super(view);

            tvRatingReview = view.findViewById(R.id.tvRatingReview);
            tvRatingGiverName = view.findViewById(R.id.tvRatingGiverName);
            tvRatingDate = view.findViewById(R.id.tvRatingDate);
            ratingBar = view.findViewById(R.id.ratingReview);
            ratingBar.setIsIndicator(true);
        }
    }

    public RatingReviewAdapter(Context context, List<RatingReviewAllListModel> myRatingReviewList) {
        this.myRatingReviewList = myRatingReviewList;
        this.context = context;
    }

    @Override
    public RatingReviewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rating_review_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RatingReviewAdapter.MyViewHolder holder, int position) {

        holder.tvRatingReview.setText(myRatingReviewList.get(position).getComment());
        holder.tvRatingGiverName.setText("By "+myRatingReviewList.get(position).getRiview_by_name()+",");
        holder.tvRatingDate.setText(myRatingReviewList.get(position).getUpdated_at());
        holder.ratingBar.setRating(Float.parseFloat(myRatingReviewList.get(position).getRating()));

    }

    @Override
    public int getItemCount() {
        return myRatingReviewList.size();
    }
}

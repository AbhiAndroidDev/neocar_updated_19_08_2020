package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.CityListModel;
import java.util.ArrayList;

/**
 * Created by parna on 23/3/18.
 */

public interface CityList_Interface {

    void CityList(ArrayList<CityListModel> arrlistCity);
}

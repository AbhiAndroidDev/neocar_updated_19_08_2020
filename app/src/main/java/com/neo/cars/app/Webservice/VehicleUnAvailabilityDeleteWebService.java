package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.VehicleUnAvailability_Interface;
import com.neo.cars.app.Interface.VehicleUnAvailableDelete_interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleUnAvailabilityDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VehicleUnAvailabilityDeleteWebService {

    Context mcontext;
    Activity activity;
    private String Status = "0", Msg = "", strUserDeleted="", yearMonth = "";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    JSONObject details;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;
    private VehicleUnAvailabilityDetailsModel vehicleUnAvailabilityModel;
    ArrayList<VehicleUnAvailabilityDetailsModel> arrListVehicleUnAvailabilityModel;
    VehicleUnAvailability_Interface vehicleUnAvailability_interface;

    public void vehicleUnAvailableDelete(Context context, final Activity activity, final String unavailable_id){

        this.mcontext = context;
        this.activity = activity;
        sharedPref = new SharedPrefUserDetails(mcontext);
        showProgressDialog();

        StringRequest citylistRequest = new StringRequest(Request.Method.POST, Urlstring.vehicle_unavailable_delete,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(activity, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", sharedPref.getUserid());
                params.put("unavailable_id", unavailable_id);

                new PrintClass("city params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        citylistRequest.setRetryPolicy((new DefaultRetryPolicy(30000, 1, 1.0f)));
        Volley.newRequestQueue(context).add(citylistRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try{
            arrListVehicleUnAvailabilityModel = new ArrayList<>();

            vehicleUnAvailabilityModel = new VehicleUnAvailabilityDetailsModel();

            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("vehicle_unavailable_delete").optString("user_deleted");
            String is_available = jobj_main.optJSONObject("vehicle_unavailable_delete").optString("is_available");
            vehicleUnAvailabilityModel.setIs_available(is_available);
            Msg = jobj_main.optJSONObject("vehicle_unavailable_delete").optString("message");
            Status = jobj_main.optJSONObject("vehicle_unavailable_delete").optString("status");

            JSONObject jsonObject = jobj_main.optJSONObject("vehicle_unavailable_delete").optJSONObject("details");

//            vehicleUnAvailabilityModel.setId(jsonObject.optString("id"));
//            vehicleUnAvailabilityModel.setVehicle_id(jsonObject.optString("vehicle_id"));
//            vehicleUnAvailabilityModel.setUnavailable_date(jsonObject.optString("unavailable_date"));
//            vehicleUnAvailabilityModel.setAll_day(jsonObject.optString("all_day"));
//            vehicleUnAvailabilityModel.setStart_time(jsonObject.optString("start_time"));
//            vehicleUnAvailabilityModel.setEnd_time(jsonObject.optString("end_time"));
//
//            arrListVehicleUnAvailabilityModel.add(vehicleUnAvailabilityModel);


        }catch (Exception e) {
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        }

        if (Status.equals(StaticClass.SuccessResult)) {
            ((VehicleUnAvailableDelete_interface)mcontext).vehicleUnAvailableDelete(Status, Msg);
        }
    }
}

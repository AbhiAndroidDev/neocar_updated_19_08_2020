package com.neo.cars.app;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;

/**
 * Created by parna on 5/9/18.
 */

public class RootActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final int REQUEST_RESOLVE_ERROR = 12;
    private Context mContext;
    public int UPDATE_INTERVAL = 1000; // 10 sec
    public int FATEST_INTERVAL = 500; // 5 sec
    public int DISPLACEMENT = 0; // 10 meters
    private GoogleApiClient mApiClient;
    private LocationRequest mLocationRequest;
    private SharedPrefUserDetails sharedPrefUserDetails;
    private static final int REQUEST_LOCATION = 1;
    private static final int REQUEST_CONTACT= 2;
    private static final int REQUEST_CAMERA = 3;
    boolean mResolvingError = true;
    boolean isPermissionGranted = false;
    private  String selectedNumber = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new AnalyticsClass(RootActivity.this);
        mContext = this;
        sharedPrefUserDetails = new SharedPrefUserDetails(mContext);

        if (isPlayServicesAvailable()) {
            if (isDataConnectionAvailable()) {
                initLocationService();
            }
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */

    public boolean isPlayServicesAvailable() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.d("checkPlayServices() : ", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /*
        Check the device to make sure it has wifi/network connection .
        If it doesn't then return false otherwise true.
     */
    public boolean isDataConnectionAvailable() {
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        NetworkInfo mobileNwInfo = conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifiNwInfo = conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return ((mobileNwInfo == null ? false : mobileNwInfo.isConnected()) || (wifiNwInfo == null ? false : wifiNwInfo.isConnected()));
    }


    private void initLocationService() {
        mApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
        Log.d("GApiClientConnected", "GoogleApiClientConnected");

//        isPermissionGranted();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

//        isPermissionGranted();
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mApiClient, mLocationRequest, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            Log.e("lat : ",""+location.getLatitude());
            if (sharedPrefUserDetails == null) {
                sharedPrefUserDetails = new SharedPrefUserDetails(mContext);
                sharedPrefUserDetails.setCurrentLat(String.valueOf(location.getLatitude()));
                sharedPrefUserDetails.setCurrentLong(String.valueOf(location.getLongitude()));
            } else {
                sharedPrefUserDetails.setCurrentLat(String.valueOf(location.getLatitude()));
                sharedPrefUserDetails.setCurrentLong(String.valueOf(location.getLongitude()));

            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mApiClient != null) {
            mApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mApiClient != null) {
            if (mApiClient.isConnected()) {
                mApiClient.disconnect();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mApiClient != null) {
            if (mApiClient.isConnected()) {
                mApiClient.disconnect();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mApiClient != null) {
            mApiClient.connect();
        }
    }

    public boolean isLocationEnable() {
        boolean isEnabled = false;

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            isEnabled=false;
        } else {
            isEnabled=true;
        }
        return isEnabled;
    }

    public void isPermissionGranted(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Check Permissions Now
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Display UI and wait for user interaction
            } else{
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_LOCATION);
            }
            Log.d("isPermissionGranted", "isPermissionGranted");
        } else {
            // permission has been granted, continue as usual
//            Location myLocation =
//                    LocationServices.FusedLocationApi.getLastLocation(mApiClient);

            Log.d("isPermissionGranted", "isPermissionGrantedelse");
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mApiClient, mLocationRequest, this);
        }
    }


    public String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                if (sharedPrefUserDetails == null) {
                    sharedPrefUserDetails = new SharedPrefUserDetails(mContext);
                    sharedPrefUserDetails.setCurrentAddress(strAdd);
                }else {
                    sharedPrefUserDetails.setCurrentAddress(strAdd);
                }
            } else {
                Log.w("address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("address", "Cannot get Address!");
        }
        return strAdd;
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
        if (requestCode == REQUEST_LOCATION ){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                isPermissionGranted = true;
            } else {

                showAlert();
            }

            // Permission was denied or request was cancelled
        }

        if (requestCode == REQUEST_CAMERA ){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){

                isPermissionGranted = true;
            } else{

                showAlert();

                // Permission was denied or request was cancelled
            }
        }

        if (requestCode == REQUEST_CONTACT ){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){

                isPermissionGranted = true;
            } else{

                showAlert();

                // Permission was denied or request was cancelled
            }
        }
    }

    public void showAlert(){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Permissions Required")
                .setMessage("You have forcefully denied some of the required permissions " +
                        "for this action. Please open settings, go to permissions and allow them.")
                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }

    private boolean isPackageInstalled(Context context, String packagename) {

        PackageManager pkManager = context.getPackageManager();
        List<ApplicationInfo> packages = pkManager.getInstalledApplications(PackageManager.GET_META_DATA);

        for(ApplicationInfo info : packages){
            Log.d("**getPkgInfo**", info.packageName);
            if (info.packageName.contains(packagename)){
                return true;
            }
        }
        return false;
    }



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String convertInputStreamToString(InputStream inputStream)
            throws IOException {

        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }

        return result.toString(StandardCharsets.UTF_8.name());

    }
}
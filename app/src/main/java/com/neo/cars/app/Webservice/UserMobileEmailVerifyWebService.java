package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.AddRatingInterface;
import com.neo.cars.app.Interface.UserEmailMobileVerifyInterface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.AddRatingModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.CustomToastRating;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 13/11/18.
 */

public class UserMobileEmailVerifyWebService {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private VehicleTypeModel vehicleTypeModel;
    private JSONObject details;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;

    public void UserMobileEmailVerification(Activity context){
        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);

        showProgressDialog();

        StringRequest deleteUserRequest = new StringRequest(Request.Method.POST, Urlstring.is_user_verified_for_booking,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d(":: Response details:: ", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", sharedPref.getUserid());

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        deleteUserRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(deleteUserRequest);

    }


    private void Apiparsedata(String response){
        JSONObject jobj_main = null;

        try{
            jobj_main = new JSONObject(response);
            strUserDeleted = jobj_main.optJSONObject("is_user_verified_for_booking").optString("user_deleted");
            Msg = jobj_main.optJSONObject("is_user_verified_for_booking").optString("message");
            Status= jobj_main.optJSONObject("is_user_verified_for_booking").optString("status");
            details=jobj_main.optJSONObject("is_user_verified_for_booking").optJSONObject("details");


            if (Status.equals(StaticClass.SuccessResult)) {
//                Toast.makeText(mcontext, "Result : "+Msg, Toast.LENGTH_SHORT).show();
//                new CustomToastRating(mcontext, Msg);
            }else if (Status.equals(StaticClass.ErrorResult)){
                details = jobj_main.optJSONObject("is_user_verified_for_booking").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                    new CustomToast(mcontext, Msg);
                }

                ((UserEmailMobileVerifyInterface)mcontext).userEmailMobileVerify(Status);
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        if ("Y".equalsIgnoreCase(strUserDeleted)){
            Log.d("d", "***strUserDeleted***"+strUserDeleted);
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            Log.d("d", "***Status 12***"+StaticClass.SuccessResult);
            if (Status.equals(StaticClass.SuccessResult)) {
                Log.d("d", "***Status 34***"+StaticClass.SuccessResult);
                ((UserEmailMobileVerifyInterface)mcontext).userEmailMobileVerify(Status);

//                new CustomToast(mcontext, Msg);
            }

        }

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext ,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

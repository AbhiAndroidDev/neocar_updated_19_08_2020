package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.DriverDetailsUnAvailability_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.DriverUnAvailabilityDetailsModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DriverUnAvailableDetailsWebService {

    private Context mcontext;
    private Activity activity;
    private String Status = "0", Msg = "", strUserDeleted="", yearMonth = "";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private JSONObject details;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;
    private DriverUnAvailabilityDetailsModel driverUnAvailabilityModel;
    private ArrayList<DriverUnAvailabilityDetailsModel> arrListdriverUnAvailabilityModel;
    private DriverDetailsUnAvailability_Interface driverUnAvailability_interface;


    public void getDriverUnAvailableDetails(Context context, final Activity activity, final String strDriverId, final String strDate){

        this.mcontext = context;
        this.activity = activity;
        sharedPref = new SharedPrefUserDetails(mcontext);
        showProgressDialog();

        StringRequest citylistRequest = new StringRequest(Request.Method.POST, Urlstring.driver_unavailable_details,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(activity, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", sharedPref.getUserid());
                params.put("driver_id", strDriverId);
                params.put("unavailable_date", strDate);

                new PrintClass("city params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        citylistRequest.setRetryPolicy((new DefaultRetryPolicy(30000, 1, 1.0f)));
        Volley.newRequestQueue(context).add(citylistRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try{
            arrListdriverUnAvailabilityModel = new ArrayList<>();

            driverUnAvailabilityModel = new DriverUnAvailabilityDetailsModel();

            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("driver_unavailable_details").optString("user_deleted");
            String is_available = jobj_main.optJSONObject("driver_unavailable_details").optString("is_available");
            driverUnAvailabilityModel.setIs_available(is_available);
            Msg = jobj_main.optJSONObject("driver_unavailable_details").optString("message");
            Status = jobj_main.optJSONObject("driver_unavailable_details").optString("status");

            JSONObject jsonObject = jobj_main.optJSONObject("driver_unavailable_details").optJSONObject("details");

            driverUnAvailabilityModel.setId(jsonObject.optString("id"));
            driverUnAvailabilityModel.setDriver_id(jsonObject.optString("driver_id"));
            driverUnAvailabilityModel.setUnavailable_date(jsonObject.optString("unavailable_date"));
            driverUnAvailabilityModel.setAll_day(jsonObject.optString("all_day"));
            driverUnAvailabilityModel.setStart_time(jsonObject.optString("start_time"));
            driverUnAvailabilityModel.setEnd_time(jsonObject.optString("end_time"));

            arrListdriverUnAvailabilityModel.add(driverUnAvailabilityModel);


        }catch (Exception e) {
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        }

        if (Status.equals(StaticClass.SuccessResult)) {
            ((DriverDetailsUnAvailability_Interface)mcontext).driverUnAvailableDetails(driverUnAvailabilityModel, Status, Msg);
        }
    }
}

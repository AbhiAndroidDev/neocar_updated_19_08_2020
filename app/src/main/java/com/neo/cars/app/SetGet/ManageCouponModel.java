package com.neo.cars.app.SetGet;

/**
 * Created by parna on 8/3/18.
 */

public class ManageCouponModel {

    String couponcode;
    String date;
    String value;

    public String getCouponcode() {
        return couponcode;
    }

    public void setCouponcode(String couponcode) {
        this.couponcode = couponcode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

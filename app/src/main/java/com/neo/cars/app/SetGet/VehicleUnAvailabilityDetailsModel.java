package com.neo.cars.app.SetGet;

public class VehicleUnAvailabilityDetailsModel {
    private String is_available, id, vehicle_id, unavailable_date, all_day, start_time, end_time;

    public String getIs_available() {
        return is_available;
    }

    public void setIs_available(String is_available) {
        this.is_available = is_available;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getUnavailable_date() {
        return unavailable_date;
    }

    public void setUnavailable_date(String unavailable_date) {
        this.unavailable_date = unavailable_date;
    }

    public String getAll_day() {
        return all_day;
    }

    public void setAll_day(String all_day) {
        this.all_day = all_day;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }
}

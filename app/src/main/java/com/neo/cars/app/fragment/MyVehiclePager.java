package com.neo.cars.app.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.widget.TextView;

/**
 * Created by parna on 12/3/18.
 */

public class MyVehiclePager extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    private int tabCount;
    private TextView tvToolBar;

    //Constructor to the class
    public MyVehiclePager(FragmentManager fm, int tabCount, TextView tvToolBar) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
        this.tvToolBar = tvToolBar;
    }


    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                VehicleListFragment tab1 = new VehicleListFragment();
                return tab1;

            case 1:
                MyVehicleBookingFragment tab2 = new MyVehicleBookingFragment();
                return tab2;

            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return tabCount;
    }
}
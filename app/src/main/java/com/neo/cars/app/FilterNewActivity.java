package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neo.cars.app.Adapter.BrandOfVehicleAdapter;
import com.neo.cars.app.Adapter.FilterLuggageAdapter;
import com.neo.cars.app.Adapter.FilterPassengerAdapter;
import com.neo.cars.app.Adapter.TripCostAdapter;
import com.neo.cars.app.Adapter.TypeOfVehicleAdapter;
import com.neo.cars.app.Interface.FilterVehicleBrand_Interface;
import com.neo.cars.app.Interface.TripCostSelectInterface;
import com.neo.cars.app.Interface.TypeOfVehicleInterface;
import com.neo.cars.app.SetGet.TypeOfVehicleModel;
import com.neo.cars.app.SetGet.VehicleBrandModel;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.FilterVehicleBrand_Webservice;
import com.neo.cars.app.Webservice.TypeOfVehicleApi;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import java.util.ArrayList;
import java.util.Objects;

public class FilterNewActivity extends AppCompatActivity implements TypeOfVehicleInterface,
        FilterVehicleBrand_Interface, TripCostSelectInterface {

    private ArrayList<String> arrListCost;
    private ArrayList<String> arrListPassenger;
    private ArrayList<String> arrListLuggage;
    private ArrayList<TypeOfVehicleModel> arrlistVehicleType;
    private ArrayList<VehicleBrandModel> arrlistVehicleBrand;
    private ArrayList<String> arrlistVehicleBrandOriginal;
    private ArrayList<String> arrlistVehicleBrandDuplicate;
//    private LinearLayout ll_total_trip_cost;
    private GridView ll_total_trip_cost;
    private Context context;
    private Toolbar toolbar;
    private ConnectionDetector cd;
    private RelativeLayout rlBackLayout;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private int transitionflag = StaticClass.transitionflagNext;
    private String strStartDate="", strPickUpTime = "", strDuration = "";
    private String trip_cost_range = "", trip_cost_range_position = "-1", vehicle_type = "", vehicle_type_position = "-1",
            vehicle_brand = "", vehicle_brand_position = "-1", passenger_capacity = "", passenger_capacity_position = "-1",
            luggage_capacity = "", luggage_capacity_position = "-1", is_luxury_status = "", overtime_available = "";
    private TextView tv_edit_search, tvPickuptime, tvDate, tvDuration, tv_lux_car_only, tv_overtime_friendly, tv_show_more;
    private CustomTextviewTitilliumWebRegular tv_toolbar_clear;
    private Button btn_apply;
    private boolean is_lux_car_selected = false, is_overtime_selected = false;
    private int selectedPosition = -1;
    private GridView gridviewTripCost, gridview_type_vehicle, gridview_brand_vehicle, gridview_brand_vehicle_all,
            gridview_passenger_capicity, gridview_luggage_capicity;
    private BottomView bottomview = new BottomView();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_new);

        Intent intent = getIntent();
        if (intent != null) {
            strStartDate = intent.getStringExtra("FromDate");
            strPickUpTime = intent.getStringExtra("PickUpTime");
            strDuration = intent.getStringExtra("Duration");
            trip_cost_range = intent.getStringExtra("trip_cost_range");
            trip_cost_range_position = intent.getStringExtra("trip_cost_range_position");
            vehicle_type = intent.getStringExtra("vehicle_type");
            vehicle_type_position = intent.getStringExtra("vehicle_type_position");
            vehicle_brand = intent.getStringExtra("vehicle_brand");
            vehicle_brand_position = intent.getStringExtra("vehicle_brand_position");
            is_luxury_status = intent.getStringExtra("is_luxury_status");
            passenger_capacity = intent.getStringExtra("passenger_capacity");
            passenger_capacity_position = intent.getStringExtra("passenger_capacity_position");
            luggage_capacity = intent.getStringExtra("luggage_capacity");
            luggage_capacity_position = intent.getStringExtra("luggage_capacity_position");
            overtime_available = intent.getStringExtra("overtime_available");

        }


        initialize();
        listener();

        if(cd.isConnectingToInternet()){

            new TypeOfVehicleApi().typeOfVehicle(FilterNewActivity.this);
            new FilterVehicleBrand_Webservice().vehicleBrandWebservice(FilterNewActivity.this);

        }else{
            Intent i = new Intent(FilterNewActivity.this, NetworkNotAvailable.class);
            transitionflag = StaticClass.transitionflagBack;
            startActivity(i);
        }

        setSelectedField();
    }

    private void setSelectedField() {
        tvDate.setText(strStartDate);
        tvDate.setAllCaps(true);
        tvDuration.setText(strDuration + " HOURS");

        if (!TextUtils.isEmpty(strPickUpTime)){
            tvPickuptime.setText(strPickUpTime);
        }

        if(is_luxury_status.equals("Y")){
            is_lux_car_selected = false;
        }else{
            is_lux_car_selected = true;
        }

        setLuxuarySelection();

        if(overtime_available.equals("Y")){
            is_overtime_selected = false;
        }else{
            is_overtime_selected = true;
        }

        setOvertimeFriendly();

//        setVehicleTypeAdapter();
//
//        setVehicleBrandAdapter();


    }

    private void initialize() {

        cd = new ConnectionDetector(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title =  findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText("FILTER BY");

        tv_toolbar_clear= findViewById(R.id.tv_toolbar_clear);
        tv_toolbar_clear.setVisibility(View.VISIBLE);

        tv_toolbar_clear.setText("RESET");

        rlBackLayout = findViewById(R.id.rlBackLayout);

        tvPickuptime = findViewById(R.id.tvPickuptime);
        tvDate = findViewById(R.id.tvDate);
        tvDuration = findViewById(R.id.tvDuration);
        tv_lux_car_only = findViewById(R.id.tv_lux_car_only);
        tv_overtime_friendly = findViewById(R.id.tv_overtime_friendly);
        tv_show_more = findViewById(R.id.tv_show_more);

        btn_apply = findViewById(R.id.btn_apply);

        gridviewTripCost = findViewById(R.id.gridview_trip_cost);
        gridview_type_vehicle = findViewById(R.id.gridview_type_vehicle);
        gridview_brand_vehicle = findViewById(R.id.gridview_brand_vehicle);
        gridview_brand_vehicle_all = findViewById(R.id.gridview_brand_vehicle_all);
        gridview_passenger_capicity = findViewById(R.id.gridview_passenger_capicity);
        gridview_luggage_capicity = findViewById(R.id.gridview_luggage_capicity);

        arrListCost = new ArrayList();
        arrListPassenger = new ArrayList();
        arrListLuggage = new ArrayList();

        //Arraylist for trip cost range
        arrListCost.add("< "+getResources().getString(R.string.Rs)+"2000");
        arrListCost.add(getResources().getString(R.string.Rs)+"2001 - "+getResources().getString(R.string.Rs)+"4999");
        arrListCost.add(getResources().getString(R.string.Rs)+"5000 - "+getResources().getString(R.string.Rs)+"9999");
        arrListCost.add(getResources().getString(R.string.Rs)+"10000 - "+getResources().getString(R.string.Rs)+"20999");
        arrListCost.add(getResources().getString(R.string.Rs)+"21000 - "+getResources().getString(R.string.Rs)+"49999");
        arrListCost.add("> "+getResources().getString(R.string.Rs)+"50000");

        //Arraylist for passenger
        arrListPassenger.add("1 - 4");
        arrListPassenger.add("5 - 6");
        arrListPassenger.add("> 6");

        //Arraylist for luggage
        arrListLuggage.add("1 - 2");
        arrListLuggage.add("3 - 4");
        arrListLuggage.add("5 - 6");
        arrListLuggage.add("> 6");

        //set tripcost adapter
        setTripcostAdapter();
        //set passenger adapter
        setpassengerAdapter();
        //set luggage adapter
        setLuggageAdapter();

        bottomview.BottomView(FilterNewActivity.this, StaticClass.Menu_Explore);

    }

    private void setTripcostAdapter(){
        gridviewTripCost.setAdapter(new TripCostAdapter(this, FilterNewActivity.this, arrListCost, Integer.parseInt(trip_cost_range_position)) );

    }
    private void setpassengerAdapter(){
        gridview_passenger_capicity.setAdapter(new FilterPassengerAdapter(this, FilterNewActivity.this, arrListPassenger,
                Integer.parseInt(passenger_capacity_position)));
    }
    private void setLuggageAdapter(){
        gridview_luggage_capicity.setAdapter(new FilterLuggageAdapter(this, FilterNewActivity.this, arrListLuggage,
                Integer.parseInt(luggage_capacity_position)));
    }

    private void listener() {

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(FilterNewActivity.this,
//                        FilterNewActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
//                        FilterNewActivity.this.getResources().getString(R.string.yes),
//                        FilterNewActivity.this.getResources().getString(R.string.no));
//                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        alertDialogYESNO.dismiss();
                        transitionflag = StaticClass.transitionflagBack;
                        finish();
//                    }
//                });
//
//                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        alertDialogYESNO.dismiss();
//                    }
//                });
//
//                alertDialogYESNO.show();
            }
        });

        tv_lux_car_only.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLuxuarySelection();
            }
        });

        tv_overtime_friendly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               setOvertimeFriendly();
            }
        });

        btn_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(is_luxury_status.equals("N"))
                    is_luxury_status = "";
                if(overtime_available.equals("N"))
                    overtime_available = "";

                Intent applyintent = new Intent();
                applyintent.putExtra("trip_cost_range", trip_cost_range);
                applyintent.putExtra("trip_cost_range_position", trip_cost_range_position);
                applyintent.putExtra("is_luxury_status", is_luxury_status);
                applyintent.putExtra("vehicle_type", vehicle_type );
                applyintent.putExtra("vehicle_type_position", vehicle_type_position );
                applyintent.putExtra("vehicle_brand", vehicle_brand );
                applyintent.putExtra("vehicle_brand_position", vehicle_brand_position );
                applyintent.putExtra("passenger_capacity", passenger_capacity );
                applyintent.putExtra("passenger_capacity_position", passenger_capacity_position );
                applyintent.putExtra("luggage_capacity", luggage_capacity );
                applyintent.putExtra("luggage_capacity_position", luggage_capacity_position );
                applyintent.putExtra("overtime_available", overtime_available);

                setResult(StaticClass.SearchFilterRequestCode, applyintent);
                finish();
            }
        });

        tv_toolbar_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(FilterNewActivity.this,
                        FilterNewActivity.this.getResources().getString(R.string.filter_reset_msg),
                        FilterNewActivity.this.getResources().getString(R.string.yes),
                        FilterNewActivity.this.getResources().getString(R.string.no));
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();

                        StaticClass.resetClicked = true;
                        setReset();

                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();
            }
        });

        tv_show_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (arrlistVehicleBrandOriginal.size() > 0) {
                    setVehicleBrandAllAdapter();

                    StaticClass.showMoreClicked = true;
                    tv_show_more.setVisibility(View.GONE);
                }
            }
        });
    }

    private void setReset() {

        tv_show_more.setVisibility(View.VISIBLE);
        gridview_brand_vehicle.setVisibility(View.VISIBLE);
        gridview_brand_vehicle_all.setVisibility(View.GONE);

        trip_cost_range = "";
        trip_cost_range_position = "-1";
        is_luxury_status = "";
        is_lux_car_selected = true;
        vehicle_type = "";
        vehicle_type_position = "-1";
        vehicle_brand = "";
        vehicle_brand_position = "-1";
        passenger_capacity = "";
        passenger_capacity_position = "-1";
        luggage_capacity = "";
        luggage_capacity_position = "-1";
        is_overtime_selected = true;
        overtime_available = "";

        StaticClass.selectedPosition = -1;
        StaticClass.showMoreClicked = false;

        setTripcostAdapter();
        setLuxuarySelection();
        setVehicleTypeAdapter();
        setVehicleBrandAdapter();
        setpassengerAdapter();
        setLuggageAdapter();
        setOvertimeFriendly();

        is_luxury_status = "";
        overtime_available = "";
    }


    private void setOvertimeFriendly() {

        if(is_overtime_selected){
            tv_overtime_friendly.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.btn_dselect, 0);
            tv_overtime_friendly.setBackgroundResource(R.drawable.base);
            is_overtime_selected = false;
            overtime_available = "N";
        }else {
            tv_overtime_friendly.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.btn_select, 0);
            tv_overtime_friendly.setBackgroundResource(R.drawable.base_yellow);
            is_overtime_selected = true;
            overtime_available = "Y";
        }
    }

    private void setLuxuarySelection(){
        if(is_lux_car_selected){
            tv_lux_car_only.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.btn_dselect, 0);
            tv_lux_car_only.setBackgroundResource(R.drawable.base);
            is_lux_car_selected = false;
            is_luxury_status = "N";
        }else {
            tv_lux_car_only.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.btn_select, 0);
            tv_lux_car_only.setBackgroundResource(R.drawable.base_yellow);
            is_lux_car_selected = true;
            is_luxury_status = "Y";
        }
    }

    @Override
    public void TypeOfVehicle(ArrayList<TypeOfVehicleModel> _arrlistVehicleType) {
        arrlistVehicleType = _arrlistVehicleType;

        if(arrlistVehicleType.size() > 0)
        gridview_type_vehicle.setAdapter(new TypeOfVehicleAdapter(this, FilterNewActivity.this, arrlistVehicleType, Integer.parseInt(vehicle_type_position)));
    }

    @Override
    public void VehicleBrandList(ArrayList<VehicleBrandModel> _arrlistVehicleBrand) {
        arrlistVehicleBrand = _arrlistVehicleBrand;
        arrlistVehicleBrandDuplicate = new ArrayList<>();
        arrlistVehicleBrandOriginal = new ArrayList<>();

        for(int i = 0; i < _arrlistVehicleBrand.size(); i++){
            arrlistVehicleBrandOriginal.add(_arrlistVehicleBrand.get(i).getTitle());
        }

        for(int i = 0; i < _arrlistVehicleBrand.size(); i++){

            if(i < 9) {
                arrlistVehicleBrandDuplicate.add(_arrlistVehicleBrand.get(i).getTitle());
            }
        }


        if(_arrlistVehicleBrand.size() > 0)
        setVehicleBrandAdapter();
    }

    @Override
    public void setTripCost(int selectedPosition, String selectedField, int showMoreClickedCount) {

        if(selectedField.equals("trip_cost")) {
            gridviewTripCost.setAdapter(new TripCostAdapter(this, FilterNewActivity.this,
                    arrListCost, selectedPosition));

            trip_cost_range_position = String.valueOf(selectedPosition);

            if(selectedPosition == 0) {
                trip_cost_range = "0-2000";
            }else if(selectedPosition == 1){
                trip_cost_range = "2001-4999";
            }else if(selectedPosition == 2){
                trip_cost_range = "5000-9999";
            }else if(selectedPosition == 3){
                trip_cost_range = "10000-20999";
            }else if(selectedPosition == 4){
                trip_cost_range = "21000-49999";
            }else if(selectedPosition == 5){
                trip_cost_range = "50000-500001";
            }

        }else if(selectedField.equals("filter_passenger")){
            gridview_passenger_capicity.setAdapter(new FilterPassengerAdapter(this, FilterNewActivity.this,
                    arrListPassenger, selectedPosition));

            passenger_capacity_position = String.valueOf(selectedPosition);

            if(selectedPosition == 0) {
                passenger_capacity = "1-4";
            }else if(selectedPosition == 1){
                passenger_capacity = "5-6";
            }else if(selectedPosition == 2){
                passenger_capacity = "6-10";
            }

        }else if(selectedField.equals("filter_luggage")){
            gridview_luggage_capicity.setAdapter(new FilterLuggageAdapter(this, FilterNewActivity.this,
                    arrListLuggage, selectedPosition));

            luggage_capacity_position = String.valueOf(selectedPosition);

            if(selectedPosition == 0) {
                luggage_capacity = "1-2";
            }else if(selectedPosition == 1){
                luggage_capacity = "3-4";
            }else if(selectedPosition == 2){
                luggage_capacity = "5-6";
            }else if(selectedPosition == 3){
                luggage_capacity = "6-10";
            }

        }else if(selectedField.equals("vehicle_type")){
            gridview_type_vehicle.setAdapter(new TypeOfVehicleAdapter(this, FilterNewActivity.this,
                    arrlistVehicleType, selectedPosition));

            vehicle_type = arrlistVehicleType.get(selectedPosition).getId();

            vehicle_type_position = String.valueOf(selectedPosition);

        }else if(selectedField.equals("vehicle_brand")){

            if(StaticClass.showMoreClicked){
                setVehicleBrandAllAdapter();
            }else {
                setVehicleBrandAdapter();
            }

            if(selectedPosition != -1) {
                vehicle_brand = arrlistVehicleBrand.get(selectedPosition).getId();
                vehicle_brand_position = String.valueOf(selectedPosition);
            }
        }
    }

    private void setVehicleTypeAdapter(){
        gridview_type_vehicle.setAdapter(new TypeOfVehicleAdapter(this, FilterNewActivity.this,
                arrlistVehicleType, Integer.parseInt(vehicle_type_position)) );
    }


    private void setVehicleBrandAdapter(){

        gridview_brand_vehicle.setAdapter(new BrandOfVehicleAdapter(this, FilterNewActivity.this,
                arrlistVehicleBrandDuplicate, Integer.parseInt(vehicle_brand_position)));

        tv_show_more.setVisibility(View.VISIBLE);
    }

    private void setVehicleBrandAllAdapter(){

        gridview_brand_vehicle.setVisibility(View.GONE);
        gridview_brand_vehicle_all.setVisibility(View.VISIBLE);
        gridview_brand_vehicle_all.setAdapter(new BrandOfVehicleAdapter(this, FilterNewActivity.this,
                arrlistVehicleBrandOriginal, Integer.parseInt(vehicle_brand_position)));

        tv_show_more.setVisibility(View.GONE);
    }

    private void  setDynamicHeight(){
        ViewGroup.LayoutParams layoutParams = gridview_brand_vehicle.getLayoutParams();
        layoutParams.height = 350; //this is in pixels
        gridview_brand_vehicle.setLayoutParams(layoutParams);
        gridview_brand_vehicle.requestFocus();
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(FilterNewActivity.this,
//                FilterNewActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
//                FilterNewActivity.this.getResources().getString(R.string.yes),
//                FilterNewActivity.this.getResources().getString(R.string.no));
//        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialogYESNO.dismiss();
                transitionflag = StaticClass.transitionflagBack;
                finish();
//
//            }
//        });
//
//        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialogYESNO.dismiss();
//            }
//        });
//
//        alertDialogYESNO.show();
    }


    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(FilterNewActivity.this, transitionflag);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) FilterNewActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}

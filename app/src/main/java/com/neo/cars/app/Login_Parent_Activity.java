package com.neo.cars.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.neo.cars.app.Interface.OtpVerification_Interface;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.LoginWithUsernameView;
import com.neo.cars.app.View.Login_with_mobile_number_View;
import com.neo.cars.app.View.OtpVerification;

public class Login_Parent_Activity extends AppCompatActivity implements OtpVerification_Interface {

    private int transitionflag = StaticClass.transitionflagNext;
    private int logintype=1, loginpage = 0;
    private RelativeLayout parent_login_relative;
    private ImageButton ibBackbutton;
    private SharedPrefUserDetails prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_login);

        logintype=getIntent().getIntExtra("logintype",1);
        new AnalyticsClass(Login_Parent_Activity.this);
        Initialize();
        Listener();
        //use to set mobile no
        StaticClass.Is_MobileNo = false;

    }

    private void Initialize(){

        prefs = new SharedPrefUserDetails(this);
        prefs.setMobile("");
        prefs.setEmail("");
        parent_login_relative=findViewById(R.id.parent_login_relative) ;
        ibBackbutton=findViewById(R.id.ibBackbutton);

        if(logintype==1){
            parent_login_relative.addView(new LoginWithUsernameView().LoginWithUsernameView(Login_Parent_Activity.this));
        }else{
            parent_login_relative.addView(new Login_with_mobile_number_View().Login_with_mobile_number_View(Login_Parent_Activity.this));

        }
    }

    private void Listener(){
        ibBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (loginpage == 0) {
                    transitionflag = StaticClass.transitionflagBack;
                    Intent i = new Intent(Login_Parent_Activity.this, LoginStepTwoActivity.class);
                    startActivity(i);
                    finish();
                }
                if (loginpage == 1) {
                    parent_login_relative.addView(new Login_with_mobile_number_View().Login_with_mobile_number_View(Login_Parent_Activity.this));
                    loginpage = 0;
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        new OnPauseSlider(Login_Parent_Activity.this, transitionflag);
    }

    @Override
    public void onBackPressed() {

        if (loginpage == 0) {
            transitionflag = StaticClass.transitionflagBack;
            Intent i = new Intent(Login_Parent_Activity.this, LoginStepTwoActivity.class);
            startActivity(i);
            finish();
        }

        if (loginpage == 1) {
            parent_login_relative.addView(new Login_with_mobile_number_View().Login_with_mobile_number_View(Login_Parent_Activity.this));
            loginpage = 0;
        }
    }

    @Override
    public void OtpVerification(String status, String st_mobile, String otp, String user_type) {
        parent_login_relative.addView(new OtpVerification().OtpVerification(Login_Parent_Activity.this,st_mobile,otp, user_type));

        //used to show mobile no input view on back click
        loginpage = 1;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }

        if (prefs.getLoginStatus()){
            finish();
        }
    }
}
